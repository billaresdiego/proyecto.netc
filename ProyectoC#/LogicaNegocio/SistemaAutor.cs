﻿using DominioNegocio;
using System.Collections.Generic;
using PersistenciaNegocio;
using ExcepcionesLogica;

namespace LogicaNegocio
{
    public partial class Sistema
    {
        private PersistenciaAutor autores;
        public void AgregarAutor(Autor unAutor)
        {
            this.autores.AgregarAutor(unAutor);
        }

        public List<Autor> DevolverAutores()
        {
            return autores.DevolverAutores();
        }

        public Autor DevolverAutor(Autor unAutor)
        {
            ComprobarExistenciaAutor(unAutor);
            return autores.DevolverAutorEnDB(unAutor);
        }

        private void ComprobarExistenciaAutor(Autor unAutor)
        {
            if (this.autores.DevolverAutorEnDB(unAutor) == null)
            {
                throw new ExcepcionAutorNoExistente();
            }
        }

        public void BorrarTodosLosAutores()
        {
            autores.BorrarTodosLosAutoresEnDB();
        }

        public void BorrarAutor(Autor autorABorrar)
        {
            autores.BorrarAutorEnDB(autorABorrar);
        }

        public void ModificarAutorEnSistema(Autor autorAModificar, Autor autorParametrosModificados)
        {
            autores.ModificarAutorEnDB(autorAModificar, autorParametrosModificados);
        }
    }
}
