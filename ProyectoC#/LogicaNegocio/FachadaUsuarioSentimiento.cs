﻿using System.Collections.Generic;
using DominioNegocio;
using DominioNegocio.Excepciones;

namespace LogicaNegocio
{
    public partial class FachadaUsuario
    {
        public void CrearSentimientoSinTipoEnSistema(string nombreSentimiento)
        {
            Sentimiento sentimientoNegativo = new Sentimiento(Sentimiento.TiposSentimiento.sinTipo, nombreSentimiento);
            fachada.AgregarSentimiento(sentimientoNegativo);
        }

        public void CrearSentimientoNegativoEnSistema(string nombreSentimiento)
        {
            Sentimiento sentimientoNegativo = new Sentimiento(Sentimiento.TiposSentimiento.negativo, nombreSentimiento);
            fachada.AgregarSentimiento(sentimientoNegativo);
        }

        public void CrearSentimientoPositivoEnSistema(string nombreSentimiento)
        {
            Sentimiento sentimientoPositivo = new Sentimiento(Sentimiento.TiposSentimiento.positivo, nombreSentimiento);
            fachada.AgregarSentimiento(sentimientoPositivo);
        }

        public bool SentimientoEstaEnAlgunaFrase(string contenidoSentimiento)
        {
            List<Frase> listaDeFrases = this.DevolverFrases();
            Sentimiento sentimientoABuscar = new Sentimiento(Sentimiento.TiposSentimiento.sinTipo, contenidoSentimiento);
            for (int i = 0; i < listaDeFrases.Count; i++)
            {
                if (FraseContieneSentimiento(listaDeFrases[i], sentimientoABuscar)){
                    return true;
                }
            }
            return false;
        }

        public List<Sentimiento> DevolverSentimientos()
        {
            return fachada.DevolverSentimientos();
        }

        public void BorrarSentimiento(string contenidoSentimientoABorrar)
        {
            Sentimiento sentimientoABorrar = new Sentimiento(Sentimiento.TiposSentimiento.sinTipo, contenidoSentimientoABorrar);
            if (!SentimientoEstaEnAlgunaFrase(contenidoSentimientoABorrar))
            {
                fachada.BorrarSentimiento(sentimientoABorrar);
            }
            else
            {
                throw new ExcepcionSentimientoNoSePuedeBorrar();
            }
        }

        public void BorrarTodosSentimientos()
        {
            fachada.BorrarTodosSentimientos();
        }
    }
}
