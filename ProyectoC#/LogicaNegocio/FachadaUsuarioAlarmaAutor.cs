﻿using DominioNegocio;
using DominioNegocio.Excepciones;
using ExcepcionesLogica;
using System;
using System.Collections.Generic;

namespace LogicaNegocio
{
    public partial class FachadaUsuario
    {
        public void ConfigurarAlarmaAutor(int cantidadDePosts, int plazo, string tipoAlarma, string tipoPlazo)
        {
            fachada.AlarmaAutorARegistrar = new AlarmaAutor
            {
                CantidadDePosts = cantidadDePosts,
            };
            DeterminarPlazo(plazo, tipoPlazo);
            DeterminarTipo(tipoAlarma);
            try
            {
                AlertaFrasesAlarmaAutor();
                fachada.AgregarAlarmaAutorConfigurada(fachada.AlarmaAutorARegistrar);
            }
            catch (Exception)
            {
                fachada.AgregarAlarmaAutorConfigurada(fachada.AlarmaAutorARegistrar);
                throw new ExcepcionAlertaXComentariosNegativosYDias();
            }
        }

        private void AlertaFrasesAlarmaAutor()
        {
            if (fachada.AlarmaAutorARegistrar.UnTipo == AlarmaAutor.TipoAlarmaAutor.negativo)
            {
                fachada.AlertaXFrasesNegativasEnYPlazoAlarmaAutor();
            }
            else
            {
                fachada.AlertaXFrasesPositivasEnYPlazoAlarmaAutor();
            }
        }

        public List<AlarmaAutor> DevolverAlarmasAutorActivadasPorFrases()
        {
            return fachada.DevolverAlarmasAutorActivadasPorFrases();
        }

        public AlarmaAutor DevolverAlarmaAutorConfigurada(AlarmaAutor unaAlarma)
        {
            return this.fachada.DevolverAlarmaAutorConfigurada(unaAlarma);
        }

        private void DeterminarTipo(string tipoAlarma)
        {
            if (tipoAlarma == "Positivo" || tipoAlarma == "Positiva")
            {
                fachada.AlarmaAutorARegistrar.UnTipo = AlarmaAutor.TipoAlarmaAutor.positivo;
            }
            else if (tipoAlarma == "Negativo" || tipoAlarma == "Negativa")
            {
                fachada.AlarmaAutorARegistrar.UnTipo = AlarmaAutor.TipoAlarmaAutor.negativo;
            }
            else
            {
                throw new ExcepcionAlarmaSinTipo();
            }
        }

        private void DeterminarPlazo(int plazo, string tipoPlazo)
        {
            if (tipoPlazo == "Dias")
            {
                fachada.AlarmaAutorARegistrar.PlazoDias = plazo;
            }
            else
            {
                fachada.AlarmaAutorARegistrar.PlazoHoras = plazo;
            }
        }

        public AlarmaAutor MostrarAlarmaAutorARegistrar()
        {
            return fachada.AlarmaAutorARegistrar;
        }

        public List<AlarmaAutor> DevolverAlarmasAutorRegistradas()
        {
            return this.fachada.DevolverAlarmasAutorRegistradas();
        }

        public void BorrarTodasLasAlarmasAutor()
        {
            fachada.BorrarAlarmasAutor();
        }
    }
}
