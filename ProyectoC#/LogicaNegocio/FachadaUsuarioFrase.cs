﻿using ExcepcionesLogica;
using DominioNegocio;
using DominioNegocio.Excepciones;
using System;
using System.Collections.Generic;

namespace LogicaNegocio
{
    public partial class FachadaUsuario
    {
        public List<Frase> DevolverFrases()
        {
            return fachada.DevolverFrases();
        }

        public void CrearFraseEnSistema(string nombreFrase, DateTime fechaFrase, Autor autor)
        {
            ValidarFecha(fechaFrase);
            Frase unaFrase = new Frase
            {
                ContenidoUnaFrase = nombreFrase,
                FechaDeIngreso = fechaFrase,
                Autor = autor
            };
            EstablecerCategoriaFrase(unaFrase);
            EstablecerEntidadFrase(unaFrase);
            fachada.AgregarFrase(unaFrase);
            fachada.DevolverAlarmasAutorActivadasPorFrases();
            fachada.DevolverAlarmasActivadasPorFrases();
        }

        private void ValidarFecha(DateTime fechaFrase)
        {
            if(fechaFrase.CompareTo(DateTime.Now) > 0)
            {
                throw new ExcepcionFechaFutura();
            }
        }

        private void EstablecerCategoriaFrase(Frase unaFrase)
        {
            List<Sentimiento.TiposSentimiento> tiposSentimientosFrase = TiposSentimientosEncontrados(unaFrase);
            if (ContieneDosTiposSentimientos(tiposSentimientosFrase))
            {
                unaFrase.UnaCategoria = "Neutro";
            }
            else if (ContieneTipoPositivo(tiposSentimientosFrase))
            {
                unaFrase.UnaCategoria = "Positivo";
            }
            else
            {
                unaFrase.UnaCategoria = "Negativo";
            }
        }

        private List<Sentimiento.TiposSentimiento> TiposSentimientosEncontrados(Frase unaFrase)
        {
            List<Sentimiento.TiposSentimiento> tiposSentimientosEncontrados = new List<Sentimiento.TiposSentimiento>();
            foreach (Sentimiento unSentimiento in fachada.DevolverSentimientos())
            {
                if (FraseContieneSentimiento(unaFrase, unSentimiento))
                {
                    AgregarTipoSentimientoEncontrado(tiposSentimientosEncontrados, unSentimiento.TipoSentimiento);
                }
            }
            if (tiposSentimientosEncontrados.Count == 0)
            {
                throw new ExcepcionFraseSinSentimientoExistente();
            }
            return tiposSentimientosEncontrados;
        }

        private bool FraseContieneSentimiento(Frase unaFrase, Sentimiento unSentimiento)
        {
            return DevelorFraseEnMinuscula(unaFrase).Contains(unSentimiento.NombreSentimiento);
        }

        private string DevelorFraseEnMinuscula(Frase unaFrase)
        {
            return unaFrase.ContenidoUnaFrase.ToLower();
        }

        private void AgregarTipoSentimientoEncontrado(List<Sentimiento.TiposSentimiento> tiposSentimientosEncontrados,
                                                       Sentimiento.TiposSentimiento tipoSentimiento)
        {
            tiposSentimientosEncontrados.Add(tipoSentimiento);
        }

        private bool ContieneDosTiposSentimientos(List<Sentimiento.TiposSentimiento> tiposSentimientosFrase)
        {
            return tiposSentimientosFrase.Contains(Sentimiento.TiposSentimiento.positivo) &&
                    tiposSentimientosFrase.Contains(Sentimiento.TiposSentimiento.negativo);
        }

        private bool ContieneTipoPositivo(List<Sentimiento.TiposSentimiento> tiposSentimientosFrase)
        {
            return tiposSentimientosFrase.Contains(Sentimiento.TiposSentimiento.positivo);
        }

        private void EstablecerEntidadFrase(Frase unaFrase)
        {
            ValidarFraseContieneEntidadExistente(unaFrase);
            unaFrase.UnaEntidad = BuscarEntidadEnFrase(unaFrase);
        }

        private void ValidarFraseContieneEntidadExistente(Frase unaFrase)
        {
            if (NoEncontroEntidad(unaFrase))
            {
                throw new ExcepcionFraseSinEntidadExistente();
            }
        }

        private bool NoEncontroEntidad(Frase unaFrase)
        {
            return BuscarEntidadEnFrase(unaFrase) == null;
        }

        private Entidad BuscarEntidadEnFrase(Frase unaFrase)
        {
            foreach (Entidad entidadAComparar in fachada.DevolverEntidades())
            {
                if (ContieneEntidadNoEstablecida(unaFrase, entidadAComparar))
                {
                    unaFrase.UnaEntidad = entidadAComparar;
                }
            }
            return unaFrase.UnaEntidad;
        }

        private bool ContieneEntidadNoEstablecida(Frase unaFrase, Entidad entidadAComparar)
        {
            return unaFrase.ContenidoUnaFrase.Contains(entidadAComparar.NombreUnaEntidad) && unaFrase.UnaEntidad == null;
        }

        public void BorrarTodasFrases()
        {
            fachada.BorrarTodasFrases();
        }

        public List<Frase> DevolverFrasesConEntidadYFechaYHora()
        {
            return fachada.DevolverFrasesConEntidadYFechaYHora();
        }
    }
}
