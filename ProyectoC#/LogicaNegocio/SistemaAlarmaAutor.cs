﻿using DominioNegocio;
using ExcepcionesLogica;
using LogicaNegocio.UtilidadesValidarCondiciones;
using PersistenciaNegocio;
using System;
using System.Collections.Generic;

namespace LogicaNegocio
{
    public partial class Sistema : Fachada
    {
        private PersistenciaAlarmaAutor alarmasAutorConfiguradas;
        private ServidorAlarma alarmaAutorARegistrar;

        public AlarmaAutor AlarmaAutorARegistrar
        {
            get
            {
                return (AlarmaAutor)alarmaAutorARegistrar;
            }
            set
            {
                this.alarmaAutorARegistrar = value;
            }
        }

        public void AgregarAlarmaAutorConfigurada(AlarmaAutor unaAlarmaAutor)
        {
            this.alarmasAutorConfiguradas.AgregarAlarmaAutorDB(unaAlarmaAutor);
        }

        public List<AlarmaAutor> DevolverAlarmasAutorRegistradas()
        {
            List<AlarmaAutor> alarmasAutorRegistradas = new List<AlarmaAutor>();
            foreach (AlarmaAutor unaAlarma in this.alarmasAutorConfiguradas.DevolverAlarmasAutoresDB())
            {
                alarmasAutorRegistradas.Add(unaAlarma);
            }
            return alarmasAutorRegistradas;
        }

        public AlarmaAutor DevolverAlarmaAutorConfigurada(AlarmaAutor unaAlarma)
        {
            HerramientasSistema.ExisteAlarma(this.alarmasAutorConfiguradas.DevolverAlarmasAutoresDelTipoServidorAlarmasDB(), unaAlarma);
            return this.alarmasAutorConfiguradas.DevolverAlarmaAutorEnDB(unaAlarma);
        }

        public void AlertaXFrasesPositivasEnYPlazoAlarmaAutor()
        {
            if (CantidadFrasesPositivasSuperaCantidadPostsAlarmaAutor(DevolverAlarmaAutorRegistrada()))
            {
                DevolverAlarmaAutorRegistrada().Activada = true;
                throw new ExcepcionAlertaXComentariosPositivosYDias();
            }
        }

        private bool CantidadFrasesPositivasSuperaCantidadPostsAlarmaAutor(AlarmaAutor unaAlarma)
        {
            return unaAlarma.CantidadDePosts <= CantidadDeFrasesPositivasAlarmaAutor(unaAlarma).Count;
        }

        private List<string> CantidadDeFrasesPositivasAlarmaAutor(AlarmaAutor unaAlarma)
        {
            List<string> sentimientosFrasesEntidadAlerta = new List<string>();
            foreach (Frase fraseActual in this.frases.DevolverFrasesConEntidadYFechaHorasDB())
            {
                if (FraseAlertaPositivaDentroDelPlazoAlarmaAutor(unaAlarma, fraseActual))
                {
                    sentimientosFrasesEntidadAlerta.Add(fraseActual.UnaCategoria);
                }
            }
            return sentimientosFrasesEntidadAlerta;
        }

        private bool FraseAlertaPositivaDentroDelPlazoAlarmaAutor(AlarmaAutor unaAlarma, Frase unaFrase)
        {
            return EsUnaFraseAlertaPositiva(unaFrase) && FechaDeFraseDentroDePlazoAlarmaAutor(unaAlarma, unaFrase);
        }

        private bool EsUnaFraseAlertaPositiva(Frase unaFrase)
        {
            return unaFrase.UnaCategoria.ToLower() == "positivo";
        }

        public void AlertaXFrasesNegativasEnYPlazoAlarmaAutor()
        {
            if (CantidadFrasesNegativasSuperaCantidadPostsAlarmaAutor(DevolverAlarmaAutorRegistrada()))
            {
                DevolverAlarmaRegistrada().Activada = true;
                throw new ExcepcionAlertaXComentariosNegativosYDias();
            }
        }


        private bool CantidadFrasesNegativasSuperaCantidadPostsAlarmaAutor(AlarmaAutor unaAlarma)
        {
            return unaAlarma.CantidadDePosts <= CantidadDeFrasesNegativasAlarmaAutor(unaAlarma).Count;
        }

        private List<string> CantidadDeFrasesNegativasAlarmaAutor(AlarmaAutor unaAlarma)
        {
            List<string> sentimientosFrasesEntidadAlertaAlarmaAutor = new List<string>();
            foreach (Frase fraseActual in this.frases.DevolverFrasesConEntidadYFechaHorasDB())
            {
                if (FraseAlertaNegativaDentroDelPlazoAlarmaAutor(unaAlarma, fraseActual))
                {
                    sentimientosFrasesEntidadAlertaAlarmaAutor.Add(fraseActual.UnaCategoria);
                }
            }
            return sentimientosFrasesEntidadAlertaAlarmaAutor;
        }

        private bool FraseAlertaNegativaDentroDelPlazoAlarmaAutor(AlarmaAutor unaAlarma, Frase unaFrase)
        {
            return EsUnaFraseAlertaNegativa(unaFrase) && FechaDeFraseDentroDePlazoAlarmaAutor(unaAlarma, unaFrase);
        }

        private bool EsUnaFraseAlertaNegativa(Frase unaFrase)
        {
            return unaFrase.UnaCategoria.ToLower() == "negativo";
        }

        private bool FechaDeFraseDentroDePlazoAlarmaAutor(AlarmaAutor unaAlarma, Frase unaFrase)
        {
            return PlazoCorrespondienteDias(unaAlarma, unaFrase) ||
                    PlazoCorrespondienteHoras(unaAlarma, unaFrase) ||
                    PlazoDiasHorasSonCero(unaAlarma, unaFrase);
        }

        private bool PlazoCorrespondienteDias(AlarmaAutor unaAlarma, Frase unaFrase)
        {
            return unaAlarma.PlazoDias != 0 && unaFrase.FechaDeIngreso.Date >= unaAlarma.CalcularFechaComienzo().Date;
        }

        private bool PlazoCorrespondienteHoras(AlarmaAutor unaAlarma, Frase unaFrase)
        {
            return unaAlarma.PlazoHoras != 0 && unaFrase.FechaDeIngreso.Hour >= unaAlarma.CalcularFechaComienzo().Hour;
        }

        private bool PlazoDiasHorasSonCero(AlarmaAutor unaAlarma, Frase unaFrase)
        {
            return unaAlarma.PlazoDias == 0 && unaAlarma.PlazoHoras == 0 &&
                   unaFrase.FechaDeIngreso.Date == DateTime.Today.Date;
        }

        private AlarmaAutor DevolverAlarmaAutorRegistrada()
        {
            return (AlarmaAutor)this.alarmaAutorARegistrar.DevolverAlarma();
        }

        public void BorrarAlarmasAutor()
        {
            this.alarmasAutorConfiguradas.BorrarTodasAlarmasAutoresDB();
        }

        public List<Alarma> DevolverAlarmasAutorActivadasPorFrases()
        {
            return BuscarAlarmasAutorActivas();
        }

        private List<Alarma> BuscarAlarmasAutorActivas()
        {
            List<Alarma> alarmasActivadas = new List<Alarma>();
            foreach (AlarmaAutor unaAlarma in this.alarmasAutorConfiguradas.DevolverAlarmasAutoresDB())
            {
                if (CumpleCriterioActivacion(unaAlarma))
                {
                    alarmasAutorConfiguradas.ActivarAlarmasAutorEnDB(unaAlarma);
                }
            }
            return alarmasActivadas;
        }

        private bool CumpleCriterioActivacion(AlarmaAutor unaAlarma)
        {
            bool cumpleCriterio = false;
            if (unaAlarma.UnTipo == AlarmaAutor.TipoAlarmaAutor.negativo)
            {
                cumpleCriterio = CantidadFrasesNegativasSuperaCantidadPostsAlarmaAutor(unaAlarma);
            }
            else
            {
                cumpleCriterio = CantidadFrasesPositivasSuperaCantidadPostsAlarmaAutor(unaAlarma);
            }
            return cumpleCriterio;
        }
    }
}
