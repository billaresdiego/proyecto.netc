﻿using ExcepcionesLogica;
using DominioNegocio;
using DominioNegocio.Excepciones;
using System.Collections.Generic;
using PersistenciaNegocio;

namespace LogicaNegocio
{
    public partial class Sistema : Fachada
    {
        private PersistenciaSentimiento sentimientos;

        public List<Sentimiento> DevolverSentimientos()
        {
            return sentimientos.DevolverSentimientos();
        }

        public void AgregarSentimiento(Sentimiento unSentimiento)
        {
            ValidarSentimiento(unSentimiento);
            this.sentimientos.AgregarSentimiento(unSentimiento);
        }

        private void ValidarSentimiento(Sentimiento unSentimiento)
        {
            if (TipoSentimientoIgualSinTipo(unSentimiento))
            {
                throw new ExcepcionSentimientoSinTipo();
            }
        }

        private bool TipoSentimientoIgualSinTipo(Sentimiento unSentimiento)
        {
            return unSentimiento.TipoSentimiento == Sentimiento.TiposSentimiento.sinTipo;
        }

        public Sentimiento DevolverSentimiento(Sentimiento unSentimiento)
        {
            return ComprobarObjetoNoVacio(unSentimiento);
        }

        private Sentimiento ComprobarObjetoNoVacio(Sentimiento unSentimiento)
        {
            if(this.sentimientos.DevolverSentimientoEnDB(unSentimiento) == null)
            {
                throw new ExcepcionSentimientoNoExistente();
            }
            return this.sentimientos.DevolverSentimientoEnDB(unSentimiento);
        }

        public void BorrarSentimiento(Sentimiento sentimientoABorrar)
        {
            sentimientos.BorrarSentimientoEnDB(sentimientoABorrar);
        }

        public void BorrarTodosSentimientos()
        {
            sentimientos.BorrarTodosSentimientosDB();
        }
    }
}
