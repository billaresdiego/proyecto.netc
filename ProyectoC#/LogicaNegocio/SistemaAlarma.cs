﻿using DominioNegocio;
using LogicaNegocio.UtilidadesValidarCondiciones;
using System;
using System.Collections.Generic;
using ExcepcionesLogica;
using PersistenciaNegocio;

namespace LogicaNegocio
{
    public partial class Sistema : Fachada
    {
        private PersistenciaAlarma alarmasConfiguradas;
        private ServidorAlarma alarmaARegistrar;

        public void AgregarAlarmaConfigurada(Alarma unaAlarma)
        {
            this.alarmasConfiguradas.AgregarAlarmaDB(unaAlarma);
        }

        public List<Alarma> DevolverAlarmasRegistradas()
        {
            List<Alarma> alarmasRegistradas = new List<Alarma>();
            foreach (Alarma unaAlarma in this.alarmasConfiguradas.DevolverAlarmasConEntidadDB())
            {
                alarmasRegistradas.Add(unaAlarma);
            }
            return alarmasRegistradas;
        }

        public Alarma DevolverAlarmaConfigurada(Alarma unaAlarma)
        {
            HerramientasSistema.ExisteAlarma(this.alarmasConfiguradas.DevolverAlarmasConEntidadDelTipoServidorAlarmasDB(), unaAlarma);
            return this.alarmasConfiguradas.DevolverAlarmaConEntidadEnDB(unaAlarma);
        }

        public Alarma AlarmaARegistrar
        {
            get
            {
                return DevolverAlarmaRegistrada();
            }
            set
            {
                this.alarmaARegistrar = value;
            }
        }

        
        public void AlertaXComentariosPositivosEnYPlazo()
        {
            if (CantidadFrasesPositivasSuperaCantidadPostsAlarma(DevolverAlarmaRegistrada()))
            {
                DevolverAlarmaRegistrada().Activada = true;
                throw new ExcepcionAlertaXComentariosPositivosYDias();
            }
        }

        private List<string> CantidadDeFrasesPositivas(Alarma unaAlarma)
        {
            List<string> sentimientosFrasesEntidadAlerta = new List<string>();
            foreach (Frase fraseActual in this.frases.DevolverFrasesConEntidadYFechaHorasDB())
            {
                if (FraseAlertaPositivaDentroDelPlazo(unaAlarma, fraseActual))
                {
                    sentimientosFrasesEntidadAlerta.Add(fraseActual.UnaCategoria);
                }
            }
            return sentimientosFrasesEntidadAlerta;
        }

        private bool FraseAlertaPositivaDentroDelPlazo(Alarma unaAlarma, Frase unaFrase)
        {
            return EsUnaFraseAlertaPositiva(unaAlarma, unaFrase) && FechaDeFraseDentroDePlazoAlarma(unaAlarma, unaFrase);
        }

        private bool EsUnaFraseAlertaPositiva(Alarma unaAlarma, Frase unaFrase)
        {
            return unaAlarma.UnaEntidad.Equals(unaFrase.UnaEntidad) && unaFrase.UnaCategoria.ToLower() == "positivo";
        }

        public List<Alarma> DevolverAlarmasActivadasPorFrases()
        {
            return BuscarAlarmasActivas();
        }

        private List<Alarma> BuscarAlarmasActivas()
        {
            List<Alarma> alarmasActivadas = new List<Alarma>();
            foreach (Alarma unaAlarma in this.alarmasConfiguradas.DevolverAlarmasConEntidadDB())
            {
                if (CumpleCriterioActivacion(unaAlarma))
                {
                    alarmasConfiguradas.ActivarAlarmasEnDB(unaAlarma);
                }
            }
            return alarmasActivadas;
        }

        private bool CumpleCriterioActivacion(Alarma unaAlarma)
        {
            bool cumpleCriterio = false;
            if(unaAlarma.UnTipo == Alarma.Tipo.negativo)
            {
                cumpleCriterio = CantidadFrasesNegativasSuperaCantidadPostsAlarma(unaAlarma);
            }
            else
            {
                cumpleCriterio = CantidadFrasesPositivasSuperaCantidadPostsAlarma(unaAlarma);
            }
            return cumpleCriterio;
        }

        public void AlertaXComentariosNegativosEnYPlazo()
        {
            if (CantidadFrasesNegativasSuperaCantidadPostsAlarma(DevolverAlarmaRegistrada()))
            {
                DevolverAlarmaRegistrada().Activada = true;
                throw new ExcepcionAlertaXComentariosNegativosYDias();
            }
        }


        private bool CantidadFrasesNegativasSuperaCantidadPostsAlarma(Alarma unaAlarma)
        {
            return unaAlarma.CantidadDePosts <= CantidadDeFrasesNegativas(unaAlarma).Count;
        }

        private List<string> CantidadDeFrasesNegativas(Alarma unaAlarma)
        {
            List<string> sentimientosFrasesEntidadAlerta = new List<string>();
            foreach (Frase fraseActual in this.frases.DevolverFrasesConEntidadYFechaHorasDB())
            {
                if (FraseAlertaNegativaDentroDelPlazo(unaAlarma, fraseActual))
                {
                    sentimientosFrasesEntidadAlerta.Add(fraseActual.UnaCategoria);
                }
            }
            return sentimientosFrasesEntidadAlerta;
        }

        private bool FraseAlertaNegativaDentroDelPlazo(Alarma unaAlarma, Frase unaFrase)
        {
            return EsUnaFraseAlertaNegativa(unaAlarma, unaFrase) && FechaDeFraseDentroDePlazoAlarma(unaAlarma, unaFrase);
        }

        private bool EsUnaFraseAlertaNegativa(Alarma unaAlarma, Frase unaFrase)
        {
            return unaAlarma.UnaEntidad.Equals(unaFrase.UnaEntidad) && unaFrase.UnaCategoria.ToLower() == "negativo";
        }

        private bool FechaDeFraseDentroDePlazoAlarma(Alarma unaAlarma, Frase unaFrase)
        {
            return  PlazoCorrespondienteDias(unaAlarma, unaFrase) || 
                    PlazoCorrespondienteHoras(unaAlarma, unaFrase) ||
                    PlazoDiasHorasSonCero(unaAlarma, unaFrase);
        }

        private bool PlazoCorrespondienteDias(Alarma unaAlarma, Frase unaFrase)
        {
            return unaAlarma.PlazoDias != 0 && unaFrase.FechaDeIngreso.Date >= unaAlarma.CalcularFechaComienzo().Date;
        }

        private bool PlazoCorrespondienteHoras(Alarma unaAlarma, Frase unaFrase)
        {
            return unaAlarma.PlazoHoras != 0 && unaFrase.FechaDeIngreso.Hour >= unaAlarma.CalcularFechaComienzo().Hour;
        }

        private bool PlazoDiasHorasSonCero(Alarma unaAlarma, Frase unaFrase)
        {
            return unaAlarma.PlazoDias == 0 && unaAlarma.PlazoHoras == 0 && 
                   unaFrase.FechaDeIngreso.Date == DateTime.Today.Date;
        }

        private bool CantidadFrasesPositivasSuperaCantidadPostsAlarma(Alarma unaAlarma)
        {
            return unaAlarma.CantidadDePosts <= CantidadDeFrasesPositivas(unaAlarma).Count;
        }

        private Alarma DevolverAlarmaRegistrada()
        {
            return (Alarma)this.alarmaARegistrar.DevolverAlarma();
        }

        public void BorrarTodasLasAlarmas()
        {
            this.alarmasConfiguradas.BorrarTodasAlarmasDB();
        }
    }
}
