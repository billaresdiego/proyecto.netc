﻿using System.Collections.Generic;
using DominioNegocio;

namespace LogicaNegocio
{
    interface Fachada
    {
        Alarma AlarmaARegistrar { get; set; }
        AlarmaAutor AlarmaAutorARegistrar { get; set; }
        void AlertaXComentariosNegativosEnYPlazo();
        void AlertaXFrasesPositivasEnYPlazoAlarmaAutor();
        void AlertaXFrasesNegativasEnYPlazoAlarmaAutor();
        List<Entidad> DevolverEntidades();
        void AgregarEntidad(Entidad entidad);
        List<Sentimiento> DevolverSentimientos();
        void AgregarSentimiento(Sentimiento unSentimiento);
        void AgregarFrase(Frase frase);
        void AgregarAutor(Autor unAutor);
        Alarma DevolverAlarmaConfigurada(Alarma alarma);
        AlarmaAutor DevolverAlarmaAutorConfigurada(AlarmaAutor alarma);
        void AgregarAlarmaConfigurada(Alarma alarmaARegistrar);
        void AgregarAlarmaAutorConfigurada(AlarmaAutor unaAlarmaAutor);
        List<Alarma> DevolverAlarmasActivadasPorFrases();
        List<AlarmaAutor> DevolverAlarmasAutorActivadasPorFrases();
        List<Frase> DevolverFrases();
        List<Frase> DevolverFrasesConEntidadYFechaYHora();
        List<Autor> DevolverAutores();
        Autor DevolverAutor(Autor unAutor);
        void BorrarAutor(Autor autorABorrar);
        void ModificarAutorEnSistema(Autor autorAModificar, Autor autorParametrosModificados);
        void BorrarSentimiento(Sentimiento sentimientoABorrar);
        void AlertaXComentariosPositivosEnYPlazo();
        List<Alarma> DevolverAlarmasRegistradas();
        List<AlarmaAutor> DevolverAlarmasAutorRegistradas();
        void BorrarTodasFrases();
        void BorrarTodosSentimientos();
        void BorrarTodasEntidades();
        void BorrarTodosLosAutores();
        void BorrarTodasLasAlarmas();
        void BorrarAlarmasAutor();
    }
}
