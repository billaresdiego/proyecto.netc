﻿using ExcepcionesLogica;
using DominioNegocio;
using PersistenciaNegocio;
using System.Collections.Generic;

namespace LogicaNegocio
{
    public partial class Sistema : Fachada
    {
        private PersistenciaEntidad entidades;

        public List<Entidad> DevolverEntidades()
        {
            return entidades.DevolverEntidades();
        }

        public void AgregarEntidad(Entidad unaEntidad)
        {
            this.entidades.AgregarEntidad(unaEntidad);
        }

        public Entidad DevolverEntidad(Entidad unaEntidad)
        {
            ComprobarExistenciaEntidad(unaEntidad);
            return entidades.DevolverEntidadEnDB(unaEntidad);
        }

        private void ComprobarExistenciaEntidad(Entidad unaEntidad)
        {
            if (this.entidades.DevolverEntidadEnDB(unaEntidad) == null)
            {
                throw new ExcepcionEntidadNoExistente();
            }
        }

        public void BorrarTodasEntidades()
        {
            entidades.BorrarTodasEntidadesDB();
        }
    }
}
