﻿using DominioNegocio;
using System.Collections.Generic;

namespace LogicaNegocio
{
    public partial class FachadaUsuario
    {
    
        Fachada fachada = new Sistema();

        public void CrearEntidadEnSistema(string nombreEntidad)
        {
            Entidad entidad = new Entidad(nombreEntidad);
            fachada.AgregarEntidad(entidad);
        }

        public List<Entidad> DevolverEntidades()
        {
            return fachada.DevolverEntidades();
        }

        public void BorrarTodasEntidades()
        {
            fachada.BorrarTodasEntidades();
        }
    }
}