﻿using System;
using System.Collections.Generic;
using DominioNegocio;
using DominioNegocio.Excepciones;
using ExcepcionesLogica;

namespace LogicaNegocio
{
    public partial class FachadaUsuario
    {
        public int ConvertirStringAInt(string unString)
        {
            try
            {
                return Int32.Parse(unString);
            }
            catch (Exception)
            {
                throw new ExcepcionElStringAConvertirNoEsValido();
            }
        }

        public List<Alarma> DevolverAlarmasRegistradas()
        {
            return this.fachada.DevolverAlarmasRegistradas();
        }

        public Alarma DevolverAlarmaConfigurada(Alarma unaAlarma)
        {
            return this.fachada.DevolverAlarmaConfigurada(unaAlarma);
        }

        public List<Alarma> DevolverAlarmasActivadasPorFrases()
        {
            return this.fachada.DevolverAlarmasActivadasPorFrases();
        }

        public Alarma MostrarAlarmaARegistrar()
        {
            return fachada.AlarmaARegistrar;
        }

        public void ConfigurarAlarma(Entidad entidad, int cantidadDePost, int plazoDiasUHoras,
                                        string tipoAlarma, string formatoEnDiasUHoras)
        {
            fachada.AlarmaARegistrar = new Alarma
            {
                UnaEntidad = entidad,
                CantidadDePosts = cantidadDePost,
                Activada = false
            };
            EstablecerTipoAlarmaEnum(tipoAlarma);
            ConfigurarPlazoSegun(formatoEnDiasUHoras, plazoDiasUHoras);
            try
            {
                AlertaFrases();
                fachada.AgregarAlarmaConfigurada(fachada.AlarmaARegistrar);
            }
            catch (Exception)
            {
                fachada.AgregarAlarmaConfigurada(fachada.AlarmaARegistrar);
                throw new ExcepcionAlertaXComentariosPositivosYDias();
            }
        }

        private void EstablecerTipoAlarmaEnum(string unTipo)
        {
            if (unTipo.ToLower() == "positiva" || unTipo.ToLower() == "positivo")
            {
                fachada.AlarmaARegistrar.UnTipo = Alarma.Tipo.positivo;
            }
            else
            {
                fachada.AlarmaARegistrar.UnTipo = Alarma.Tipo.negativo;
            }
        }

        private void ConfigurarPlazoSegun(string formatoEnDiasUHoras, int plazoDiasUHoras)
        {
            if (FormatoEnDias(formatoEnDiasUHoras))
            {
                fachada.AlarmaARegistrar.PlazoDias = plazoDiasUHoras;
                fachada.AlarmaARegistrar.PlazoHoras = 0;
            }
            if (FormatoEnHoras(formatoEnDiasUHoras))
            {
                fachada.AlarmaARegistrar.PlazoHoras = plazoDiasUHoras;
                fachada.AlarmaARegistrar.PlazoDias = 0;
            }
        }

        private bool FormatoEnDias(string formatoEnDiasUHoras)
        {
            return formatoEnDiasUHoras.ToLower() == "dias";
        }

        private bool FormatoEnHoras(string formatoEnDiasUHoras)
        {
            return formatoEnDiasUHoras.ToLower() == "horas";
        }

        private void AlertaFrases()
        {
            if (fachada.AlarmaARegistrar.UnTipo == Alarma.Tipo.positivo)
            {
                AlertaFrasesPositivas();
            }
            else if (fachada.AlarmaARegistrar.UnTipo == Alarma.Tipo.negativo)
            {
                AlertaFrasesNegativas();
            }
        }

        private void AlertaFrasesPositivas()
        {
            fachada.AlertaXComentariosPositivosEnYPlazo();
        }

        private void AlertaFrasesNegativas()
        {
            fachada.AlertaXComentariosNegativosEnYPlazo();
        }

        public void BorrarTodasLasAlarmas()
        {
            fachada.BorrarTodasLasAlarmas();
        }
    }
}
