﻿using System.Linq;

namespace LogicaNegocio.UtilidadesValidarCondiciones
{
    class HerramientasValidacionStrings
    {
        public static bool StringVacio(string cadenaCaracteres)
        {
            return cadenaCaracteres.Length == 0;
        }

        public static bool StringLargoMenorAlEspecificado(string cadenaCaracteres, int largo)
        {
            return cadenaCaracteres.Length < largo;
        }

        public static bool StringContieneCaracterEspecificado (string cadenaCaracteres, char caracter)
        {
            return cadenaCaracteres.Contains(caracter);
        }
    }
}
