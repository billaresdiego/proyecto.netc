﻿using System;

namespace LogicaNegocio.UtilidadesValidarCondiciones
{
    class HerramientasValidacionObjetos
    {
        public static bool validarObjetoDeEquals(Object unObjeto, Object unObjectValidar)
        {
            return unObjeto == null || !unObjectValidar.GetType().Equals(unObjeto.GetType());
        }

    }
}
