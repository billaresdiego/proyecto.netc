﻿using ExcepcionesLogica;
using DominioNegocio;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LogicaNegocio.UtilidadesValidarCondiciones
{
    class HerramientasSistema
    {
        public static void ExisteAlarma(List<ServidorAlarma> alarmas, Object unaAlarma)
        {
            if (!ContieneAlarma(alarmas, unaAlarma))
            {
                throw new ExcepcionAlarmaNoExistente();
            }
        }

        private static bool ContieneAlarma(List<ServidorAlarma> alarmas, Object unaAlarma)
        {
            return alarmas.Contains(unaAlarma);
        }

        public static int BuscarAlarma(List<ServidorAlarma> alarmas, Object unaAlarma)
        {
            return alarmas.IndexOf((ServidorAlarma)unaAlarma);
        }
    }
}
