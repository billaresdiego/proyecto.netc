﻿namespace LogicaNegocio.UtilidadesValidarCondiciones
{
    class HerramientasValidacionEnteros
    {
        public static bool EsIgualACero(int numero)
        {
            return numero == 0;
        }
    }
}
