﻿using ExcepcionesLogica;
using DominioNegocio;
using System.Collections.Generic;
using PersistenciaNegocio;

namespace LogicaNegocio
{
    public partial class Sistema : Fachada
    {
        private PersistenciaFrase frases;

        public Sistema()
        {
            this.entidades = new PersistenciaEntidad();
            this.sentimientos = new PersistenciaSentimiento();
            this.frases = new PersistenciaFrase();
            this.alarmaARegistrar = new Alarma();
            this.alarmasConfiguradas = new PersistenciaAlarma();
            this.autores = new PersistenciaAutor();
            this.alarmaAutorARegistrar = new AlarmaAutor();
            this.alarmasAutorConfiguradas = new PersistenciaAlarmaAutor();
        }

        public void AgregarFrase(Frase unaFrase)
        {
            this.frases.AgregarFraseDB(unaFrase);
        }

        public Frase DevolverFrase(Frase unaFrase)
        {
            ComprobarExistenciaFrase(unaFrase);
            return frases.DevolverFraseEnDB(unaFrase);
        }

        private void ComprobarExistenciaFrase(Frase unaFrase)
        {
            if (this.frases.DevolverFraseEnDB(unaFrase) == null)
            {
                throw new ExcepcionFraseNoExistente();
            }
        }

        public List<Frase> DevolverFrases()
        {
            return this.frases.DevolverFrasesDB();
        }

        public void BorrarTodasFrases()
        {
            this.frases.BorrarTodasFrasesDB();
        }

        public List<Frase> DevolverFrasesConEntidadYFechaYHora()
        {
            return this.frases.DevolverFrasesConEntidadYFechaHorasDB();
        }
    }
}
