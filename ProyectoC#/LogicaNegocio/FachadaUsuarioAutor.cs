﻿using DominioNegocio;
using DominioNegocio.Excepciones;
using System;
using System.Collections.Generic;

namespace LogicaNegocio
{
    public partial class FachadaUsuario
    {
        public void CrearAutorEnSistema(String nombre, String apellido, String usuario, DateTime fechaNacimiento)
        {
            Autor autorAAgregar = new Autor
            {
                Nombre = nombre,
                Apellido = apellido,
                Usuario = usuario,
                FechaNacimiento = fechaNacimiento
            };
            fachada.AgregarAutor(autorAAgregar);
        }

        public List<Autor> DevolverAutores()
        {
            return fachada.DevolverAutores();
        }

        public void BorrarTodosLosAutores()
        {
            fachada.BorrarTodosLosAutores();
        }

        public void BorrarAutor(String usuarioABorrar)
        {
            Autor autorABorrar = new Autor
            {
                Nombre = "nombre",
                Apellido = "apellido",
                Usuario = usuarioABorrar,
                FechaNacimiento = DateTime.Now.AddYears(-50)
            };
            if (!AutorTieneFrases(autorABorrar))
            {
                fachada.BorrarAutor(autorABorrar);
            }
            else
            {
                throw new ExcepcionAutorNoSePuedeBorrar();
            }
        }

        private bool AutorTieneFrases(Autor autorABorrar)
        {
            List<Frase> listaFrases = fachada.DevolverFrasesConEntidadYFechaYHora();
            for (int i = 0; i < listaFrases.Count; i++)
            {
                if (listaFrases[i].Autor.Equals(autorABorrar))
                    return true;
            }
            return false;
        }

        public void ModificarAutor(Autor autorAModificar, Autor autorParametrosModificados)
        {
            fachada.ModificarAutorEnSistema(autorAModificar, autorParametrosModificados);
        }

        public double CalcularPorcentajeFrasesPositivasAutor(Autor autor)
        {
            double cantidadFrases = 0;
            double cantidadFrasesPositivas = 0;
            List<Frase> listaFrasesDelAutor = DevolverListaFrasesDelAutor(autor);
            for (int i = 0; i < listaFrasesDelAutor.Count; i++)
            {
                cantidadFrases++;
                if (listaFrasesDelAutor[i].UnaCategoria.Equals("Positivo"))
                {
                    cantidadFrasesPositivas++;
                }
            }
            double resultado = (cantidadFrasesPositivas / cantidadFrases) * 100;
            return resultado;
        }

        public int CalcularCantidadEntidadesMencionadas(Autor autor)
        {
            List<string> listaEntidadesMencionadas = DevolverListaEntidadesMencionadas(autor);
            return listaEntidadesMencionadas.Count;
        }

        private List<string> DevolverListaEntidadesMencionadas(Autor autor)
        {
            List<Frase> listaTodasLasFrases = fachada.DevolverFrasesConEntidadYFechaYHora();
            List<string> listaEntidadesMencionadas = new List<string>();
            for (int i = 0; i < listaTodasLasFrases.Count; i++)
            {
                if (listaTodasLasFrases[i].Autor.Equals(autor))
                {
                    if (!listaEntidadesMencionadas.Contains(listaTodasLasFrases[i].UnaEntidad.NombreUnaEntidad))
                    {
                        listaEntidadesMencionadas.Add(listaTodasLasFrases[i].UnaEntidad.NombreUnaEntidad);
                    }
                }
            }
            return listaEntidadesMencionadas;
        }

        public double CalcularPromedioFrasesDiarias(Autor autor)
        {
            List<Frase> listaFrasesDeAutor = DevolverListaFrasesDelAutor(autor);
            DateTime fechaMasAntigua = DevolverFechaFraseMasAntigua(listaFrasesDeAutor);
            TimeSpan tiempoConcurrido = DateTime.Now - fechaMasAntigua;
            double cantidadDeDiasDesdeFraseMasAntigua = tiempoConcurrido.Days + 1;
            double promedio = listaFrasesDeAutor.Count / cantidadDeDiasDesdeFraseMasAntigua;
            return promedio;
        }

        private List<Frase> DevolverListaFrasesDelAutor(Autor autor)
        {
            List<Frase> listaTodasLasFrases = fachada.DevolverFrasesConEntidadYFechaYHora();
            List<Frase> listaFrasesDeAutor = new List<Frase>();
            for (int i = 0; i < listaTodasLasFrases.Count; i++)
            {
                if (listaTodasLasFrases[i].Autor.Equals(autor))
                {
                    listaFrasesDeAutor.Add(listaTodasLasFrases[i]);
                }
            }

            return listaFrasesDeAutor;
        }

        private static DateTime DevolverFechaFraseMasAntigua(List<Frase> listaFrasesDeAutor)
        {
            DateTime fechaMasAntigua = DateTime.Now;
            for (int i = 0; i < listaFrasesDeAutor.Count; i++)
            {
                if (listaFrasesDeAutor[i].FechaDeIngreso.CompareTo(fechaMasAntigua) < 0)
                {
                    fechaMasAntigua = listaFrasesDeAutor[i].FechaDeIngreso;
                }
            }

            return fechaMasAntigua;
        }
    }
}
