﻿using System;

namespace ExcepcionesLogica
{
    public class ExcepcionAlertaXComentariosNegativosYDias : Exception
    {
        public ExcepcionAlertaXComentariosNegativosYDias() : base("Aviso: alarma activada.")
        {
        }
    }

    public class ExcepcionAlertaXComentariosPositivosYDias : Exception
    {
        public ExcepcionAlertaXComentariosPositivosYDias() : base("Aviso: alarma activada.")
        {
        }
    }

    public class ExcepcionFraseSinEntidadExistente : Exception
    {
        public ExcepcionFraseSinEntidadExistente() : base("Aviso: no se encontró una entidad en la frase.")
        {
        }
    }

    public class ExcepcionFraseSinSentimientoExistente : Exception
    {
        public ExcepcionFraseSinSentimientoExistente() : base("Aviso: no se encontró un sentimiento en la frase.")
        {
        }
    }
}
