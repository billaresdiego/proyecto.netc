﻿using System;

namespace ExcepcionesLogica
{
    public class ExcepcionEntidadNoExistente : Exception
    {
        public ExcepcionEntidadNoExistente() : base("Aviso: la entidad no existe.")
        {
        }
    }

    public class ExcepcionEntidadExistente : Exception
    {
        public ExcepcionEntidadExistente() : base("Aviso: la entidad que desea ingresar ya existe.")
        {
        }
    }

    public class ExcepcionSentimientoNoExistente : Exception
    {
        public ExcepcionSentimientoNoExistente() : base("Aviso: el sentimiento no existe.")
        {
        }
    }

    public class ExcepcionSentimientoExistente : Exception
    {
        public ExcepcionSentimientoExistente() : base("Aviso: el sentimiento que desea ingresar ya existe.")
        {
        }
    }

    public class ExcepcionFraseNoExistente : Exception
    {
        public ExcepcionFraseNoExistente() : base("Aviso: la frase no existe.")
        {
        }
    }

    public class ExcepcionErrorAlAgregarFrase : Exception
    {
        public ExcepcionErrorAlAgregarFrase() : base("Aviso: se generó un error al intentar agregar la frase.")
        {
        }
    }

    public class ExcepcionAlarmaNoExistente : Exception
    {
        public ExcepcionAlarmaNoExistente() : base("Aviso: la alarma no existe.")
        {
        }
    }

    public class ExcepcionErrorAlAgregarAlarma : Exception
    {
        public ExcepcionErrorAlAgregarAlarma() : base("Aviso: se generó un error al intentar agregar la alarma.")
        {
        }
    }

    public class ExcepcionAutorNoExistente : Exception
    {
        public ExcepcionAutorNoExistente() : base("Aviso: el autor no existe.")
        {
        }
    }

    public class ExcepcionAutorExistente : Exception
    {
        public ExcepcionAutorExistente() : base("Aviso: el autor ya existe.")
        {
        }
    }

}

