﻿using DominioNegocio;
using ExcepcionesLogica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using DominioNegocio.Excepciones;

namespace PersistenciaNegocio
{
    public class PersistenciaAlarma
    {
        public Alarma DevolverAlarmaEnDB(Alarma unaAlarma)
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    return BuscarAlarmaEnDB(unContexto, unaAlarma);
                }
                catch (Exception)
                {
                    throw new ExcepcionAlarmaNoExistente();
                }
            }
        }

        public Alarma BuscarAlarmaEnDB(ContextoNegocio unContexto, Alarma unaAlarma)
        {
            return unContexto.Alarmas.Find(unaAlarma.Id);
        }

        public Alarma DevolverAlarmaConEntidadEnDB(Alarma unaAlarma)
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    return BuscarPrimeraAparicionAlarmaEnDB(unContexto, unaAlarma);
                }
                catch (Exception)
                {
                    throw new ExcepcionAlarmaNoExistente();
                }
            }
        }

        public Alarma BuscarPrimeraAparicionAlarmaEnDB(ContextoNegocio unContexto, Alarma unaAlarma)
        {
            return unContexto.Alarmas.Include(alarma => alarma.UnaEntidad)
                                     .First<Alarma>(alarma => alarma.Id == unaAlarma.Id); 
        }

        public void AgregarAlarmaDB(Alarma unaAlarma)
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    EstablecerAlarmaEnDB(unContexto, unaAlarma);
                }
                catch (Exception)
                {
                    throw new ExcepcionErrorAlAgregarAlarma();
                }
            }
        }

        private void EstablecerAlarmaEnDB(ContextoNegocio unContexto, Alarma unaAlarma)
        {
            Entidad entidadEnDB = unContexto.Entidades.Find(unaAlarma.UnaEntidad.NombreUnaEntidad);
            unaAlarma.UnaEntidad = entidadEnDB;
            unContexto.Alarmas.Add(unaAlarma);
            unContexto.SaveChanges();
        }

        public List<ServidorAlarma> DevolverAlarmasConEntidadDelTipoServidorAlarmasDB()
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    return GenerarListaAlarmasConEntidadDelTipoServidorAlarmasDB(unContexto);
                }
                catch (Exception)
                {
                    throw new ExcepcionAlarmaNoExistente();
                }
            }
        }

        private List<ServidorAlarma> GenerarListaAlarmasConEntidadDelTipoServidorAlarmasDB(ContextoNegocio unContexto)
        {
            return unContexto.Alarmas.Include(alarma => alarma.UnaEntidad).ToList<ServidorAlarma>();
        }

        public List<Alarma> DevolverAlarmasConEntidadDB()
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    return GenerarListaAlarmasConEntidadDB(unContexto);
                }
                catch (Exception)
                {
                    throw new ExcepcionAlarmaNoExistente();
                }
            }
        }

        private List<Alarma> GenerarListaAlarmasConEntidadDB(ContextoNegocio unContexto)
        {
            return unContexto.Alarmas.Include(alarma => alarma.UnaEntidad).ToList<Alarma>();
        }

        public List<Alarma> DevolverAlarmasDB()
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    return GenerarListaAlarmasDB(unContexto);
                }
                catch (Exception)
                {
                    throw new ExcepcionAlarmaNoExistente();
                }
            }
        }

        public void BorrarTodasAlarmasDB()
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    RecorrerAlarmasABorrar(unContexto);
                    ResetearIdTablasFisicaAlarma(unContexto);
                    unContexto.SaveChanges();
                }
                catch (Exception)
                {
                    throw new ExcepcionAlarmaNoSePuedeBorrar();
                }
            }
        }

        private void RecorrerAlarmasABorrar(ContextoNegocio unContexto)
        {
            foreach (Alarma unaAlarma in GenerarListaAlarmasDB(unContexto))
            {
                EliminarAlarmaDB(unContexto, unaAlarma);
            }
        }

        private List<Alarma> GenerarListaAlarmasDB(ContextoNegocio unContexto)
        {
            return unContexto.Alarmas.ToList<Alarma>();
        }

        private void EliminarAlarmaDB(ContextoNegocio unContexto, Alarma unaAlarma)
        {
            unContexto.Alarmas.Remove(unaAlarma);
        }

        private void ResetearIdTablasFisicaAlarma(ContextoNegocio unContexto)
        {
            unContexto.Database.ExecuteSqlCommand("DBCC CHECKIDENT('Alarma_table', RESEED, 0)");
        }

        public void ActivarAlarmasEnDB(Alarma alarmaAModificar)
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                Alarma alarmaAModEnDB = BuscarAlarmaEnDB(unContexto, alarmaAModificar);
                ModificarAtributo(alarmaAModEnDB);
                unContexto.SaveChanges();
            }
        }

        private static void ModificarAtributo(Alarma alarmaAModEnDB)
        {
            alarmaAModEnDB.Activada = true;
        }
    }
}
