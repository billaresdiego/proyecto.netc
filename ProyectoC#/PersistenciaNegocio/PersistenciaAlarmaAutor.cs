﻿using DominioNegocio;
using DominioNegocio.Excepciones;
using ExcepcionesLogica;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PersistenciaNegocio
{
    public class PersistenciaAlarmaAutor
    {
        public AlarmaAutor DevolverAlarmaAutorEnDB(AlarmaAutor unaAlarma)
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    return BuscarAlarmaAutorEnDB(unContexto, unaAlarma);
                }
                catch (Exception)
                {
                    throw new ExcepcionAlarmaNoExistente();
                }
            }
        }

        public AlarmaAutor BuscarAlarmaAutorEnDB(ContextoNegocio unContexto, AlarmaAutor unaAlarma)
        {
            return unContexto.AlarmasAutores.Find(unaAlarma.Id);
        }

        public void AgregarAlarmaAutorDB(AlarmaAutor unaAlarma)
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    EstablecerAlarmaAutorEnDB(unContexto, unaAlarma);
                }
                catch (Exception)
                {
                    throw new ExcepcionErrorAlAgregarAlarma();
                }
            }
        }

        private void EstablecerAlarmaAutorEnDB(ContextoNegocio unContexto, AlarmaAutor unaAlarma)
        {
            unContexto.AlarmasAutores.Add(unaAlarma);
            unContexto.SaveChanges();
        }

        public List<ServidorAlarma> DevolverAlarmasAutoresDelTipoServidorAlarmasDB()
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    return GenerarListaAlarmasAutoresDelTipoServidorAlarmasDB(unContexto);
                }
                catch (Exception)
                {
                    throw new ExcepcionAlarmaNoExistente();
                }
            }
        }

        private List<ServidorAlarma> GenerarListaAlarmasAutoresDelTipoServidorAlarmasDB(ContextoNegocio unContexto)
        {
            return unContexto.AlarmasAutores.ToList<ServidorAlarma>();
        }

        public List<AlarmaAutor> DevolverAlarmasAutoresDB()
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    return GenerarListaAlarmasAutoresDB(unContexto);
                }
                catch (Exception)
                {
                    throw new ExcepcionAlarmaNoExistente();
                }
            }
        }

        public void BorrarTodasAlarmasAutoresDB()
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    RecorrerAlarmasAutoresABorrar(unContexto);
                    ResetearIdTablasFisicaAlarmaAutor(unContexto);
                    unContexto.SaveChanges();
                }
                catch (Exception)
                {
                    throw new ExcepcionAlarmaNoSePuedeBorrar();
                }
            }
        }

        private void RecorrerAlarmasAutoresABorrar(ContextoNegocio unContexto)
        {
            foreach (AlarmaAutor unaAlarma in GenerarListaAlarmasAutoresDB(unContexto))
            {
                EliminarAlarmaAutoresDB(unContexto, unaAlarma);
            }
        }

        private List<AlarmaAutor> GenerarListaAlarmasAutoresDB(ContextoNegocio unContexto)
        {
            return unContexto.AlarmasAutores.ToList<AlarmaAutor>();
        }

        private void EliminarAlarmaAutoresDB(ContextoNegocio unContexto, AlarmaAutor unaAlarma)
        {
            unContexto.AlarmasAutores.Remove(unaAlarma);
        }

        private void ResetearIdTablasFisicaAlarmaAutor(ContextoNegocio unContexto)
        {
            unContexto.Database.ExecuteSqlCommand("DBCC CHECKIDENT('AlarmasAutores_table', RESEED, 0)");
        }

        public void ActivarAlarmasAutorEnDB(AlarmaAutor alarmaAutorAModificar)
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                AlarmaAutor alarmaAutorAModEnDB = BuscarAlarmaAutorEnDB(unContexto, alarmaAutorAModificar);
                ModificarAtributo(alarmaAutorAModEnDB);
                unContexto.SaveChanges();
            }
        }

        private static void ModificarAtributo(AlarmaAutor alarmaAutorAModEnDB)
        {
            alarmaAutorAModEnDB.Activada = true;
        }
    }
}
