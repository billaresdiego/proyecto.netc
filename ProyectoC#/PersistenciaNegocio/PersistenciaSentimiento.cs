﻿using DominioNegocio;
using DominioNegocio.Excepciones;
using ExcepcionesLogica;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PersistenciaNegocio
{
    public class PersistenciaSentimiento
    {
        public Sentimiento DevolverSentimientoEnDB(Sentimiento unSentimiento)
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    return BuscarSentimientoEnDB(unContexto, unSentimiento);
                }
                catch (Exception)
                {
                    throw new ExcepcionSentimientoNoExistente();
                }
            }
        }

        public Sentimiento BuscarSentimientoEnDB(ContextoNegocio unContexto, Sentimiento unSentimiento)
        {
            return unContexto.Sentimientos.Find(unSentimiento.NombreSentimiento);
        }

        public void AgregarSentimiento(Sentimiento unSentimiento)
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    EstablecerSentimientoEnDB(unContexto, unSentimiento);
                }
                catch (Exception)
                {
                    throw new ExcepcionSentimientoExistente();
                }
            }
        }

        private void EstablecerSentimientoEnDB(ContextoNegocio unContexto, Sentimiento unSentimiento)
        {
            unContexto.Sentimientos.Add(unSentimiento);
            unContexto.SaveChanges();
        }

        public List<Sentimiento> DevolverSentimientos()
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    return GenerarListaSentimientoDeDB(unContexto);
                }
                catch (Exception)
                {
                    throw new ExcepcionSentimientoExistente();
                }
            }
        }

        public void BorrarSentimientoEnDB(Sentimiento unSentimiento)
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    unContexto.Sentimientos.Attach(unSentimiento);
                    EliminarSentimientoDB(unContexto, unSentimiento);
                }
                catch (Exception)
                {
                    throw new ExcepcionSentimientoNoSePuedeBorrar();
                }
            }
        }

        public void BorrarTodosSentimientosDB()
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    RecorrerSentimientosABorrar(unContexto);
                }
                catch (Exception e)
                {
                    throw new ExcepcionSentimientoNoSePuedeBorrar();
                }
            }
        }

        private void RecorrerSentimientosABorrar(ContextoNegocio unContexto)
        {
            foreach (Sentimiento unSentimiento in GenerarListaSentimientoDeDB(unContexto))
            {
                EliminarSentimientoDB(unContexto, unSentimiento);
            }
        }

        private List<Sentimiento> GenerarListaSentimientoDeDB(ContextoNegocio unContexto)
        {
            return unContexto.Sentimientos.ToList();
        }

        private void EliminarSentimientoDB(ContextoNegocio unContexto, Sentimiento unSentimiento)
        {
            unContexto.Sentimientos.Remove(unSentimiento);
            unContexto.SaveChanges();
        }
    }
}
