﻿using DominioNegocio;
using DominioNegocio.Excepciones;
using ExcepcionesLogica;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PersistenciaNegocio
{
    public class PersistenciaEntidad
    {
        public Entidad DevolverEntidadEnDB(Entidad unaEntidad)
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    return BuscarEntidadEnDB(unContexto, unaEntidad);
                }
                catch (Exception)
                {
                    throw new ExcepcionEntidadNoExistente();
                }
            }
        }

        public Entidad BuscarEntidadEnDB(ContextoNegocio unContexto, Entidad unaEntidad)
        {
            return unContexto.Entidades.Find(unaEntidad.NombreUnaEntidad);
        }

        public void AgregarEntidad(Entidad unaEntidad)
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    EstablecerEntidadEnDB(unContexto, unaEntidad);
                }
                catch (Exception)
                {
                    throw new ExcepcionEntidadExistente();
                }
            }
        }

        private void EstablecerEntidadEnDB(ContextoNegocio unContexto, Entidad unaEntidad)
        {
            unContexto.Entidades.Add(unaEntidad);
            unContexto.SaveChanges();
        }

        public List<Entidad> DevolverEntidades()
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    return GenerarListaEntidadesDeDB(unContexto);
                }
                catch (Exception)
                {
                    throw new ExcepcionEntidadExistente();
                }
            }
        }

        public void BorrarTodasEntidadesDB()
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    RecorrerEntidadesABorrar(unContexto);
                }
                catch (Exception)
                {
                    throw new ExcepcionEntidadNoSePuedeBorrar();
                }
            }
        }

        private void RecorrerEntidadesABorrar(ContextoNegocio unContexto)
        {
            foreach (Entidad unaEntidad in GenerarListaEntidadesDeDB(unContexto))
            {
                EliminarEntidadDB(unContexto, unaEntidad);
            }
        }

        private List<Entidad> GenerarListaEntidadesDeDB(ContextoNegocio unContexto)
        {
            return unContexto.Entidades.ToList();
        }

        private void EliminarEntidadDB(ContextoNegocio unContexto, Entidad unaEntidad)
        {
            unContexto.Entidades.Remove(unaEntidad);
            unContexto.SaveChanges();
        }
    }
}
