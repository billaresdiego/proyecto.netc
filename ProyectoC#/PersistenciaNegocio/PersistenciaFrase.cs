﻿using DominioNegocio;
using DominioNegocio.Excepciones;
using ExcepcionesLogica;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;

namespace PersistenciaNegocio
{
    public class PersistenciaFrase
    {
        public Frase DevolverFraseEnDB(Frase unaFrase)
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    return BuscarFraseEnDB(unContexto, unaFrase);
                }
                catch (Exception)
                {
                    throw new ExcepcionFraseNoExistente();
                }
            }
        }

        public Frase BuscarFraseEnDB(ContextoNegocio unContexto, Frase unaFrase)
        {
            return unContexto.Frases.Find(unaFrase.Id);
        }

        public void AgregarFraseDB(Frase unaFrase)
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    EstablecerFraseEnDB(unContexto, unaFrase);
                }
                catch (Exception)
                {
                    throw new ExcepcionErrorAlAgregarFrase();
                }
            }
        }

        private void EstablecerFraseEnDB(ContextoNegocio unContexto, Frase unaFrase)
        {
            Entidad entidadEnDB = unContexto.Entidades.Find(unaFrase.UnaEntidad.NombreUnaEntidad);
            unaFrase.UnaEntidad = entidadEnDB;
            Autor autorEnDB = unContexto.Autores.Find(unaFrase.Autor.Usuario);
            unaFrase.Autor = autorEnDB;
            unContexto.Frases.Add(unaFrase);
            unContexto.SaveChanges();
        }

        public List<Frase> DevolverFrasesConEntidadYFechaHorasDB()
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    return GenerarListaFrasesConEntidadYFechaHorasDeDB(unContexto);
                }
                catch (Exception)
                {
                    throw new ExcepcionFraseNoExistente();
                }
            }
        }

        private List<Frase> GenerarListaFrasesConEntidadYFechaHorasDeDB(ContextoNegocio unContexto)
        {
            return unContexto.Frases.Include(frase => frase.UnaEntidad).Include(frase => frase.Autor).ToList<Frase>();
        }

        public List<Frase> DevolverFrasesDB()
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    return GenerarListaFrasesDeDB(unContexto);
                }
                catch (Exception)
                {
                    throw new ExcepcionFraseNoExistente();
                }
            }
        }

        public void BorrarTodasFrasesDB()
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    RecorrerFrasesABorrar(unContexto);
                }
                catch (Exception)
                {
                    throw new ExcepcionFraseNoSePuedeBorrar();
                }
            }
        }

        private void RecorrerFrasesABorrar(ContextoNegocio unContexto)
        {
            foreach (Frase unaFrase in GenerarListaFrasesDeDB(unContexto))
            {
                EliminarFraseDB(unContexto, unaFrase);
            }
        }

        private List<Frase> GenerarListaFrasesDeDB(ContextoNegocio unContexto)
        {
            return unContexto.Frases.ToList();
        }

        private void EliminarFraseDB(ContextoNegocio unContexto, Frase unaFrase)
        {
            unContexto.Frases.Remove(unaFrase);
            unContexto.SaveChanges();
        }
    }
}
