﻿using DominioNegocio;
using DominioNegocio.Excepciones;
using ExcepcionesLogica;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PersistenciaNegocio
{
    public class PersistenciaAutor
    {
        public void AgregarAutor(Autor unAutor)
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    EstablecerAutorEnDB(unAutor, unContexto);
                }
                catch (Exception)
                {
                    throw new ExcepcionAutorExistente();
                }
            }
        }

        private static void EstablecerAutorEnDB(Autor unAutor, ContextoNegocio unContexto)
        {
            unContexto.Autores.Add(unAutor);
            unContexto.SaveChanges();
        }

        public Autor DevolverAutorEnDB(Autor unAutor)
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    return BuscarAutorEnDB(unAutor, unContexto);
                }
                catch (Exception)
                {
                    throw new ExcepcionAutorNoExistente();
                }
            }
        }

        public void BorrarAutorEnDB(Autor autorABorrar)
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    EliminarAutorEnDB(autorABorrar, unContexto);
                }
                catch (Exception)
                {
                    throw new ExcepcionAutorNoSePuedeBorrar();
                }
            }
        }

        private static void EliminarAutorEnDB(Autor autorABorrar, ContextoNegocio unContexto)
        {
            unContexto.Autores.Remove(unContexto.Autores.Single(a => a.Usuario == autorABorrar.Usuario));
            unContexto.SaveChanges();
        }

        private static Autor BuscarAutorEnDB(Autor unAutor, ContextoNegocio unContexto)
        {
            return unContexto.Autores.Find(unAutor.Usuario);
        }

        public List<Autor> DevolverAutores()
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    return GenerarListaAutoresDeDB(unContexto);
                }
                catch (Exception)
                {
                    throw new ExcepcionAutorExistente();
                }
            }
        }

        public void BorrarTodosLosAutoresEnDB()
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                try
                {
                    RecorrerAutoresABorrar(unContexto);
                }
                catch (Exception)
                {
                    throw new ExcepcionAutorNoSePuedeBorrar();
                }
            }
        }

        private void RecorrerAutoresABorrar(ContextoNegocio unContexto)
        {
            foreach (Autor unAutor in GenerarListaAutoresDeDB(unContexto))
            {
                unContexto.Autores.Remove(unAutor);
                unContexto.SaveChanges();
            }
        }

        private List<Autor> GenerarListaAutoresDeDB(ContextoNegocio unContexto)
        {
            return unContexto.Autores.ToList();
        }

        public void ModificarAutorEnDB(Autor autorAModificar, Autor autorParametrosModificados)
        {
            using (ContextoNegocio unContexto = new ContextoNegocio())
            {
                Autor autorAModEnDB = BuscarAutorEnDB(autorAModificar, unContexto);
                ModificarAtributos(autorParametrosModificados, autorAModEnDB);
                unContexto.SaveChanges();
            }
        }

        private static void ModificarAtributos(Autor a2, Autor autorAModEnDB)
        {
            autorAModEnDB.Nombre = a2.Nombre;
            autorAModEnDB.Apellido = a2.Apellido;
            autorAModEnDB.FechaNacimiento = a2.FechaNacimiento;
        }
    }
}
