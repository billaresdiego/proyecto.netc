﻿using DominioNegocio;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace PersistenciaNegocio
{
    public class ContextoNegocio : DbContext
    {
        public ContextoNegocio()
        {

        }

        public DbSet<Entidad> Entidades { get; set; }

        public DbSet<Sentimiento> Sentimientos { get; set; }

        public DbSet<Frase> Frases { get; set; }

        public DbSet<Autor> Autores { get; set; }

        public DbSet<Alarma> Alarmas { get; set; }

        public DbSet<AlarmaAutor> AlarmasAutores { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            NombreDeTablasACrear(modelBuilder);
            RestriccionesEntidadesDB(modelBuilder);
            RestriccionesSentimientosDB(modelBuilder);
            RestriccionesAutoresDB(modelBuilder);
            RestriccionesAlarmas(modelBuilder);
            RestriccionesFrasesDB(modelBuilder);
            RestriccionesAlarmasAutorDB(modelBuilder);
        }


        private void NombreDeTablasACrear(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Entidad>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Entidad_table");
            });
            modelBuilder.Entity<Sentimiento>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Sentimiento_table");
            });
            modelBuilder.Entity<Frase>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Frase_table");
            });
            modelBuilder.Entity<Autor>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Autor_table");
            });
            modelBuilder.Entity<Alarma>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("Alarma_table");
            });
            modelBuilder.Entity<AlarmaAutor>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("AlarmasAutores_table");
            });
        }

        private void RestriccionesSentimientosDB(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Sentimiento>().HasKey(sentimiento => sentimiento.NombreSentimiento);
        }

        private void RestriccionesEntidadesDB(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Entidad>().HasKey(entidad => entidad.NombreUnaEntidad);
        }
        private void RestriccionesAutoresDB(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Autor>().HasKey(autor => autor.Usuario);
        }

        private void RestriccionesFrasesDB(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Frase>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }

        private void RestriccionesAlarmas(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Alarma>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }

        private void RestriccionesAlarmasAutorDB(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AlarmaAutor>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}
