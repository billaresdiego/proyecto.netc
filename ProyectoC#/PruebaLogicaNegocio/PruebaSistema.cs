﻿using System;
using LogicaNegocio;
using ExcepcionesLogica;
using DominioNegocio;
using DominioNegocio.Excepciones;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace PruebaLogicaNegocio
{
    [TestClass]
    public class PruebaSistema
    {
        [TestInitialize]
        public void Init()
        {
            Sistema sistema = new Sistema();
            limpiarTablasFisicasDB(sistema);
        }

        [TestCleanup]
        public void CleanUp()
        {
            Sistema sistema = new Sistema();
            limpiarTablasFisicasDB(sistema);
        }

        private void limpiarTablasFisicasDB(Sistema sistema)
        {
            sistema.BorrarTodasFrases();
            sistema.BorrarTodosLosAutores();
            sistema.BorrarTodasLasAlarmas();
            sistema.BorrarTodosSentimientos();
            sistema.BorrarTodasEntidades();
            sistema.BorrarAlarmasAutor();
        }

        [TestMethod]
        public void PruebaAgregarSentimiento()
        {
            Sistema sistema = new Sistema();
            Sentimiento sentimiento = new Sentimiento(Sentimiento.TiposSentimiento.negativo, "odio");
            sistema.AgregarSentimiento(sentimiento);
            Sentimiento sentimientoSistema = sistema.DevolverSentimiento(sentimiento);
            Assert.AreEqual(sentimiento.NombreSentimiento, sentimientoSistema.NombreSentimiento);
        }

        [ExpectedException(typeof(ExcepcionSentimientoNoExistente))]
        [TestMethod]
        public void PruebaDevolverSentimientoNoExistente()
        {
            Sistema sistema = new Sistema();
            Sentimiento sentimiento = new Sentimiento(Sentimiento.TiposSentimiento.positivo, "alegro");
            sistema.DevolverSentimiento(sentimiento);
        }

        [ExpectedException(typeof(ExcepcionSentimientoExistente))]
        [TestMethod]
        public void PruebaAgregarSentimientoExistente()
        {
            Sistema sistema = new Sistema();
            Sentimiento sentimientoPrueba = new Sentimiento(Sentimiento.TiposSentimiento.positivo, "extrabagante");
            sistema.AgregarSentimiento(sentimientoPrueba);
            sistema.AgregarSentimiento(sentimientoPrueba);
        }

        [ExpectedException(typeof(ExcepcionSentimientoSinTipo))]
        [TestMethod]
        public void PruebaAgregarSentimientoSinTipo()
        {
            Sistema sistema = new Sistema();
            Sentimiento sentimiento = new Sentimiento();
            sentimiento.NombreSentimiento = "odio";
            sistema.AgregarSentimiento(sentimiento);
        }

        [TestMethod]
        public void PruebaAgregarEntidad()
        {
            Sistema sistema = new Sistema();
            Entidad entidadPrueba = new Entidad("Nombre de prueba");
            sistema.AgregarEntidad(entidadPrueba);
            Entidad entidadRetorno = sistema.DevolverEntidad(entidadPrueba);
            Assert.AreEqual(entidadPrueba.NombreUnaEntidad, entidadRetorno.NombreUnaEntidad);
        }

        [ExpectedException(typeof(ExcepcionEntidadExistente))]
        [TestMethod]
        public void PruebaAgregarEntidadExistente()
        {
            Sistema sistema = new Sistema();
            Entidad entidadPrueba = new Entidad("adaw");
            sistema.AgregarEntidad(entidadPrueba);
            sistema.AgregarEntidad(entidadPrueba);
        }

        [ExpectedException(typeof(ExcepcionEntidadNoExistente))]
        [TestMethod]
        public void PruebaDevolverEntidadNoExistente()
        {
            Sistema sistema = new Sistema();
            Entidad entidadPrueba = new Entidad("adwawdxvs");
            sistema.DevolverEntidad(entidadPrueba);
        }

        [TestMethod]
        public void PruebaAgregarFrase()
        {
            Sistema sistema = new Sistema();
            Entidad entidad = new Entidad("La Trinidad");
            DateTime fechaAyer = DateTime.Now.AddDays(-1);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = DateTime.Now.AddYears(-50)
            };
            sistema.AgregarAutor(autor);
            Frase frase = new Frase("es lo maximo pero no me gusta la comida de La Trinidad", "Neutro", entidad, fechaAyer, autor);
            sistema.AgregarFrase(frase);
            Assert.AreEqual(frase, sistema.DevolverFrase(frase));
        }

        [ExpectedException(typeof(ExcepcionFraseNoExistente))]
        [TestMethod]
        public void PruebaDevolverFraseNoExistente()
        {
            Sistema sistema = new Sistema();
            Entidad entidad = new Entidad("Telino");
            DateTime fechaAyer = DateTime.Now.AddDays(-1);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = DateTime.Now.AddYears(-50)
            };
            Frase frase = new Frase("el mejor parque de diversiones es el Telino", "Positivo", entidad, fechaAyer,autor);
            sistema.DevolverFrase(frase);
        }

        [TestMethod]
        public void PruebaAlarmasRegistradas()
        {
            Sistema sistema = new Sistema();
            Entidad entidad = new Entidad("Loneanse");
            Alarma alarma = new Alarma(entidad, Alarma.Tipo.positivo, 2, false, 2, 0);
            Sentimiento sentimiento = new Sentimiento(Sentimiento.TiposSentimiento.positivo, "Maravilla");
            sistema.AgregarEntidad(entidad);
            sistema.AgregarAlarmaConfigurada(alarma);
            sistema.AgregarAlarmaConfigurada(alarma);
            List<Alarma> listaEsperada = new List<Alarma>();
            listaEsperada.Add(alarma);
            listaEsperada.Add(alarma);
            Assert.AreEqual(listaEsperada[1], sistema.DevolverAlarmasRegistradas()[1]);
        }

        [TestMethod]
        public void PruebaRegistrarAlarmaEnHoras()
        {
            Sistema sistema = new Sistema();
            Entidad entidad = new Entidad("Loneanse");
            Alarma alarma = new Alarma(entidad, Alarma.Tipo.positivo, 1, false, 0, 2);
            Sentimiento sentimiento = new Sentimiento(Sentimiento.TiposSentimiento.positivo, "Maravilla");
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = DateTime.Now.AddYears(-50)
            };
            sistema.AgregarAutor(autor);
            Frase frase = new Frase("Maravilla Loneanse", "Positivo", entidad, DateTime.Now, autor);
            sistema.AgregarSentimiento(sentimiento);
            sistema.AgregarFrase(frase);
            sistema.AgregarEntidad(entidad);
            sistema.AgregarAlarmaConfigurada(alarma);
            Assert.AreEqual(alarma.PlazoHoras, sistema.DevolverAlarmasRegistradas()[0].PlazoHoras);
        }

        [TestMethod]
        public void PruebaRegistrarAlarmaSinPlazoHora()
        {
            Sistema sistema = new Sistema();
            Entidad entidad = new Entidad("Loneanse");
            Alarma alarma = new Alarma(entidad, Alarma.Tipo.positivo, 1, false, 0, 0);
            Sentimiento sentimiento = new Sentimiento(Sentimiento.TiposSentimiento.positivo, "Maravilla");
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = DateTime.Now.AddYears(-50)
            };
            sistema.AgregarAutor(autor);
            Frase frase = new Frase("Maravilla Loneanse", "Positivo", entidad, DateTime.Now, autor);
            sistema.AgregarFrase(frase);
            sistema.AgregarEntidad(entidad);
            sistema.AgregarAlarmaConfigurada(alarma);
            Assert.AreEqual(0, sistema.DevolverAlarmasRegistradas()[0].PlazoDias);
        }

        [TestMethod]
        public void PruebaRegistrarAlarmaSinPlazoDia()
        {
            Sistema sistema = new Sistema();
            Entidad entidad = new Entidad("Loneanse");
            Alarma alarma = new Alarma(entidad, Alarma.Tipo.positivo, 1, false, 0, 0);
            Sentimiento sentimiento = new Sentimiento(Sentimiento.TiposSentimiento.positivo, "Maravilla");
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = DateTime.Now.AddYears(-50)
            };
            sistema.AgregarAutor(autor);
            Frase frase = new Frase("Maravilla Loneanse", "Positivo", entidad, DateTime.Now, autor);
            sistema.AgregarFrase(frase);
            sistema.AgregarEntidad(entidad);
            sistema.AgregarAlarmaConfigurada(alarma);
            Assert.AreEqual(0, sistema.DevolverAlarmasRegistradas()[0].PlazoHoras);
        }

        [TestMethod]
        public void PruebaAgregarAutor()
        {
            Sistema sistema = new Sistema();
            Autor autorParaAgregar = new Autor();
            autorParaAgregar.Nombre = "Arturo";
            autorParaAgregar.Usuario = "Arturo91";
            autorParaAgregar.Apellido = "Vidal";
            autorParaAgregar.FechaNacimiento = DateTime.Now.AddYears(-50);
            sistema.AgregarAutor(autorParaAgregar);
            Autor autorSistema = sistema.DevolverAutor(autorParaAgregar);
            Assert.AreEqual(autorParaAgregar.Nombre, autorSistema.Nombre);
        }

        [ExpectedException(typeof(ExcepcionAutorNoExistente))]
        [TestMethod]
        public void PruebaDevolverAutorNoExistente()
        {
            Sistema sistema = new Sistema();
            Autor unAutor = new Autor
            {
                Nombre = "Arturo",
                Usuario = "Arturo91",
                Apellido = "Vidal",
                FechaNacimiento = DateTime.Now.AddYears(-50)
            };
            sistema.DevolverAutor(unAutor);
        }

        [ExpectedException(typeof(ExcepcionAutorExistente))]
        [TestMethod]
        public void PruebaAgregarAutorExistente()
        {
            Sistema sistema = new Sistema();
            Autor unAutor = new Autor
            {
                Nombre = "Arturo",
                Usuario = "Arturo91",
                Apellido = "Vidal",
                FechaNacimiento = DateTime.Now.AddYears(-50)
            };
            sistema.AgregarAutor(unAutor);
            sistema.AgregarAutor(unAutor);
        }

        [TestMethod]
        public void PruebaDevolverAlarmaAutorAAregistrar()
        {
            Sistema sistema = new Sistema();
            AlarmaAutor alarma = new AlarmaAutor
            {
                Activada = false,
                CantidadDePosts = 4,
                PlazoDias = 5,
            };
            sistema.AlarmaAutorARegistrar = alarma;
            Assert.AreEqual(alarma, sistema.AlarmaAutorARegistrar);
        }

        [TestMethod]
        public void PruebaAlarmasAutorRegistradas()
        {
            Sistema sistema = new Sistema();
            AlarmaAutor alarmaAutor = new AlarmaAutor(AlarmaAutor.TipoAlarmaAutor.positivo, 2, false, 2, 1);
            Sentimiento sentimiento = new Sentimiento(Sentimiento.TiposSentimiento.positivo, "Maravilla");
            sistema.AgregarAlarmaAutorConfigurada(alarmaAutor);
            sistema.AgregarAlarmaAutorConfigurada(alarmaAutor);
            List<AlarmaAutor> listaEsperada = new List<AlarmaAutor>();
            listaEsperada.Add(alarmaAutor);
            listaEsperada.Add(alarmaAutor);
            Assert.AreEqual(listaEsperada[1], sistema.DevolverAlarmasAutorRegistradas()[1]);
        }

        [TestMethod]
        public void DevolverFrasesConEntidadEnDB()
        {
            Sistema sistema = new Sistema();
            Autor unAutor = new Autor
            {
                Nombre = "Arturo",
                Usuario = "Arturo91",
                Apellido = "Vidal",
                FechaNacimiento = DateTime.Now.AddYears(-50)
            };
            Entidad entidad = new Entidad("Texaco");
            sistema.AgregarEntidad(entidad);
            sistema.AgregarAutor(unAutor);
            Frase frase = new Frase("Maravilla Texaco", "positivo", entidad, DateTime.Now, unAutor);
            sistema.AgregarFrase(frase);
            Assert.AreEqual(entidad, sistema.DevolverFrasesConEntidadYFechaYHora()[0].UnaEntidad);
        }
    }
}
