﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LogicaNegocio;
using System.Collections.Generic;
using ExcepcionesLogica;
using DominioNegocio;
using DominioNegocio.Excepciones;

namespace PruebaLogicaNegocio
{
    [TestClass]
    public class PruebaFachadaUsuario
    {
        [TestInitialize]
        public void Init()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            LimpiarTablasFisicasDB(fachadaUsuario);
        }

        [TestCleanup]
        public void CleanUp()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            LimpiarTablasFisicasDB(fachadaUsuario);
        }

        private void LimpiarTablasFisicasDB(FachadaUsuario fachadaUsuario)
        {
            fachadaUsuario.BorrarTodasFrases();
            fachadaUsuario.BorrarTodosLosAutores();
            fachadaUsuario.BorrarTodasLasAlarmas();
            fachadaUsuario.BorrarTodasLasAlarmasAutor();
            fachadaUsuario.BorrarTodosSentimientos();
            fachadaUsuario.BorrarTodasEntidades();
        }

        [TestMethod]
        public void PruebaConfigurarAlarmaDiasConEntidad()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            Entidad entidad = new Entidad("Tromoville");
            fachadaUsuario.CrearEntidadEnSistema("Tromoville");
            Alarma alarmaParaValidar = new Alarma(entidad);
            alarmaParaValidar.CantidadDePosts = 4;
            alarmaParaValidar.PlazoDias = 5;
            fachadaUsuario.ConfigurarAlarma(entidad, 4, 5, "Positivo", "Dias");
            Assert.AreEqual(alarmaParaValidar, fachadaUsuario.MostrarAlarmaARegistrar());
        }

        [TestMethod]
        public void PruebaConfigurarAlarmaHorasConEntidad()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            Entidad entidad = new Entidad("Tromoville");
            fachadaUsuario.CrearEntidadEnSistema("Tromoville");
            Alarma alarmaParaValidar = new Alarma(entidad);
            alarmaParaValidar.CantidadDePosts = 4;
            alarmaParaValidar.PlazoHoras = 5;
            fachadaUsuario.ConfigurarAlarma(entidad, 4, 5, "Positivo", "Horas");
            Assert.AreEqual(alarmaParaValidar, fachadaUsuario.MostrarAlarmaARegistrar());
        }

        [TestMethod]
        public void PruebaDevolverAlarmaConfiguradaConEntidad()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            Entidad entidad = new Entidad("Tromoville");
            fachadaUsuario.CrearEntidadEnSistema("Tromoville");
            Alarma alarmaParaValidar = new Alarma(entidad);
            alarmaParaValidar.CantidadDePosts = 4;
            alarmaParaValidar.PlazoDias = 5;
            alarmaParaValidar.UnaEntidad = entidad;
            alarmaParaValidar.Id = 1;
            fachadaUsuario.ConfigurarAlarma(entidad, 4, 5, "Positivo", "Dias");
            Assert.AreEqual(alarmaParaValidar, fachadaUsuario.DevolverAlarmaConfigurada(alarmaParaValidar));
        }

        [TestMethod]
        public void PruebaDevolverAlarmaAutorConfiguradaConEntidad()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            AlarmaAutor alarmaParaValidar = new AlarmaAutor
            {
                PlazoDias = 2,
                CantidadDePosts = 2,
                Activada = false,
                UnTipo = AlarmaAutor.TipoAlarmaAutor.negativo,
                Id = 1
            };
            fachadaUsuario.ConfigurarAlarmaAutor(2, 2, "Negativo", "Dias");
            Assert.AreEqual(alarmaParaValidar, fachadaUsuario.DevolverAlarmaAutorConfigurada(alarmaParaValidar));
        }

        [TestMethod]
        public void PruebaAlertaAlarmaNegativaAlIngresarFrases()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            Entidad entidad = new Entidad("Loneanse");
            fachadaUsuario.CrearEntidadEnSistema("Loneanse");
            fachadaUsuario.CrearSentimientoNegativoEnSistema("Porqueria");
            fachadaUsuario.ConfigurarAlarma(entidad, 2, 2, "negativo", "Dias");
            DateTime fecha = DateTime.Now.AddYears(-45);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fecha);
            fachadaUsuario.CrearFraseEnSistema("Una porqueria Loneanse", DateTime.Now, autor);
            fachadaUsuario.CrearFraseEnSistema("La verdad que si, porqueria es poco Loneanse", DateTime.Now, autor);
            Alarma alarmaPrueba = new Alarma(entidad, Alarma.Tipo.negativo, 2, true, 2, 0);
            Assert.AreEqual(alarmaPrueba, fachadaUsuario.DevolverAlarmasActivadasPorFrases()[0]);
        }

        [TestMethod]
        public void PruebaAlertaAlarmaPositivaAlIngresarFrases()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            Entidad entidad = new Entidad("Loneanse");
            fachadaUsuario.CrearEntidadEnSistema("Loneanse");
            fachadaUsuario.CrearSentimientoPositivoEnSistema("amo");
            fachadaUsuario.ConfigurarAlarma(entidad, 2, 2, "positivo", "Dias");
            DateTime fecha = DateTime.Now.AddYears(-45);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fecha);
            fachadaUsuario.CrearFraseEnSistema("amo Loneanse", DateTime.Now, autor);
            fachadaUsuario.CrearFraseEnSistema("La verdad que si, amo a Loneanse", DateTime.Now, autor);
            Alarma alarmaPrueba = new Alarma(entidad, Alarma.Tipo.positivo, 2, true, 2, 0);
            Assert.AreEqual(alarmaPrueba, fachadaUsuario.DevolverAlarmasActivadasPorFrases()[0]);
        }

        [TestMethod]
        public void PruebaAlertaAlarmaEnHorasNegativaAlIngresarFrases()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            Entidad entidad = new Entidad("Loneanse");
            fachadaUsuario.CrearEntidadEnSistema("Loneanse");
            fachadaUsuario.CrearSentimientoNegativoEnSistema("Porqueria");
            fachadaUsuario.ConfigurarAlarma(entidad, 2, 2, "negativo", "Horas");
            DateTime fecha = DateTime.Now.AddYears(-45);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fecha);
            fachadaUsuario.CrearFraseEnSistema("Una porqueria Loneanse", DateTime.Now, autor);
            fachadaUsuario.CrearFraseEnSistema("La verdad que si, porqueria es poco Loneanse", DateTime.Now, autor);
            Alarma alarmaPrueba = new Alarma(entidad, Alarma.Tipo.negativo, 2, true, 0, 2);
            Assert.AreEqual(alarmaPrueba, fachadaUsuario.DevolverAlarmasActivadasPorFrases()[0]);
        }

        [ExpectedException(typeof(ExcepcionSentimientoSinTipo))]
        [TestMethod]
        public void PruebaCrearSentimientoSinTipo()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            fachadaUsuario.CrearSentimientoSinTipoEnSistema("odio");
        }

        [ExpectedException(typeof(ExcepcionFraseSinEntidadExistente))]
        [TestMethod]
        public void PruebaCrearFraseSinEntidadExistente()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            fachadaUsuario.CrearSentimientoPositivoEnSistema("fabuloso");
            DateTime fecha = DateTime.Now.AddYears(-45);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fecha);
            fachadaUsuario.CrearFraseEnSistema("Es fabuloso trabajar en Micotrist", DateTime.Now, autor);
        }

        [ExpectedException(typeof(ExcepcionFraseSinSentimientoExistente))]
        [TestMethod]
        public void PruebaCrearFraseSinSentimientoExistente()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            DateTime fecha = DateTime.Now.AddYears(-45);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fecha);
            fachadaUsuario.CrearFraseEnSistema("Es fabuloso trabajar en Micotrist", DateTime.Now, autor);
        }

        [ExpectedException(typeof(ExcepcionElStringAConvertirNoEsValido))]
        [TestMethod]
        public void PruebaConvertirStringInvalidoEnInt()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            int numeroPrueba = fachadaUsuario.ConvertirStringAInt("");
        }

        [TestMethod]
        public void PruebaConvertirString()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            int numeroPrueba = fachadaUsuario.ConvertirStringAInt("4");
            Assert.AreEqual(4, numeroPrueba);
        }


        [TestMethod]
        public void PruebaDevolverFraseNoExistente()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            DateTime fechaAyer = DateTime.Now.AddDays(-1);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = DateTime.Now.AddYears(-50)
            };
            Frase unaFrase = new Frase("frase de prueba", "positiva", new Entidad("entidad"), fechaAyer, autor);
            Assert.AreEqual(0, fachadaUsuario.DevolverFrases().Count);
        }

        [ExpectedException(typeof(ExcepcionFechaFutura))]
        [TestMethod]
        public void PruebaCrearFraseFechaFutura()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            DateTime fechaManiana = DateTime.Now.AddDays(1);
            fachadaUsuario.CrearEntidadEnSistema("NukaCola");
            fachadaUsuario.CrearSentimientoPositivoEnSistema("encanta");
            DateTime fecha = DateTime.Now.AddYears(-45);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fecha);
            fachadaUsuario.CrearFraseEnSistema("Me encanta la NukaCola", fechaManiana, autor);
        }

        [TestMethod]
        public void PruebaDevolverFraseExistente()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            fachadaUsuario.CrearEntidadEnSistema("NukaCola");
            fachadaUsuario.CrearSentimientoPositivoEnSistema("encanta");
            DateTime fecha = DateTime.Now.AddYears(-45);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fecha);
            fachadaUsuario.CrearFraseEnSistema("Me encanta la NukaCola", DateTime.Now, autor);
            Entidad entidadDePrueba = new Entidad("NukaCola");
            DateTime fechaAyer = DateTime.Now.AddDays(-1);
            Frase buscar = new Frase("Me encanta la NukaCola", "positivo", entidadDePrueba, fechaAyer, autor);
            Frase esperada = new Frase("Me encanta la NukaCola", "positivo", entidadDePrueba, fechaAyer, autor);
            Assert.AreEqual(esperada, fachadaUsuario.DevolverFrases()[0]);
        }

        [TestMethod]
        public void PruebaCrearFraseNeutra()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            fachadaUsuario.CrearEntidadEnSistema("NukaCola");
            fachadaUsuario.CrearSentimientoPositivoEnSistema("encanta");
            fachadaUsuario.CrearSentimientoNegativoEnSistema("odio");
            DateTime fecha = DateTime.Now.AddYears(-45);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fecha);
            fachadaUsuario.CrearFraseEnSistema("Me encanta y odio la NukaCola", DateTime.Now, autor);
            Assert.AreEqual("Neutro", fachadaUsuario.DevolverFrases()[0].UnaCategoria);
        }

        [TestMethod]
        public void PruebaDevolverEntidades()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            fachadaUsuario.CrearEntidadEnSistema("NukaCola");
            fachadaUsuario.CrearEntidadEnSistema("FlimFlam");
            Entidad entidad1 = new Entidad("NukaCola");
            Entidad entidad2 = new Entidad("FlimFlam");
            List<Entidad> listaEsperada = new List<Entidad>();
            listaEsperada.Add(entidad1);
            listaEsperada.Add(entidad2);
            List<Entidad> lista = fachadaUsuario.DevolverEntidades();
            Assert.AreEqual(listaEsperada.Count, lista.Count);
        }

        [TestMethod]
        public void PruebaDevolverEntidadesCuandoNoHay()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            List<Entidad> listaEsperada = new List<Entidad>();
            CollectionAssert.AreEqual(listaEsperada, fachadaUsuario.DevolverEntidades());
        }

        [TestMethod]
        public void PruebaDevolverFrases()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            fachadaUsuario.CrearEntidadEnSistema("NukaCola");
            fachadaUsuario.CrearEntidadEnSistema("FlimFlam");
            fachadaUsuario.CrearSentimientoPositivoEnSistema("amo");
            Entidad entidad1 = new Entidad("NukaCola");
            Entidad entidad2 = new Entidad("FlimFlam");
            List<Frase> listaEsperada = new List<Frase>();
            DateTime fecha = DateTime.Now.AddYears(-45);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fecha);
            fachadaUsuario.CrearFraseEnSistema("Yo amo la NukaCola", DateTime.Now, autor);
            fachadaUsuario.CrearFraseEnSistema("Amo el FlimFlam mas que nada en el mundo", DateTime.Now, autor);
            DateTime fechaAyer = DateTime.Now.AddDays(-1);
            Frase frase1 = new Frase("Yo amo la NukaCola", "positivo", entidad1, fechaAyer, autor);
            Frase frase2 = new Frase("Amo el FlimFlam mas que nada en el mundo", "positivo", entidad2, fechaAyer, autor);
            listaEsperada.Add(frase1);
            listaEsperada.Add(frase2);
            Assert.AreEqual(listaEsperada[1], fachadaUsuario.DevolverFrases()[1]);
        }

        [TestMethod]
        public void PruebaDevolverFrasesCuandoNoHay()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            List<Frase> listaEsperada = new List<Frase>();
            CollectionAssert.AreEqual(listaEsperada, fachadaUsuario.DevolverFrases());
        }

        [TestMethod]
        public void PruebaSentimientoEstaEnAlgunaFrase()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            fachadaUsuario.CrearEntidadEnSistema("NukaCola");
            fachadaUsuario.CrearSentimientoPositivoEnSistema("amo");
            DateTime fecha = DateTime.Now.AddYears(-45);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fecha);
            fachadaUsuario.CrearFraseEnSistema("Yo amo la NukaCola", DateTime.Now, autor);
            Sentimiento sentimientoDePrueba = new Sentimiento(Sentimiento.TiposSentimiento.positivo, "amo");
            Assert.AreEqual(true, fachadaUsuario.SentimientoEstaEnAlgunaFrase("amo"));
        }

        [TestMethod]
        public void PruebaSentimientoEstaEnAlgunaFraseNoEsta()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            fachadaUsuario.CrearEntidadEnSistema("NukaCola");
            fachadaUsuario.CrearSentimientoPositivoEnSistema("amo");
            Sentimiento sentimientoDePrueba = new Sentimiento(Sentimiento.TiposSentimiento.positivo, "amo");
            Assert.AreEqual(false, fachadaUsuario.SentimientoEstaEnAlgunaFrase("amo"));
        }

        [TestMethod]
        public void PruebaBorrarSentimiento()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            fachadaUsuario.CrearSentimientoPositivoEnSistema("amo");
            Sentimiento sentimientoDePrueba = new Sentimiento(Sentimiento.TiposSentimiento.positivo, "amo");
            fachadaUsuario.BorrarSentimiento("amo");
            Assert.AreEqual(false, fachadaUsuario.DevolverSentimientos().Contains(sentimientoDePrueba));
        }

        [ExpectedException(typeof(ExcepcionSentimientoNoSePuedeBorrar))]
        [TestMethod]
        public void PruebaBorrarSentimientoCuandoEstaEnUnaFrase()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            Entidad entidadDePrueba = new Entidad("NukaCola");
            fachadaUsuario.CrearEntidadEnSistema("NukaCola");
            fachadaUsuario.CrearSentimientoPositivoEnSistema("amo");
            DateTime fecha = DateTime.Now.AddYears(-45);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fecha);
            fachadaUsuario.CrearFraseEnSistema("Amo la NukaCola", DateTime.Now, autor);
            Sentimiento sentimientoDePrueba = new Sentimiento(Sentimiento.TiposSentimiento.positivo, "amo");
            fachadaUsuario.BorrarSentimiento("amo");
        }

        [ExpectedException(typeof(ExcepcionAlertaXComentariosNegativosYDias))]
        [TestMethod]
        public void PruebaAlertaAlarmaNegativa()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            Entidad entidad = new Entidad("Loneanse");
            fachadaUsuario.CrearEntidadEnSistema("Loneanse");
            fachadaUsuario.CrearSentimientoNegativoEnSistema("Porqueria");
            DateTime fecha = DateTime.Now.AddYears(-45);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fecha);
            fachadaUsuario.CrearFraseEnSistema("Una porqueria Loneanse", DateTime.Now, autor);
            fachadaUsuario.CrearFraseEnSistema("La verdad que si, porqueria es poco Loneanse", DateTime.Now, autor);
            fachadaUsuario.ConfigurarAlarma(entidad, 2, 2, "negativo", "Dias");
        }

        [ExpectedException(typeof(ExcepcionAlertaXComentariosPositivosYDias))]
        [TestMethod]
        public void PruebaAlertaAlarmaPositiva()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            Entidad entidad = new Entidad("Loneanse");
            fachadaUsuario.CrearEntidadEnSistema("Loneanse");
            fachadaUsuario.CrearSentimientoPositivoEnSistema("Maravilla");
            DateTime fecha = DateTime.Now.AddYears(-45);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fecha);
            fachadaUsuario.CrearFraseEnSistema("Una Maravilla Loneanse", DateTime.Now, autor);
            fachadaUsuario.CrearFraseEnSistema("La verdad que si, maravilla es poco Loneanse", DateTime.Now, autor);
            fachadaUsuario.ConfigurarAlarma(entidad, 2, 2, "positivo", "Dias");
        }

        [TestMethod]
        public void PruebaDevolverAlarmasRegistradas()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            Entidad entidad = new Entidad("Loneanse");
            fachadaUsuario.CrearEntidadEnSistema("Loneanse");
            fachadaUsuario.CrearSentimientoPositivoEnSistema("Maravilla");
            fachadaUsuario.ConfigurarAlarma(entidad, 2, 2, "positivo", "Dias");
            Alarma alarma = new Alarma(entidad, Alarma.Tipo.positivo, 2, false, 2, 0);
            Assert.AreEqual(alarma, fachadaUsuario.DevolverAlarmasRegistradas()[0]);
        }

        [ExpectedException(typeof(ExcepcionFechaNacimientoMinima))]
        [TestMethod]
        public void PruebaCrearAutorFechaFutura()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            DateTime fechaManiana = DateTime.Now.AddDays(1);
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fechaManiana);
        }

        [ExpectedException(typeof(ExcepcionAutorExistente))]
        [TestMethod]
        public void PruebaCrearAutorYaExistente()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            DateTime fecha = DateTime.Now.AddYears(-45);
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fecha);
            fachadaUsuario.CrearAutorEnSistema("Marcos", "Rojo", "Arturo89", fecha);
        }

        [TestMethod]
        public void PruebaDevolverAutores()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            DateTime fecha = DateTime.Now.AddYears(-45);
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fecha);
            fachadaUsuario.CrearAutorEnSistema("Marcos", "Rojo", "Marc9", fecha);
            Autor autor1 = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            Autor autor2 = new Autor
            {
                Nombre = "Marcos",
                Apellido = "Rojo",
                Usuario = "Marc9",
                FechaNacimiento = fecha
            };
            List<Autor> listaEsperada = new List<Autor>();
            listaEsperada.Add(autor1);
            listaEsperada.Add(autor2);
            CollectionAssert.AreEqual(listaEsperada, fachadaUsuario.DevolverAutores());
        }

        [TestMethod]
        public void PruebaBorrarAutor()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            DateTime fecha = DateTime.Now.AddYears(-45);
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fecha);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            fachadaUsuario.BorrarAutor(autor.Usuario);
            Assert.AreEqual(false, fachadaUsuario.DevolverAutores().Contains(autor));
        }

        [ExpectedException(typeof(ExcepcionAutorNoSePuedeBorrar))]
        [TestMethod]
        public void PruebaBorrarAutorTieneFrase()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            fachadaUsuario.CrearEntidadEnSistema("CocaCola");
            fachadaUsuario.CrearSentimientoPositivoEnSistema("encanta");
            DateTime fecha = DateTime.Now.AddYears(-45);
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fecha);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            fachadaUsuario.CrearFraseEnSistema("Me encanta la CocaCola", DateTime.Now, autor);
            fachadaUsuario.BorrarAutor(autor.Usuario);
        }

        [TestMethod]
        public void PruebaModificarAutorExistente()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            DateTime fecha = DateTime.Now.AddYears(-45);
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fecha);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            Autor autorParametrosAModificar = new Autor
            {
                Nombre = "Thibaut",
                Apellido = "Courtois",
                Usuario = "Courtois1",
                FechaNacimiento = fecha
            };
            fachadaUsuario.ModificarAutor(autor, autorParametrosAModificar);
            Assert.AreEqual(autorParametrosAModificar.Nombre, fachadaUsuario.DevolverAutores()[0].Nombre);
        }

        [TestMethod]
        public void PruebaConfigurarAlarmaAutor()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            AlarmaAutor alarmaParaValidar = new AlarmaAutor
            {
                Activada = false,
                CantidadDePosts = 4,
                PlazoDias = 5,
            };
            fachadaUsuario.ConfigurarAlarmaAutor(4, 5, "Positivo", "Dias");
            Assert.AreEqual(alarmaParaValidar, fachadaUsuario.MostrarAlarmaAutorARegistrar());
        }

        [ExpectedException(typeof(ExcepcionAlarmaSinTipo))]
        [TestMethod]
        public void PruebaConfigurarAlarmaAutorSinTipo()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            fachadaUsuario.ConfigurarAlarmaAutor(4, 5, "", "Dias");
        }

        [TestMethod]
        public void PruebaDevolverAlarmasAutorRegistradas()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            fachadaUsuario.CrearSentimientoPositivoEnSistema("Maravilla");
            AlarmaAutor alarmaParaValidar = new AlarmaAutor
            {
                Activada = false,
                CantidadDePosts = 2,
                PlazoHoras = 10,
            };
            fachadaUsuario.ConfigurarAlarmaAutor(2, 10, "Positivo", "Horas");
            Assert.AreEqual(alarmaParaValidar, fachadaUsuario.DevolverAlarmasAutorRegistradas()[0]);
        }

        [TestMethod]
        public void PruebaAlertaAlarmaAutorNegativaAlIngresarFrases()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            fachadaUsuario.CrearEntidadEnSistema("Loneanse");
            fachadaUsuario.CrearSentimientoNegativoEnSistema("Porqueria");
            fachadaUsuario.ConfigurarAlarmaAutor(2, 2, "Negativo", "Dias");
            DateTime fecha = DateTime.Now.AddYears(-45);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fecha);
            fachadaUsuario.CrearFraseEnSistema("Una porqueria Loneanse", DateTime.Now, autor);
            fachadaUsuario.CrearFraseEnSistema("La verdad que si, porqueria es poco Loneanse", DateTime.Now, autor);
            AlarmaAutor alarmaPrueba = new AlarmaAutor
            {
                PlazoDias = 2,
                CantidadDePosts = 2,
                Activada = true,
                UnTipo = AlarmaAutor.TipoAlarmaAutor.negativo
            };
            Assert.AreEqual(alarmaPrueba, fachadaUsuario.DevolverAlarmasAutorActivadasPorFrases()[0]);
        }


        [TestMethod]
        public void PruebaAlertaAlarmaAutorEnHorasNegativaAlIngresarFrases()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            fachadaUsuario.CrearEntidadEnSistema("Loneanse");
            fachadaUsuario.CrearSentimientoNegativoEnSistema("Porqueria");
            fachadaUsuario.ConfigurarAlarmaAutor(2, 2, "Negativo", "Horas");
            DateTime fecha = DateTime.Now.AddYears(-45);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fecha);
            fachadaUsuario.CrearFraseEnSistema("Una porqueria Loneanse", DateTime.Now, autor);
            fachadaUsuario.CrearFraseEnSistema("La verdad que si, porqueria es poco Loneanse", DateTime.Now, autor);
            AlarmaAutor alarmaPrueba = new AlarmaAutor
            {
                PlazoHoras = 2,
                CantidadDePosts = 2,
                Activada = true,
                UnTipo = AlarmaAutor.TipoAlarmaAutor.negativo
            };
            Assert.AreEqual(alarmaPrueba, fachadaUsuario.DevolverAlarmasAutorActivadasPorFrases()[0]);
        }

        [TestMethod]
        public void PruebaAlertaAlarmaAutorPositivaAlIngresarFrases()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            fachadaUsuario.CrearEntidadEnSistema("Loneanse");
            fachadaUsuario.CrearSentimientoPositivoEnSistema("genial");
            fachadaUsuario.ConfigurarAlarmaAutor(2, 2, "Positivo", "Dias");
            DateTime fecha = DateTime.Now.AddYears(-45);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fecha);
            fachadaUsuario.CrearFraseEnSistema("Loneanse es genial!", DateTime.Now, autor);
            fachadaUsuario.CrearFraseEnSistema("La verdad que si, Loneanse es genial", DateTime.Now, autor);
            AlarmaAutor alarmaPrueba = new AlarmaAutor
            {
                PlazoDias = 2,
                CantidadDePosts = 2,
                Activada = true,
                UnTipo = AlarmaAutor.TipoAlarmaAutor.positivo
            };
            Assert.AreEqual(alarmaPrueba, fachadaUsuario.DevolverAlarmasAutorActivadasPorFrases()[0]);
        }

        [ExpectedException(typeof(ExcepcionAlertaXComentariosNegativosYDias))]
        [TestMethod]
        public void PruebaAlertaAlarmaAutorNegativa()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            fachadaUsuario.CrearSentimientoNegativoEnSistema("Porqueria");
            fachadaUsuario.CrearEntidadEnSistema("Loneanse");
            DateTime fecha = DateTime.Now.AddYears(-45);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fecha);
            fachadaUsuario.CrearFraseEnSistema("Una porqueria Loneanse", DateTime.Now, autor);
            fachadaUsuario.CrearFraseEnSistema("La verdad que si, porqueria es poco Loneanse", DateTime.Now, autor);
            fachadaUsuario.ConfigurarAlarmaAutor(2, 2, "Negativo", "Dias");
        }

        [ExpectedException(typeof(ExcepcionAlertaXComentariosPositivosYDias))]
        [TestMethod]
        public void PruebaAlertaAlarmaAutorPositiva()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            fachadaUsuario.CrearSentimientoPositivoEnSistema("grandioso");
            fachadaUsuario.CrearEntidadEnSistema("Loneanse");
            DateTime fecha = DateTime.Now.AddYears(-45);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fecha);
            fachadaUsuario.CrearFraseEnSistema("Loneanse es grandioso", DateTime.Now, autor);
            fachadaUsuario.CrearFraseEnSistema("La verdad que si, grandioso es poco para Loneanse", DateTime.Now, autor);
            fachadaUsuario.ConfigurarAlarmaAutor(2, 2, "Positivo", "Dias");
        }

        [TestMethod]
        public void PruebaDevolverFrasesConEntidad()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            fachadaUsuario.CrearSentimientoNegativoEnSistema("Porqueria");
            fachadaUsuario.CrearEntidadEnSistema("Loneanse");
            Entidad entidadAComparar = new Entidad("Loneanse");
            DateTime fecha = DateTime.Now.AddYears(-45);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fecha);
            fachadaUsuario.CrearFraseEnSistema("Porqueria Loneanse", DateTime.Now, autor);
            Assert.AreEqual(entidadAComparar, fachadaUsuario.DevolverFrasesConEntidadYFechaYHora()[0].UnaEntidad);
        }



        [TestMethod]
        public void PruebaCalcularPorcentajeFrasesPositivasAutor()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            DateTime fecha = DateTime.Now.AddYears(-45);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            fachadaUsuario.CrearSentimientoNegativoEnSistema("Porqueria");
            fachadaUsuario.CrearSentimientoPositivoEnSistema("Maravilla");
            fachadaUsuario.CrearEntidadEnSistema("Loneanse");
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fecha);
            fachadaUsuario.CrearFraseEnSistema("Porqueria Loneanse", DateTime.Now, autor);
            fachadaUsuario.CrearFraseEnSistema("Maravilla Loneanse", DateTime.Now, autor);
            Assert.AreEqual(50, fachadaUsuario.CalcularPorcentajeFrasesPositivasAutor(autor));
        }

        [TestMethod]
        public void PruebaCalcularCantidadEntidadesMencionadas()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            DateTime fecha = DateTime.Now.AddYears(-45);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            fachadaUsuario.CrearSentimientoNegativoEnSistema("Porqueria");
            fachadaUsuario.CrearSentimientoPositivoEnSistema("Maravilla");
            fachadaUsuario.CrearEntidadEnSistema("Loneanse");
            fachadaUsuario.CrearEntidadEnSistema("Texaco");
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fecha);
            fachadaUsuario.CrearFraseEnSistema("Porqueria Loneanse", DateTime.Now, autor);
            fachadaUsuario.CrearFraseEnSistema("Maravilla Texaco", DateTime.Now, autor);
            Assert.AreEqual(2, fachadaUsuario.CalcularCantidadEntidadesMencionadas(autor));
        }

        [TestMethod]
        public void PruebaCalcularPromedioFrasesDiarias()
        {
            FachadaUsuario fachadaUsuario = new FachadaUsuario();
            DateTime primeraFecha = DateTime.Now.AddDays(-2);
            DateTime fecha = DateTime.Now.AddYears(-45);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            fachadaUsuario.CrearSentimientoNegativoEnSistema("Porqueria");
            fachadaUsuario.CrearSentimientoPositivoEnSistema("Maravilla");
            fachadaUsuario.CrearEntidadEnSistema("Loneanse");
            fachadaUsuario.CrearEntidadEnSistema("Texaco");
            fachadaUsuario.CrearAutorEnSistema("Arturo", "Vidal", "Arturo89", fecha);
            fachadaUsuario.CrearFraseEnSistema("Porqueria Loneanse", DateTime.Now, autor);
            fachadaUsuario.CrearFraseEnSistema("Maravilla Texaco", primeraFecha, autor);
            fachadaUsuario.CrearFraseEnSistema("Maravilla Texaco", primeraFecha, autor);
            Assert.AreEqual(1, fachadaUsuario.CalcularPromedioFrasesDiarias(autor));
        }
    }
}
