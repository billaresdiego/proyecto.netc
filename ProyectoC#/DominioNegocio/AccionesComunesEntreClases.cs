﻿namespace DominioNegocio
{
    public interface AccionesComunesEntreClases
    {
        void ValidarString(string unaCadenaCaracteres);
        bool ConvertirObjeto(object unObjeto);
    }
}
