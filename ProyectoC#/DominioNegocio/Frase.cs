﻿using System;
using System.Linq;
using DominioNegocio.Excepciones;
using DominioNegocio.UtilidadesValidarCondiciones;
using System.ComponentModel.DataAnnotations;

namespace DominioNegocio
{
    public class Frase : AccionesComunesEntreClases
    {
        private string contenidoFrase;
        private string categoria;
        private Entidad entidad;
        private DateTime fechaDeIngreso;

        [Key]
        private int id;

        private Autor autor;

        public Frase()
        {

        }

        public Frase(string frase, string categoria, Entidad entidad, DateTime fechaDeIngreso, Autor autor)
        {
            this.contenidoFrase = frase;
            this.categoria = categoria;
            this.entidad = entidad;
            this.fechaDeIngreso = fechaDeIngreso;
            this.autor = autor;
        }

        public int Id
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
            }
        }

        public DateTime FechaDeIngreso
        {
            get
            {
                return fechaDeIngreso;
            }
            set
            {
                fechaDeIngreso = new DateTime(value.Year, value.Month, value.Day, DateTime.Now.Hour, 0, 0);
            }
        }

        public Entidad UnaEntidad
        {
            get
            {
                return entidad;
            }
            set
            {
                entidad = value;
            }
        }

        public string UnaCategoria
        {
            get
            {
                return categoria;
            }
            set
            {
                ValidarString(value);
                ValidarCategoria(value);
                categoria = value;
            }
        }

        public Autor Autor
        {
            get
            {
                return autor;
            }
            set
            {
                ValidarAutor(value);
                autor = value;
            }
        }

        private static void ValidarAutor(Autor autorAValidar)
        {
            if (autorAValidar == null)
            {
                throw new ExcepcionAutorInvalido();
            }
        }

        private void ValidarCategoria(string categoria)
        {
            if (CategoriaInvalida(categoria))
            {
                throw new ExcepcionCategoriaInvalida();
            }
        }

        private bool CategoriaInvalida(string categoria)
        {
            return CategoriasPositivoNegativo(categoria) && categoria.ToLower() != "neutro";
        }

        private bool CategoriasPositivoNegativo(string categoria)
        {
            return categoria.ToLower() != "positivo" && categoria.ToLower() != "negativo";
        }

        public string ContenidoUnaFrase
        {
            get
            {
                return contenidoFrase;
            }
            set
            {
                ValidarString(value);
                ValidarFrase(value);
                contenidoFrase = value;
            }
        }

        public void ValidarString(string cadenaCaracteres)
        {
            if (HerramientasValidacionStrings.StringVacio(cadenaCaracteres))
            {
                throw new ExcepcionStringVacio();
            }

        }

        private void ValidarFrase(string frase)
        {
            if (HerramientasValidacionStrings.StringLargoMenorAlEspecificado(frase, 4))
            {
                throw new ExcepcionFraseLargoMenorDeCuatro();
            }
            if (!StringContieneCaracterEspecificado(frase, ' '))
            {
                throw new ExcepcionFraseSinEspacios();
            }
        }

        public static bool StringContieneCaracterEspecificado(string cadenaCaracteres, char caracter)
        {
            return cadenaCaracteres.Contains(caracter);
        }

        public override bool Equals(object unObjeto)
        {
            bool retorno;
            if (HerramientasValidacionObjetos.validarObjetoDeEquals(unObjeto, (Object)this))
            {
                retorno = false;
            }
            else
            {
                retorno = ConvertirObjeto(unObjeto);
            }
            return retorno;
        }

        public bool ConvertirObjeto(object unObjeto)
        {
            Frase fraseAComparar = (Frase)unObjeto;
            return fraseAComparar.ContenidoUnaFrase.Equals(this.ContenidoUnaFrase);
        }

        public override string ToString()
        {
            return this.ContenidoUnaFrase;
        }
    }
}
