﻿using DominioNegocio.UtilidadesValidarCondiciones;
using DominioNegocio.Excepciones;
using System;

namespace DominioNegocio
{
    public class Entidad : AccionesComunesEntreClases
    {
        private string nombreEntidad;
        public string NombreUnaEntidad
        {
            get
            {
                return this.nombreEntidad;
            }
            set
            {
                ValidarString(value);
                this.nombreEntidad = value;
                
            }
        }

        public void ValidarString(string unaCadenaCaracteres)
        {
            if (HerramientasValidacionStrings.StringVacio(unaCadenaCaracteres))
            {
                throw new ExcepcionStringVacio();
            }
        }

        public Entidad(string unNombre)
        {
            NombreUnaEntidad = unNombre;
        }

        public Entidad()
        {
            this.nombreEntidad = "";
        }

        public override bool Equals(object unObjeto)
        {
            bool retorno;
            if (HerramientasValidacionObjetos.validarObjetoDeEquals(unObjeto, (Object)this))
            {
                retorno = false;
            }
            else
            {
                retorno = ConvertirObjeto(unObjeto);
            }
            return retorno;
        }

        public bool ConvertirObjeto(Object unObjeto)
        {
            Entidad entidadAComparar = (Entidad)unObjeto;
            return entidadAComparar.NombreUnaEntidad.Equals(this.NombreUnaEntidad);
        }

        public override string ToString()
        {
            return this.NombreUnaEntidad;
        }
    }
}