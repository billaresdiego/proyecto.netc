﻿using DominioNegocio.Excepciones;
using DominioNegocio.UtilidadesValidarCondiciones;
using System;
using System.ComponentModel.DataAnnotations;

namespace DominioNegocio
{
    public class AlarmaAutor : ServidorAlarma
    {
        private int plazoDias;
        private int plazoHoras;
        private TipoAlarmaAutor unTipo;
        private bool activada;
        private int cantidadDePosts;

        [Key]
        private int id;

        public AlarmaAutor(TipoAlarmaAutor miTipo, int cantidadDePosts, bool activada, int plazoDias, int plazoHoras)
        { 
            this.unTipo = miTipo;
            this.cantidadDePosts = cantidadDePosts;
            this.activada = activada;
            this.plazoDias = plazoDias;
            this.plazoHoras = plazoHoras;
        }

        public AlarmaAutor()
        {
        }

        public int Id
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
            }
        }

        public int PlazoDias
        {
            get
            {
                return plazoDias;
            }
            set
            {
                plazoDias = value;
            }
        }

        public int PlazoHoras
        {
            get
            {
                return plazoHoras;
            }
            set
            {
                MayorA23(value);
                plazoHoras = value;
            }
        }

        public TipoAlarmaAutor UnTipo
        {
            get
            {
                return unTipo;
            }
            set
            {
                unTipo = value;
            }
        }

        public bool Activada
        {
            get
            {
                return activada;
            }
            set
            {
                activada = value;
            }
        }

        public int CantidadDePosts
        {
            get
            {
                return cantidadDePosts;
            }
            set
            {
                ValidarCantidadDePosts(value);
                cantidadDePosts = value;
            }
        }

        private void ValidarCantidadDePosts(int cantidad)
        {
            ValidarCantidadMayorACero(cantidad);
            ValidarCantidadMenorAMil(cantidad);
        }

        private static void ValidarCantidadMenorAMil(int cantidad)
        {
            if (cantidad > 999)
            {
                throw new ExcepcionCantidadInvalida();
            }
        }

        private static void ValidarCantidadMayorACero(int cantidad)
        {
            if (cantidad < 1)
            {
                throw new ExcepcionCeroNoValido();
            }
        }

        public enum TipoAlarmaAutor
        {
            positivo,
            sinTipo,
            negativo
        }

        private static void MayorA23(int plazo)
        {
            if (plazo > 23)
            {
                throw new ExcepcionHoraNoValida();
            }
        }

        public DateTime CalcularFechaComienzo()
        {
            DateTime fechaCalculada = DateTime.Now;
            if (PlazoHoras > 0)
            {
                fechaCalculada = DateTime.Now.AddHours(-PlazoHoras);
            }
            else
            {
                fechaCalculada = DateTime.Now.AddDays(-PlazoDias);
            }
            return fechaCalculada;
        }

        public override bool Equals(object unObjeto)
        {
            bool sonIguales;
            if (HerramientasValidacionObjetos.validarObjetoDeEquals(this, unObjeto))
            {
                sonIguales = false;
            }
            else
            {
                sonIguales = Comparar(unObjeto);
            }
            return sonIguales;
        }

        private bool Comparar(object unObjeto)
        {
            AlarmaAutor a = (AlarmaAutor)unObjeto;
            return a.PlazoDias == PlazoDias && a.plazoHoras == PlazoHoras &&
                   a.cantidadDePosts == CantidadDePosts && a.unTipo == UnTipo;
        }

        public Object DevolverAlarma()
        {
            return this;
        }
    }
}