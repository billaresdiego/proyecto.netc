﻿using DominioNegocio.Excepciones;
using DominioNegocio.UtilidadesValidarCondiciones;
using System;
using System.ComponentModel.DataAnnotations;

namespace DominioNegocio
{
    public class Alarma : ServidorAlarma
    {
        private Entidad unaEntidad;
        private Tipo unTipo;
        private int cantidadDePosts;
        private bool activada;
        private int plazoDias;
        private int plazoHoras;

        [Key]
        private int id;

        public Alarma(Entidad unaEntidad)
        {
            this.UnaEntidad = unaEntidad;
            this.unTipo = Alarma.Tipo.positivo;
            this.cantidadDePosts = Int32.MaxValue;
            this.activada = false;
            this.plazoDias = 0;
            this.PlazoHoras = 0;
        }

        public Alarma()
        {
        }

        public int Id
        {
            get
            {
                return this.id;
            }
            set
            {
                id = value;
            }
        }

        public Alarma(Entidad miEntidad, Tipo miTipo, int cantidadDePosts, bool activada, int plazoDias, int plazoHoras)
        {
            this.unaEntidad = miEntidad;
            this.unTipo = miTipo;
            this.cantidadDePosts = cantidadDePosts;
            this.activada = activada;
            this.plazoDias = plazoDias;
            this.plazoHoras = plazoHoras;
        }

        public Object DevolverAlarma()
        {
            return this;
        }

        public Entidad UnaEntidad
        {
            get
            {
                return this.unaEntidad;
            }
            set
            {
                ValidarEntidad(value);
                this.unaEntidad = value;
            }
        }

        private void ValidarEntidad(Entidad unaEntidad)
        {
            if (unaEntidad == null)
            {
                throw new ExcepcionAlarmaSinEntidad();
            }
        }

        public int CantidadDePosts
        {
            get
            {
                return this.cantidadDePosts;
            }
            set
            {
                ValidarCantidadDePosts(value);
                this.cantidadDePosts = value;
            }
        }

        private void ValidarCantidadDePosts(int cantidadDePosts)
        {
            if (HerramientasValidacionEnteros.EsIgualACero(cantidadDePosts))
            {
                throw new ExcepcionCeroNoValido();
            }
        }

        public Tipo UnTipo
        {
            get
            {
                return this.unTipo;
            }
            set
            {
                this.unTipo = value;
            }
        }

        public enum Tipo
        {
            sinTipo,
            positivo,
            negativo
        }

        public bool Activada
        {
            get
            {
                return this.activada;
            }
            set
            {
                this.activada = value;
            }
        }

        public int PlazoDias
        {
            get
            {
                return this.plazoDias;
            }
            set
            {
                this.plazoDias = value;
            }
        }

        public int PlazoHoras
        {
            get
            {
                return this.plazoHoras;
            }
            set
            {
                ValidarPlazoHoras(value);
                this.plazoHoras = value;
            }
        }

        private void ValidarPlazoHoras(int unPlazoHoras)
        {
            if (MayorA23(unPlazoHoras))
            {
                throw new ExcepcionHoraNoValida();
            }
        }

        private static bool MayorA23(int value)
        {
            return value > 23;
        }

        public DateTime CalcularFechaComienzo()
        {
            DateTime fechaComienzo = DateTime.Now.AddDays(-this.PlazoDias).AddHours(-this.PlazoHoras);
            return fechaComienzo;
        }

        public override bool Equals(object unObjeto)
        {
            bool retorno;
            if (HerramientasValidacionObjetos.validarObjetoDeEquals(unObjeto, (Object)this))
            {
                retorno = false;
            }
            else
            {
                retorno = ConvertirObjeto(unObjeto);
            }
            return retorno;
        }

        public bool ConvertirObjeto(Object unObjeto)
        {
            Alarma alarmaAComparar = (Alarma)unObjeto;
            return alarmaAComparar.UnaEntidad.Equals(this.UnaEntidad);
        }

        public override string ToString()
        {
            return "Entidad: " +this.UnaEntidad + ", Tipo: "+ this.UnTipo + ", Posts: " + this.CantidadDePosts;
        }
    }
}