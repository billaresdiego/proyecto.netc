﻿using DominioNegocio.Excepciones;
using DominioNegocio.UtilidadesValidarCondiciones;
using System;

namespace DominioNegocio
{
    public class Sentimiento : AccionesComunesEntreClases
    {

        private string nombreSentimiento;
        private TiposSentimiento tipoSentimiento;

        public Sentimiento()
        {
            this.nombreSentimiento = "";
            this.tipoSentimiento = TiposSentimiento.sinTipo;
        }

        public Sentimiento(TiposSentimiento unTipoSentimiento, string unSentimiento)
        {
            this.NombreSentimiento = unSentimiento;
            this.TipoSentimiento = unTipoSentimiento;
        }

        public TiposSentimiento TipoSentimiento
        {
            get
            {
                return tipoSentimiento;
            }
            set
            {
                tipoSentimiento = value;
            }
        }

        public enum TiposSentimiento
        {
            sinTipo,
            positivo,
            negativo
        }

        public string NombreSentimiento
        {
            get
            {
                return nombreSentimiento;
            }
            set
            {
                ValidarString(value);
                nombreSentimiento = DevolverSentimientoMinuscula(value);
            }
        }

        private string DevolverSentimientoMinuscula(string unSentimiento)
        {
            return unSentimiento.ToLower();
        }

        public void ValidarString(string unaCadenaCaracteres)
        {
            if (HerramientasValidacionStrings.StringLargoMenorAlEspecificado(unaCadenaCaracteres, 2))
            {
                throw new ExcepcionSentimientoLargoMenorDeDos();
            }

            ValidarCaracteres(unaCadenaCaracteres);
        }

        private void ValidarCaracteres(string unSentimiento)
        {
            foreach (char unaLetra in unSentimiento)
            {
                if (EsCaractereNuméricoOEspecial(unaLetra))
                {
                    throw new ExcepcionSentimientoConCaracteresNoAlfabeticos();
                }
            }
        }

        private bool EsCaractereNuméricoOEspecial(char unCaracter)
        {

            return !(CaracterAlfabetico(unCaracter) || CaracterEspacio(unCaracter));
        }

        private bool CaracterAlfabetico(char unCaracter)
        {
            return char.ToLower(unCaracter) >= 'a' && char.ToLower(unCaracter) <= 'z';
        }

        private bool CaracterEspacio(char caracter)
        {
            return caracter == ' ';
        }

        public override bool Equals(object unObjeto)
        {
            bool retorno;
            if (HerramientasValidacionObjetos.validarObjetoDeEquals(unObjeto, (Object)this))
            {
                retorno = false;
            }
            else
            {
                retorno = ConvertirObjeto(unObjeto);
            }
            return retorno;
        }

        public bool ConvertirObjeto(object unObjeto)
        {
            Sentimiento sentimientoAComparar = (Sentimiento)unObjeto;
            return sentimientoAComparar.NombreSentimiento.Equals(this.NombreSentimiento);
        }

        public override string ToString()
        {
            return NombreSentimiento;
        }
    }
}