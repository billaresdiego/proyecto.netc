﻿using System;

namespace DominioNegocio.Excepciones
{
    public class ExcepcionSentimientoSinTipo : Exception
    {
        public ExcepcionSentimientoSinTipo() : base("Aviso: el sentimiento no tiene tipo.")
        {
        }
    }

    public class ExcepcionSentimientoLargoMenorDeDos : Exception
    {
        public ExcepcionSentimientoLargoMenorDeDos() : base("Aviso: el sentimiento no ingresado es demasiado corto.")
        {
        }
    }

    public class ExcepcionSentimientoConCaracteresNoAlfabeticos : Exception
    {
        public ExcepcionSentimientoConCaracteresNoAlfabeticos() : base("Aviso: el sentimiento no puede contener caracteres especiales ni numéricos.")
        {
        }
    }
    public class ExcepcionSentimientoNoSePuedeBorrar : Exception
    {
        public ExcepcionSentimientoNoSePuedeBorrar() : base("Aviso: el sentimiento no se pudo borrar")
        {
        }
    }

}
