﻿using System;

namespace DominioNegocio.Excepciones
{

    public class ExcepcionAutorNoSePuedeBorrar : Exception
    {
        public ExcepcionAutorNoSePuedeBorrar() : base("Aviso: el autor no se pudo borrar")
        {
        }
    }

    public class ExcepcionAutorInvalido : Exception
    {
        public ExcepcionAutorInvalido() : base("Aviso: el autor no es válido")
        {
        }
    }
}
