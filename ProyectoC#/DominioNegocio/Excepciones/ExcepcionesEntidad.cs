﻿using System;

namespace DominioNegocio.Excepciones
{
    public class ExcepcionEntidadNoSePuedeBorrar : Exception
    {
        public ExcepcionEntidadNoSePuedeBorrar() : base("Aviso: la entidad no se pudo borrar")
        {
        }
    }
}
