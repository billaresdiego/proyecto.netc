﻿using System;

namespace DominioNegocio.Excepciones
{
    public class ExcepcionFechaPasada : Exception
    {
        public ExcepcionFechaPasada() : base("Aviso: la fecha es pasada.")
        {
        }
    }

    public class ExcepcionFechaActual : Exception
    {
        public ExcepcionFechaActual() : base("Aviso: la fecha es actual.")
        {
        }
    }

    public class ExcepcionFechaNoActual : Exception
    {
        public ExcepcionFechaNoActual() : base("Aviso: la fecha no es actual.")
        {
        }
    }

    public class ExcepcionFechaFutura : Exception
    {
        public ExcepcionFechaFutura() : base("Aviso: la fecha no puede ser futura.")
        {
        }
    }

    public class ExcepcionFechaNacimientoMinima : Exception
    {
        public ExcepcionFechaNacimientoMinima() : base("Aviso: el autor debe tener al menos 13 años.")
        {
        }
    }

    public class ExcepcionFechaNacimientoMaxima : Exception
    {
        public ExcepcionFechaNacimientoMaxima() : base("Aviso: el autor debe como máximo 100 años.")
        {
        }
    }
}
