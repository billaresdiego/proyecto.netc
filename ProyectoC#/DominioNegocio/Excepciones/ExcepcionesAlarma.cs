﻿using System;

namespace DominioNegocio.Excepciones
{
    public class ExcepcionAlarmaSinEntidad : Exception
    {
        public ExcepcionAlarmaSinEntidad() : base("Aviso: Se debe ingresar una entidad para configurar alarma")
        {
        }
    }

    public class ExcepcionCeroNoValido : Exception
    {
        public ExcepcionCeroNoValido() : base("Aviso: el cero no es válido en este caso.")
        {
        }
    }

    public class ExcepcionHoraNoValida : Exception
    {
        public ExcepcionHoraNoValida() : base("Aviso: la hora no es válida.")
        {
        }
    }

    public class ExcepcionCantidadInvalida : Exception
    {
        public ExcepcionCantidadInvalida() : base("Aviso: la cantidad no es válida.")
        {
        }
    }

    public class ExcepcionAlarmaNoSePuedeBorrar : Exception
    {
        public ExcepcionAlarmaNoSePuedeBorrar() : base("Aviso: la alarma no se puedo borrar")
        {

        }
    }

    public class ExcepcionAlarmaSinTipo : Exception
    {
        public ExcepcionAlarmaSinTipo() : base("Aviso: debe seleccionar un tipo de alarma")
        {

        }
    }
}
