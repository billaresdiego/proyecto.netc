﻿using System;

namespace DominioNegocio.Excepciones
{
    public class ExcepcionCategoriaInvalida : Exception
    {
        public ExcepcionCategoriaInvalida() : base("Aviso: la categoría es inválida.")
        {
        }
    }

    public class ExcepcionFraseLargoMenorDeCuatro : Exception
    {
        public ExcepcionFraseLargoMenorDeCuatro() : base("Aviso: la frase ingresada es demasiado corta.")
        {
        }
    }

    public class ExcepcionFraseSinEspacios : Exception
    {
        public ExcepcionFraseSinEspacios() : base("Aviso: la frase ingresada es inválida.")
        {
        }
    }

    public class ExcepcionFraseNoSePuedeBorrar : Exception
    {
        public ExcepcionFraseNoSePuedeBorrar() : base("Aviso: la frase no se puedo borrar")
        {

        }
    }
}
