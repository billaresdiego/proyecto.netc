﻿using System;

namespace DominioNegocio.Excepciones
{
    public class ExcepcionStringVacio : Exception
    {
        public ExcepcionStringVacio() : base("Aviso: campo(s) vacío(s).")
        {
        }
    }

    public class ExcepcionElStringAConvertirNoEsValido : Exception
    {
        public ExcepcionElStringAConvertirNoEsValido() : base("Aviso: Los campos numericos enteros solo permiten numeros " +
                                                                "y no pueden haber campos vacios")
        {

        }
    }

    public class ExcepcionStringNoAlfanumerico : Exception
    {
        public ExcepcionStringNoAlfanumerico() : base("")
        {

        }
    }

    public class ExcepcionStringLargoInvalido : Exception
    {
        public ExcepcionStringLargoInvalido() : base("")
        {

        }
    }

    public class ExcepcionStringNoNumerico : Exception
    {
        public ExcepcionStringNoNumerico() : base("")
        {

        }
    }
}
