﻿namespace DominioNegocio.UtilidadesValidarCondiciones
{
    public class HerramientasValidacionEnteros
    {
        public static bool EsIgualACero(int numero)
        {
            return numero == 0;
        }
    }
}
