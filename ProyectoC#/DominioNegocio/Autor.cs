﻿using DominioNegocio.Excepciones;
using DominioNegocio.UtilidadesValidarCondiciones;
using System;
using System.Text.RegularExpressions;

namespace DominioNegocio
{
    public class Autor
    {
        private string usuario;
        private string nombre;
        private string apellido;
        private DateTime fechaNacimiento;

        public string Usuario
        {
            get
            {
                return usuario;
            }
            set
            {
                ValidarUsuario(value);
                usuario = value;
            }
        }

        public string Apellido
        {
            get
            {
                return apellido;
            }
            set
            {
                ValidarNombreOApellido(value);
                apellido = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                ValidarNombreOApellido(value);
                nombre = value;
            }
        }

        public DateTime FechaNacimiento
        {
            get
            {
                return fechaNacimiento;
            }
            set
            {
                ValidarFechaNacimiento(value);
                fechaNacimiento = value;
            }
        }

        private static void ValidarFechaNacimiento(DateTime value)
        {
            ValidarFechaNacimientoMinima(value);
            ValidarFechaNacimientoMaxima(value);
        }

        private static void ValidarFechaNacimientoMaxima(DateTime value)
        {
            if (value.CompareTo(DateTime.UtcNow.AddYears(-101)) <= 0)
                throw new ExcepcionFechaNacimientoMaxima();
        }

        private static void ValidarFechaNacimientoMinima(DateTime value)
        {
            if (value.CompareTo(DateTime.UtcNow.AddYears(-12)) >= 0)
                throw new ExcepcionFechaNacimientoMinima();
        }

        private void ValidarUsuario(String usuarioAValidar)
        {
            ValidarStringVacio(usuarioAValidar);
            ValidarStringNoAlfanumerico(usuarioAValidar);
            ValidarLargoMaximoString(usuarioAValidar, 10);
        }

        private static void ValidarLargoMaximoString(string usuarioAValidar, int largo)
        {
            if (!HerramientasValidacionStrings.StringLargoMenorAlEspecificado(usuarioAValidar, largo))
                throw new ExcepcionStringLargoInvalido();
        }

        private static void ValidarStringNoAlfanumerico(string usuarioAValidar)
        {
            if (HerramientasValidacionStrings.StringContieneCaracterNoAlfanumerico(usuarioAValidar))
                throw new ExcepcionStringNoAlfanumerico();
        }

        private static void ValidarStringVacio(string usuarioAValidar)
        {
            if (HerramientasValidacionStrings.StringVacio(usuarioAValidar))
                throw new ExcepcionStringVacio();
        }

        private void ValidarNombreOApellido(string valorAValidar)
        {
            ValidarStringVacio(valorAValidar);
            ValidarStringNoAlfabetico(valorAValidar);
            ValidarLargoMaximoString(valorAValidar, 15);
        }

        private static void ValidarStringNoAlfabetico(string valorAValidar)
        {
            if (Regex.IsMatch(valorAValidar, @"[^(a-z)(A-Z)/s]"))
                throw new ExcepcionStringNoNumerico();
        }

        public override bool Equals(object unObjeto)
        {
            bool retorno;
            if (HerramientasValidacionObjetos.validarObjetoDeEquals(unObjeto, (Object)this))
            {
                retorno = false;
            }
            else
            {
                retorno = ConvertirObjeto(unObjeto);
            }
            return retorno;
        }

        public bool ConvertirObjeto(Object unObjeto)
        {
            Autor autorAComparar = (Autor)unObjeto;
            return autorAComparar.Usuario.Equals(this.Usuario);
        }

        public override string ToString()
        {
            return Nombre + " " + Apellido + ", " + Usuario + ", " + fechaNacimiento.ToShortDateString();
        }
    }
}
