﻿using System;

namespace DominioNegocio
{
    public interface ServidorAlarma
    {
        Object DevolverAlarma();
        int CantidadDePosts { get; set; }
        int Id { get; set; }

        bool Activada { get; set; }
    }
}
