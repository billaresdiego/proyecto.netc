﻿using System;
using System.Windows.Forms;
using LogicaNegocio;
using DominioNegocio;

namespace InterfazDeUsuario
{
    public partial class IngresarFrase : UserControl
    {
        FachadaUsuario fachadaSistema;
        public IngresarFrase(FachadaUsuario fachadaSistema)
        {
            InitializeComponent();
            this.fachadaSistema = fachadaSistema;
            RefrescarListaAutores();
            InicializarCalendario();
            EsconderMensaje();
            RefrescarNumeroAlarmasActivas();
        }

        private void RefrescarNumeroAlarmasActivas()
        {
            lblAlarmas.Text = "Número de Alarmas Activas: " + fachadaSistema.DevolverAlarmasActivadasPorFrases().Count;
        }

        private void InicializarCalendario()
        {
            clndrFechaIngreso.MaxSelectionCount = 1;
        }

        private void RefrescarListaAutores()
        {
            lstAutores.DataSource = null;
            lstAutores.DataSource = fachadaSistema.DevolverAutores();
        }

        private void EsconderMensaje()
        {
            lblMensaje.Text = "";
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            EsconderMensaje();
            Registrar();
            RefrescarListaAutores();
            RefrescarNumeroAlarmasActivas();
            ResetearCampos();
        }

        private void Registrar()
        {
            try
            {
                DateTime fechaSeleccionada = clndrFechaIngreso.SelectionRange.Start;
                Autor autorSeleccionado = (Autor)lstAutores.SelectedItem;
                fachadaSistema.CrearFraseEnSistema(this.rchTxtBxContenidoFrase.Text, fechaSeleccionada, autorSeleccionado);
                lblMensaje.Text = "Frase agregada.";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ResetearCampos()
        {
            this.rchTxtBxContenidoFrase.Text = "";
        }
    }
}
