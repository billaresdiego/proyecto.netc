﻿using System;
using System.Windows.Forms;
using LogicaNegocio;
using DominioNegocio;

namespace InterfazDeUsuario
{
    public partial class ConfigurarAlarma : UserControl
    {
        FachadaUsuario fachadaSistema;
        int plazo;
        int cantidadDePost;
        string tipoPlazo;

        public ConfigurarAlarma(FachadaUsuario fachadaSistema)
        {
            InitializeComponent();
            plazo = 0;
            cantidadDePost = 0;
            tipoPlazo = "";
            this.fachadaSistema = fachadaSistema;
            CargarEntidades();
            EsconderMensaje();
        }

        private void CargarEntidades()
        {
            foreach(Entidad unaEntidad in fachadaSistema.DevolverEntidades())
            {
                lstBoxEntidades.Items.Add(unaEntidad);
            }
        }

        private void EsconderMensaje()
        {
            lblMensaje.Text = "";
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            RegistrarAlarma();
            RestablecerCampos();
            RestablecerVariablesGlobales();
        }

        private void RegistrarAlarma()
        {
            try
            {
                EstablecerConfiguracionAlarma();
                lblMensaje.Text = "Alarma agregada";
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void EstablecerConfiguracionAlarma()
        {
            EstablecerCantidadDePost();
            DevolverCampoDiaUHoraValidado();
            DevolverPlazoSeleccionado();
            if (chckBxAlarmaAutor.Checked)
            {
                fachadaSistema.ConfigurarAlarmaAutor(cantidadDePost, plazo, boxTipoAlarma.Text, tipoPlazo);
            }
            else
            {
                fachadaSistema.ConfigurarAlarma((Entidad)lstBoxEntidades.SelectedItem, cantidadDePost,
                                                plazo, boxTipoAlarma.Text, tipoPlazo);
            }
        }

        private void EstablecerCantidadDePost()
        {
           cantidadDePost = fachadaSistema.ConvertirStringAInt(txtBoxCantidadPosts.Text);
        }

        private void DevolverCampoDiaUHoraValidado()
        {
            DevolverCampoDiaUHora();
        }

        private void DevolverCampoDiaUHora()
        {
            if (rBtnDías.Checked)
            {
                plazo = fachadaSistema.ConvertirStringAInt(txtBoxDias.Text);
            }
            else if(rBtnHoras.Checked)
            {
                plazo = fachadaSistema.ConvertirStringAInt(txtBoxHoras.Text);
            }
        }

        private void DevolverPlazoSeleccionado()
        {
            tipoPlazo = "Horas";
            if (rBtnDías.Checked)
            {
                tipoPlazo = "Dias";
            }
        }

        private void RestablecerCampos()
        {
            rBtnDías.Checked = false;
            rBtnHoras.Checked = false;
            txtBoxDias.Text = "";
            txtBoxHoras.Text = "";
            txtBoxCantidadPosts.Text = "";
            boxTipoAlarma.SelectedIndex = -1;
            lstBoxEntidades.SelectedIndex = -1;
        }

        private void RestablecerVariablesGlobales()
        {
            plazo = 0;
            cantidadDePost = 0;
            tipoPlazo = "";
        }

        private void chckBxAlarmaAutor_CheckedChanged(object sender, EventArgs e)
        {
            if (chckBxAlarmaAutor.Checked)
            {
                lstBoxEntidades.ClearSelected();
                lstBoxEntidades.Enabled = false;
            }
            else
            {
                lstBoxEntidades.Enabled = true;
            }
        }
    }
}
