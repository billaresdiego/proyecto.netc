﻿using System.Windows.Forms;
using LogicaNegocio;
using DominioNegocio;

namespace InterfazDeUsuario
{
    public partial class ReporteAlarmas : UserControl
    {
        FachadaUsuario fachadaSistema;

        public ReporteAlarmas(FachadaUsuario fachadaSistema)
        {
            InitializeComponent();
            this.fachadaSistema = fachadaSistema;
            CargarGrillaAlarmas();
            CargarGrillaAlarmasAutor();
        }

        private void CargarGrillaAlarmasAutor()
        {
            for (int numeroAlarma = 0; numeroAlarma < fachadaSistema.DevolverAlarmasAutorRegistradas().Count; numeroAlarma++)
            {
                gridListadoAA.Rows.Add();
                SetearCamposFilaAlarmaAutor(numeroAlarma);
            }
        }

        private void SetearCamposFilaAlarmaAutor(int numeroFila)
        {
            AlarmaAutor unaAlarma = fachadaSistema.DevolverAlarmasAutorRegistradas()[numeroFila];
            {
                gridListadoAA[0, numeroFila].Value = DevolverTipoAlarmaAutor(unaAlarma);
                gridListadoAA[1, numeroFila].Value = unaAlarma.CantidadDePosts;
                gridListadoAA[2, numeroFila].Value = unaAlarma.Activada;
                gridListadoAA[3, numeroFila].Value = DevolverPlazo(unaAlarma.PlazoDias, unaAlarma.PlazoHoras);
                gridListadoAA[4, numeroFila].Value = DevolverTipoPlazo(unaAlarma.PlazoDias);
            }
        }

        private string DevolverTipoAlarmaAutor(AlarmaAutor unaAlarma)
        {
            if(unaAlarma.UnTipo == AlarmaAutor.TipoAlarmaAutor.positivo)
            {
                return "Positivo";
            }
            else
            {
                return "Negativo";
            }
        }

        private void CargarGrillaAlarmas()
        {
            for (int numeroAlarma = 0; numeroAlarma < fachadaSistema.DevolverAlarmasRegistradas().Count; numeroAlarma++)
            {
                gridListado.Rows.Add();
                SetearCamposFilaAlarma(numeroAlarma);
            }
        }

        private void SetearCamposFilaAlarma(int numeroFila)
        {
            Alarma unaAlarma = fachadaSistema.DevolverAlarmasRegistradas()[numeroFila];
            {
                gridListado[0, numeroFila].Value = unaAlarma.UnaEntidad;
                gridListado[1, numeroFila].Value = unaAlarma.UnTipo;
                gridListado[2, numeroFila].Value = unaAlarma.CantidadDePosts;
                gridListado[3, numeroFila].Value = unaAlarma.Activada;
                gridListado[4, numeroFila].Value = DevolverPlazo(unaAlarma.PlazoDias, unaAlarma.PlazoHoras);
                gridListado[5, numeroFila].Value = DevolverTipoPlazo(unaAlarma.PlazoDias);
            }
        }

        private int DevolverPlazo(int plazoDias, int plazoHoras)
        {
            if (plazoDias != 0)
            {
                return plazoDias;
            }
            else
            {
                return plazoHoras;
            }
        }

        private string DevolverTipoPlazo(int plazoDias)
        {
            if (plazoDias != 0)
            {
                return "Días";
            }
            else
            {
                return "Horas";
            }
        }
    }
}
