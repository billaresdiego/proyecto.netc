﻿using LogicaNegocio;
using System;
using System.Windows.Forms;

namespace InterfazDeUsuario
{
    public partial class VentanaPrincipal : Form
    {
        FachadaUsuario fachadaSistema;
        public VentanaPrincipal()
        {
            InitializeComponent();
            fachadaSistema = new FachadaUsuario();
            CargarPanelInicial(fachadaSistema);
        }

        private void CargarPanelInicial(FachadaUsuario fachadaSistema)
        {
            pnlPanel.Controls.Clear();
            UserControl registrarSentimiento = new RegistrarSentimiento(fachadaSistema);
            pnlPanel.Controls.Add(registrarSentimiento);
        }

        private void btnRegistrarEntidad_Click(object sender, EventArgs e)
        {
            pnlPanel.Controls.Clear();
            UserControl registrarEntidad = new RegistrarEntidad(fachadaSistema);
            pnlPanel.Controls.Add(registrarEntidad);
        }

        private void btnRegistrarSentimiento_Click(object sender, EventArgs e)
        {
            CargarPanelInicial(fachadaSistema);
        }

        private void btnIngresarFrase_Click(object sender, EventArgs e)
        {
            pnlPanel.Controls.Clear();
            UserControl ingresarFrase = new IngresarFrase(fachadaSistema);
            pnlPanel.Controls.Add(ingresarFrase);
        }

        private void btnReporteAnalisis_Click(object sender, EventArgs e)
        {
            pnlPanel.Controls.Clear();
            UserControl reporteAnalisis = new ReporteAnalisis(fachadaSistema);
            pnlPanel.Controls.Add(reporteAnalisis);
        }

        private void btnCrearAlarma_Click(object sender, EventArgs e)
        {
            pnlPanel.Controls.Clear();
            UserControl configurarAlarma = new ConfigurarAlarma(fachadaSistema);
            pnlPanel.Controls.Add(configurarAlarma);
        }

        private void btnReporteAlarmas_Click(object sender, EventArgs e)
        {
            pnlPanel.Controls.Clear();
            UserControl configurarAlarma = new ReporteAlarmas(fachadaSistema);
            pnlPanel.Controls.Add(configurarAlarma);
        }

        private void btnIngresarAutor_Click(object sender, EventArgs e)
        {
            pnlPanel.Controls.Clear();
            UserControl ingresarAutor = new RegistrarAutor(fachadaSistema);
            pnlPanel.Controls.Add(ingresarAutor);
        }
    }
}
