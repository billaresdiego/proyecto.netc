﻿using System;
using System.Windows.Forms;
using LogicaNegocio;
using DominioNegocio;

namespace InterfazDeUsuario
{
    public partial class RegistrarAutor : UserControl
    {
        FachadaUsuario fachadaSistema;

        public RegistrarAutor(FachadaUsuario fachada)
        {
            InitializeComponent();
            this.fachadaSistema = fachada;
            RefrescarLista();
            lblMensaje.Text = "";
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            Registro();
            RefrescarLista();
            VaciarCampos();
        }

        private void RefrescarLista()
        {
            lstAutores.DataSource = null;
            lstAutores.DataSource = fachadaSistema.DevolverAutores();
        }

        private void Registro()
        {
            try
            {
                String nombre = txtNombre.Text;
                String apellido = txtBxApellido.Text;
                String usuario = txtBxUsuario.Text;
                DateTime fechaSeleccionada = clndrFechaNacimiento.SelectionRange.Start;
                fachadaSistema.CrearAutorEnSistema(nombre, apellido, usuario, fechaSeleccionada);
                lblMensaje.Text = "Autor creado con éxito";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            Autor autorAModificar = (Autor)lstAutores.SelectedItem;
            if (lstAutores.SelectedItem != null)
            {
                Modificar(autorAModificar);
            }
            RefrescarLista();
            VaciarCampos();
        }

        private void VaciarCampos()
        {
            txtBxApellido.Text = "";
            txtNombre.Text = "";
            txtBxUsuario.Text = "";
            clndrFechaNacimiento.SelectionRange.Start = DateTime.Now;
            lstAutores.SelectedIndex = -1;
        }

        private void Modificar(Autor autorAModificar)
        {
            try
            {
                Autor autorConAtributosAModificar = new Autor {
                    Nombre = txtNombre.Text,
                    Apellido = txtBxApellido.Text,
                    Usuario = txtBxUsuario.Text,
                    FechaNacimiento = clndrFechaNacimiento.SelectionRange.Start
                };
                fachadaSistema.ModificarAutor(autorAModificar, autorConAtributosAModificar);
                lblMensaje.Text = "Autor modificado con éxito";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            Autor autorABorrar = (Autor)lstAutores.SelectedItem;
            if (lstAutores.SelectedItem != null)
            {
                Borrar(autorABorrar);
            }
            RefrescarLista();
            VaciarCampos();
        }

        private void Borrar(Autor autorABorrar)
        {
            try
            {
                fachadaSistema.BorrarAutor(autorABorrar.Usuario);
                lblMensaje.Text = "Autor eliminado con éxito";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lstAutores_SelectedIndexChanged(object sender, EventArgs e)
        {
            Autor autorSeleccionado = (Autor)lstAutores.SelectedItem;
            if (autorSeleccionado != null)
            {
                txtNombre.Text = autorSeleccionado.Nombre;
                txtBxApellido.Text = autorSeleccionado.Apellido;
                txtBxUsuario.Text = autorSeleccionado.Usuario;
                clndrFechaNacimiento.SelectionStart = autorSeleccionado.FechaNacimiento;
            }
        }
    }
}
