﻿namespace InterfazDeUsuario
{
    partial class ReporteAlarmas
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitulo = new System.Windows.Forms.Label();
            this.gridListado = new System.Windows.Forms.DataGridView();
            this.Entidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tipo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CantidadPosts = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Activada = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Plazo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoPlazo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabAlarmas = new System.Windows.Forms.TabPage();
            this.tabAlarmasAutores = new System.Windows.Forms.TabPage();
            this.gridListadoAA = new System.Windows.Forms.DataGridView();
            this.Tipo2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CantidadPosts2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Activada2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Plazo2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoPlazo2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gridListado)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabAlarmas.SuspendLayout();
            this.tabAlarmasAutores.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridListadoAA)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.Location = new System.Drawing.Point(240, 36);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(172, 24);
            this.lblTitulo.TabIndex = 3;
            this.lblTitulo.Text = "Reporte de alarmas";
            // 
            // gridListado
            // 
            this.gridListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridListado.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Entidad,
            this.Tipo,
            this.CantidadPosts,
            this.Activada,
            this.Plazo,
            this.TipoPlazo});
            this.gridListado.Location = new System.Drawing.Point(0, 0);
            this.gridListado.Name = "gridListado";
            this.gridListado.Size = new System.Drawing.Size(649, 339);
            this.gridListado.TabIndex = 14;
            // 
            // Entidad
            // 
            this.Entidad.HeaderText = "Entidad";
            this.Entidad.Name = "Entidad";
            this.Entidad.ReadOnly = true;
            // 
            // Tipo
            // 
            this.Tipo.HeaderText = "Tipo";
            this.Tipo.Name = "Tipo";
            this.Tipo.ReadOnly = true;
            // 
            // CantidadPosts
            // 
            this.CantidadPosts.HeaderText = "Cantidad de posts";
            this.CantidadPosts.Name = "CantidadPosts";
            this.CantidadPosts.ReadOnly = true;
            // 
            // Activada
            // 
            this.Activada.HeaderText = "Activada";
            this.Activada.Name = "Activada";
            this.Activada.ReadOnly = true;
            // 
            // Plazo
            // 
            this.Plazo.HeaderText = "Plazo";
            this.Plazo.Name = "Plazo";
            this.Plazo.ReadOnly = true;
            // 
            // TipoPlazo
            // 
            this.TipoPlazo.HeaderText = "TipoPlazo";
            this.TipoPlazo.Name = "TipoPlazo";
            this.TipoPlazo.ReadOnly = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabAlarmas);
            this.tabControl1.Controls.Add(this.tabAlarmasAutores);
            this.tabControl1.Location = new System.Drawing.Point(47, 63);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(653, 365);
            this.tabControl1.TabIndex = 15;
            // 
            // tabAlarmas
            // 
            this.tabAlarmas.Controls.Add(this.gridListado);
            this.tabAlarmas.Location = new System.Drawing.Point(4, 22);
            this.tabAlarmas.Name = "tabAlarmas";
            this.tabAlarmas.Padding = new System.Windows.Forms.Padding(3);
            this.tabAlarmas.Size = new System.Drawing.Size(645, 339);
            this.tabAlarmas.TabIndex = 0;
            this.tabAlarmas.Text = "Alarmas";
            this.tabAlarmas.UseVisualStyleBackColor = true;
            // 
            // tabAlarmasAutores
            // 
            this.tabAlarmasAutores.Controls.Add(this.gridListadoAA);
            this.tabAlarmasAutores.Location = new System.Drawing.Point(4, 22);
            this.tabAlarmasAutores.Name = "tabAlarmasAutores";
            this.tabAlarmasAutores.Padding = new System.Windows.Forms.Padding(3);
            this.tabAlarmasAutores.Size = new System.Drawing.Size(645, 339);
            this.tabAlarmasAutores.TabIndex = 1;
            this.tabAlarmasAutores.Text = "Alarmas Autor";
            this.tabAlarmasAutores.UseVisualStyleBackColor = true;
            // 
            // gridListadoAA
            // 
            this.gridListadoAA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridListadoAA.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Tipo2,
            this.CantidadPosts2,
            this.Activada2,
            this.Plazo2,
            this.TipoPlazo2});
            this.gridListadoAA.Location = new System.Drawing.Point(52, 0);
            this.gridListadoAA.Name = "gridListadoAA";
            this.gridListadoAA.Size = new System.Drawing.Size(543, 339);
            this.gridListadoAA.TabIndex = 15;
            // 
            // Tipo2
            // 
            this.Tipo2.HeaderText = "Tipo";
            this.Tipo2.Name = "Tipo2";
            this.Tipo2.ReadOnly = true;
            // 
            // CantidadPosts2
            // 
            this.CantidadPosts2.HeaderText = "Cantidad de posts";
            this.CantidadPosts2.Name = "CantidadPosts2";
            this.CantidadPosts2.ReadOnly = true;
            // 
            // Activada2
            // 
            this.Activada2.HeaderText = "Activada";
            this.Activada2.Name = "Activada2";
            this.Activada2.ReadOnly = true;
            // 
            // Plazo2
            // 
            this.Plazo2.HeaderText = "Plazo";
            this.Plazo2.Name = "Plazo2";
            this.Plazo2.ReadOnly = true;
            // 
            // TipoPlazo2
            // 
            this.TipoPlazo2.HeaderText = "TipoPlazo";
            this.TipoPlazo2.Name = "TipoPlazo2";
            this.TipoPlazo2.ReadOnly = true;
            // 
            // ReporteAlarmas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.lblTitulo);
            this.Name = "ReporteAlarmas";
            this.Size = new System.Drawing.Size(750, 450);
            ((System.ComponentModel.ISupportInitialize)(this.gridListado)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabAlarmas.ResumeLayout(false);
            this.tabAlarmasAutores.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridListadoAA)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.DataGridView gridListado;
        private System.Windows.Forms.DataGridViewTextBoxColumn Entidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tipo;
        private System.Windows.Forms.DataGridViewTextBoxColumn CantidadPosts;
        private System.Windows.Forms.DataGridViewTextBoxColumn Activada;
        private System.Windows.Forms.DataGridViewTextBoxColumn Plazo;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoPlazo;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabAlarmas;
        private System.Windows.Forms.TabPage tabAlarmasAutores;
        private System.Windows.Forms.DataGridView gridListadoAA;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tipo2;
        private System.Windows.Forms.DataGridViewTextBoxColumn CantidadPosts2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Activada2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Plazo2;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoPlazo2;
    }
}
