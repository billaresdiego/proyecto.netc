﻿using System;
using System.Windows.Forms;
using LogicaNegocio;

namespace InterfazDeUsuario
{
    public partial class RegistrarEntidad : UserControl
    {
        FachadaUsuario fachadaSistema;
        public RegistrarEntidad(FachadaUsuario fachadaSistema)
        {
            InitializeComponent();
            this.fachadaSistema = fachadaSistema;
            EsconderMensaje();
        }

        private void EsconderMensaje()
        {
            lblMensaje.Text = "";
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            EsconderMensaje();
            Registro();
            ResetearCampos();
        }

        private void Registro()
        {
            try
            {
                fachadaSistema.CrearEntidadEnSistema(txtNombre.Text);
                lblMensaje.Text = "Entidad agregada.";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ResetearCampos()
        {
            txtNombre.Text = "";
        }
    }
}
