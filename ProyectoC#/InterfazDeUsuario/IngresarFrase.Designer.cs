﻿namespace InterfazDeUsuario
{
    partial class IngresarFrase
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitulo = new System.Windows.Forms.Label();
            this.rchTxtBxContenidoFrase = new System.Windows.Forms.RichTextBox();
            this.lblFrase = new System.Windows.Forms.Label();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.lblMensaje = new System.Windows.Forms.Label();
            this.clndrFechaIngreso = new System.Windows.Forms.MonthCalendar();
            this.lblAlarmas = new System.Windows.Forms.Label();
            this.lstAutores = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.Location = new System.Drawing.Point(240, 36);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(219, 24);
            this.lblTitulo.TabIndex = 2;
            this.lblTitulo.Text = "Ingresar una nueva frase";
            // 
            // rchTxtBxContenidoFrase
            // 
            this.rchTxtBxContenidoFrase.Location = new System.Drawing.Point(105, 130);
            this.rchTxtBxContenidoFrase.Name = "rchTxtBxContenidoFrase";
            this.rchTxtBxContenidoFrase.Size = new System.Drawing.Size(212, 55);
            this.rchTxtBxContenidoFrase.TabIndex = 4;
            this.rchTxtBxContenidoFrase.Text = "";
            // 
            // lblFrase
            // 
            this.lblFrase.AutoSize = true;
            this.lblFrase.Location = new System.Drawing.Point(63, 133);
            this.lblFrase.Name = "lblFrase";
            this.lblFrase.Size = new System.Drawing.Size(36, 13);
            this.lblFrase.TabIndex = 5;
            this.lblFrase.Text = "Frase:";
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.Location = new System.Drawing.Point(543, 395);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(150, 25);
            this.btnRegistrar.TabIndex = 6;
            this.btnRegistrar.Text = "Ingresar";
            this.btnRegistrar.UseVisualStyleBackColor = true;
            this.btnRegistrar.Click += new System.EventHandler(this.btnRegistrar_Click);
            // 
            // lblMensaje
            // 
            this.lblMensaje.AutoSize = true;
            this.lblMensaje.Location = new System.Drawing.Point(124, 246);
            this.lblMensaje.Name = "lblMensaje";
            this.lblMensaje.Size = new System.Drawing.Size(0, 13);
            this.lblMensaje.TabIndex = 7;
            // 
            // clndrFechaIngreso
            // 
            this.clndrFechaIngreso.Location = new System.Drawing.Point(90, 197);
            this.clndrFechaIngreso.Name = "clndrFechaIngreso";
            this.clndrFechaIngreso.TabIndex = 10;
            // 
            // lblAlarmas
            // 
            this.lblAlarmas.AutoSize = true;
            this.lblAlarmas.Location = new System.Drawing.Point(262, 395);
            this.lblAlarmas.Name = "lblAlarmas";
            this.lblAlarmas.Size = new System.Drawing.Size(140, 13);
            this.lblAlarmas.TabIndex = 11;
            this.lblAlarmas.Text = "Número de Alarmas Activas:";
            // 
            // lstAutores
            // 
            this.lstAutores.FormattingEnabled = true;
            this.lstAutores.Location = new System.Drawing.Point(361, 131);
            this.lstAutores.Name = "lstAutores";
            this.lstAutores.Size = new System.Drawing.Size(332, 225);
            this.lstAutores.TabIndex = 17;
            // 
            // IngresarFrase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lstAutores);
            this.Controls.Add(this.lblAlarmas);
            this.Controls.Add(this.clndrFechaIngreso);
            this.Controls.Add(this.lblMensaje);
            this.Controls.Add(this.btnRegistrar);
            this.Controls.Add(this.lblFrase);
            this.Controls.Add(this.rchTxtBxContenidoFrase);
            this.Controls.Add(this.lblTitulo);
            this.Name = "IngresarFrase";
            this.Size = new System.Drawing.Size(750, 450);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.RichTextBox rchTxtBxContenidoFrase;
        private System.Windows.Forms.Label lblFrase;
        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.Label lblMensaje;
        private System.Windows.Forms.MonthCalendar clndrFechaIngreso;
        private System.Windows.Forms.Label lblAlarmas;
        private System.Windows.Forms.ListBox lstAutores;
    }
}
