﻿using System;
using System.Windows.Forms;
using LogicaNegocio;
using DominioNegocio;

namespace InterfazDeUsuario
{
    public partial class RegistrarSentimiento : UserControl
    {
        FachadaUsuario fachadaSistema;
        public RegistrarSentimiento(FachadaUsuario fachadaSistema)
        {
            InitializeComponent();
            this.fachadaSistema = fachadaSistema;
            RefrescarLista();
            EsconderMensaje();
        }

        private void RefrescarLista()
        {
            lstBoxListaSentimientos.DataSource = null;
            lstBoxListaSentimientos.DataSource = fachadaSistema.DevolverSentimientos();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            EsconderMensaje();
            Registro();
            ResetearCampos();
            RefrescarLista();
        }
        private void EsconderMensaje()
        {
            lblMensaje.Text = "";
        }

        private void Registro()
        {
            try
            {
                AgregarSentimientoAPartir(comboTipo.SelectedIndex);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AgregarSentimientoAPartir(int indiceTipoSeleccionado)
        {
            switch (indiceTipoSeleccionado)
            {
                case -1:
                    fachadaSistema.CrearSentimientoSinTipoEnSistema(txtSentimiento.Text);
                    break;
                case 0:
                    fachadaSistema.CrearSentimientoPositivoEnSistema(txtSentimiento.Text);
                    break;
                case 1:
                    fachadaSistema.CrearSentimientoNegativoEnSistema(txtSentimiento.Text);
                    break;
            }
            lblMensaje.Text = "Sentimiento agregado.";
        }

        private void ResetearCampos()
        {
            txtSentimiento.Text = "";
            comboTipo.SelectedIndex = -1;
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            Sentimiento sentimientoABorrar = (Sentimiento)lstBoxListaSentimientos.SelectedItem;
            if(lstBoxListaSentimientos.SelectedItem != null)
            {
                Borrar(sentimientoABorrar);
            }
            RefrescarLista();
        }

        private void Borrar(Sentimiento sentimientoABorrar)
        {
            try
            {
                fachadaSistema.BorrarSentimiento(sentimientoABorrar.NombreSentimiento);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
