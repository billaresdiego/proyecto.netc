﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using LogicaNegocio;
using DominioNegocio;

namespace InterfazDeUsuario
{
    public partial class ReporteAnalisis : UserControl
    {
        FachadaUsuario fachadaSistema;
        public ReporteAnalisis(FachadaUsuario fachadaSistema)
        {
            InitializeComponent();
            this.fachadaSistema = fachadaSistema;
            CargarGrillaFrases();
            CargarGrillaAutores();
        }

        private void CargarGrillaAutores()
        {
            List<Autor> listaTodosAutores = fachadaSistema.DevolverAutores();
            for (int numeroAutor = 0; numeroAutor < listaTodosAutores.Count; numeroAutor++)
            {
                gridListado.Rows.Add();
                SetearCamposFilaAutor(listaTodosAutores, numeroAutor);
            }
        }

        private void SetearCamposFilaAutor(List<Autor> listaAutores, int numeroFila)
        {

            Autor unAutor = listaAutores[numeroFila];
            {
                gridListado[0, numeroFila].Value = unAutor.Nombre;
                gridListado[1, numeroFila].Value = unAutor.Apellido;
                gridListado[2, numeroFila].Value = unAutor.Usuario;
                gridListado[3, numeroFila].Value = unAutor.FechaNacimiento.ToString("dd/MM/yyyy");
            }

        }

        private void CargarGrillaFrases()
        {
            for (int numeroFrase = 0; numeroFrase < fachadaSistema.DevolverFrases().Count; numeroFrase++)
            {
                frasesCaracteristicasGrid.Rows.Add();
                SetearCamposFilaFrase(numeroFrase);
            }
        }

        private void SetearCamposFilaFrase(int numeroFila)
        {
            Frase unaFrase = fachadaSistema.DevolverFrasesConEntidadYFechaYHora()[numeroFila];
            frasesCaracteristicasGrid[0, numeroFila].Value = unaFrase.ContenidoUnaFrase;
            frasesCaracteristicasGrid[2, numeroFila].Value = unaFrase.UnaEntidad;
            frasesCaracteristicasGrid[3, numeroFila].Value = unaFrase.UnaCategoria;
            frasesCaracteristicasGrid[1, numeroFila].Value = unaFrase.FechaDeIngreso.ToString("dd/MM/yyyy");
            frasesCaracteristicasGrid[4, numeroFila].Value = unaFrase.Autor.Nombre+ unaFrase.Autor.Apellido;
        }

        private void comboBxCriterio_SelectedIndexChanged(object sender, EventArgs e)
        {
            gridReporteAutores.DataSource = null;
            gridReporteAutores.Rows.Clear();
            int indiceSeleccionado = comboBxCriterio.SelectedIndex;
            for (int numeroAutor = 0; numeroAutor < fachadaSistema.DevolverAutores().Count; numeroAutor++)
            {
                gridReporteAutores.Rows.Add();
                if (indiceSeleccionado == 0)
                {
                    CargarGrillaReportePorcentaje(numeroAutor);
                }
                else if (indiceSeleccionado == 1)
                {
                    CargarGrillaReporteEntidades(numeroAutor);
                }
                else if (indiceSeleccionado == 2)
                {
                    CargarGrillaReportPromedio(numeroAutor);
                }
            }
        }
        private void CargarGrillaReportePorcentaje(int numeroFila)
        {
            Autor unAutor = fachadaSistema.DevolverAutores()[numeroFila];
            {
                gridReporteAutores[0, numeroFila].Value = unAutor.Nombre;
                gridReporteAutores[1, numeroFila].Value = unAutor.Apellido;
                gridReporteAutores[2, numeroFila].Value = unAutor.Usuario;
                gridReporteAutores[3, numeroFila].Value = fachadaSistema.CalcularPorcentajeFrasesPositivasAutor(unAutor);
            }
        }

        private void CargarGrillaReportPromedio(int numeroFila)
        {
            Autor unAutor = fachadaSistema.DevolverAutores()[numeroFila];
            {
                gridReporteAutores[0, numeroFila].Value = unAutor.Nombre;
                gridReporteAutores[1, numeroFila].Value = unAutor.Apellido;
                gridReporteAutores[2, numeroFila].Value = unAutor.Usuario;
                gridReporteAutores[3, numeroFila].Value = fachadaSistema.CalcularPromedioFrasesDiarias(unAutor);
            }
        }

        private void CargarGrillaReporteEntidades(int numeroFila)
        {
            Autor unAutor = fachadaSistema.DevolverAutores()[numeroFila];
            {
                gridReporteAutores[0, numeroFila].Value = unAutor.Nombre;
                gridReporteAutores[1, numeroFila].Value = unAutor.Apellido;
                gridReporteAutores[2, numeroFila].Value = unAutor.Usuario;
                gridReporteAutores[3, numeroFila].Value = fachadaSistema.CalcularCantidadEntidadesMencionadas(unAutor);
            }
        }
    }
}
