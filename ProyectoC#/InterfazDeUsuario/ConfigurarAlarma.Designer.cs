﻿namespace InterfazDeUsuario
{
    partial class ConfigurarAlarma
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitulo = new System.Windows.Forms.Label();
            this.lstBoxEntidades = new System.Windows.Forms.ListBox();
            this.lblEntidad = new System.Windows.Forms.Label();
            this.lblTipo = new System.Windows.Forms.Label();
            this.boxTipoAlarma = new System.Windows.Forms.ComboBox();
            this.lblCantidadPosts = new System.Windows.Forms.Label();
            this.txtBoxCantidadPosts = new System.Windows.Forms.TextBox();
            this.txtBoxHoras = new System.Windows.Forms.TextBox();
            this.lblPlazoTiempo = new System.Windows.Forms.Label();
            this.txtBoxDias = new System.Windows.Forms.TextBox();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.rBtnDías = new System.Windows.Forms.RadioButton();
            this.rBtnHoras = new System.Windows.Forms.RadioButton();
            this.boxDíasHoras = new System.Windows.Forms.GroupBox();
            this.lblMensaje = new System.Windows.Forms.Label();
            this.chckBxAlarmaAutor = new System.Windows.Forms.CheckBox();
            this.boxDíasHoras.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.Location = new System.Drawing.Point(240, 36);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(261, 24);
            this.lblTitulo.TabIndex = 3;
            this.lblTitulo.Text = "Configurar una nueva alarma";
            // 
            // lstBoxEntidades
            // 
            this.lstBoxEntidades.FormattingEnabled = true;
            this.lstBoxEntidades.Location = new System.Drawing.Point(212, 130);
            this.lstBoxEntidades.Name = "lstBoxEntidades";
            this.lstBoxEntidades.Size = new System.Drawing.Size(130, 160);
            this.lstBoxEntidades.TabIndex = 4;
            // 
            // lblEntidad
            // 
            this.lblEntidad.AutoSize = true;
            this.lblEntidad.Location = new System.Drawing.Point(94, 130);
            this.lblEntidad.Name = "lblEntidad";
            this.lblEntidad.Size = new System.Drawing.Size(112, 13);
            this.lblEntidad.TabIndex = 5;
            this.lblEntidad.Text = "Seleccione la entidad:";
            // 
            // lblTipo
            // 
            this.lblTipo.AutoSize = true;
            this.lblTipo.Location = new System.Drawing.Point(382, 130);
            this.lblTipo.Name = "lblTipo";
            this.lblTipo.Size = new System.Drawing.Size(94, 13);
            this.lblTipo.TabIndex = 6;
            this.lblTipo.Text = "Seleccione el tipo:";
            // 
            // boxTipoAlarma
            // 
            this.boxTipoAlarma.FormattingEnabled = true;
            this.boxTipoAlarma.Items.AddRange(new object[] {
            "Positiva",
            "Negativa"});
            this.boxTipoAlarma.Location = new System.Drawing.Point(482, 127);
            this.boxTipoAlarma.Name = "boxTipoAlarma";
            this.boxTipoAlarma.Size = new System.Drawing.Size(121, 21);
            this.boxTipoAlarma.TabIndex = 7;
            // 
            // lblCantidadPosts
            // 
            this.lblCantidadPosts.AutoSize = true;
            this.lblCantidadPosts.Location = new System.Drawing.Point(381, 187);
            this.lblCantidadPosts.Name = "lblCantidadPosts";
            this.lblCantidadPosts.Size = new System.Drawing.Size(95, 13);
            this.lblCantidadPosts.TabIndex = 8;
            this.lblCantidadPosts.Text = "Cantidad de posts:";
            // 
            // txtBoxCantidadPosts
            // 
            this.txtBoxCantidadPosts.Location = new System.Drawing.Point(539, 184);
            this.txtBoxCantidadPosts.Name = "txtBoxCantidadPosts";
            this.txtBoxCantidadPosts.Size = new System.Drawing.Size(31, 20);
            this.txtBoxCantidadPosts.TabIndex = 9;
            // 
            // txtBoxHoras
            // 
            this.txtBoxHoras.Location = new System.Drawing.Point(539, 267);
            this.txtBoxHoras.Name = "txtBoxHoras";
            this.txtBoxHoras.Size = new System.Drawing.Size(31, 20);
            this.txtBoxHoras.TabIndex = 10;
            // 
            // lblPlazoTiempo
            // 
            this.lblPlazoTiempo.AutoSize = true;
            this.lblPlazoTiempo.Location = new System.Drawing.Point(381, 241);
            this.lblPlazoTiempo.Name = "lblPlazoTiempo";
            this.lblPlazoTiempo.Size = new System.Drawing.Size(85, 13);
            this.lblPlazoTiempo.TabIndex = 11;
            this.lblPlazoTiempo.Text = "Plazo de tiempo:";
            // 
            // txtBoxDias
            // 
            this.txtBoxDias.Location = new System.Drawing.Point(539, 238);
            this.txtBoxDias.Name = "txtBoxDias";
            this.txtBoxDias.Size = new System.Drawing.Size(31, 20);
            this.txtBoxDias.TabIndex = 12;
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.Location = new System.Drawing.Point(453, 316);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(150, 25);
            this.btnRegistrar.TabIndex = 15;
            this.btnRegistrar.Text = "Registrar";
            this.btnRegistrar.UseVisualStyleBackColor = true;
            this.btnRegistrar.Click += new System.EventHandler(this.btnRegistrar_Click);
            // 
            // rBtnDías
            // 
            this.rBtnDías.AccessibleName = "";
            this.rBtnDías.AutoSize = true;
            this.rBtnDías.Location = new System.Drawing.Point(6, 14);
            this.rBtnDías.Name = "rBtnDías";
            this.rBtnDías.Size = new System.Drawing.Size(48, 17);
            this.rBtnDías.TabIndex = 16;
            this.rBtnDías.TabStop = true;
            this.rBtnDías.Text = "Días";
            this.rBtnDías.UseVisualStyleBackColor = true;
            // 
            // rBtnHoras
            // 
            this.rBtnHoras.AccessibleName = "";
            this.rBtnHoras.AutoSize = true;
            this.rBtnHoras.Location = new System.Drawing.Point(6, 42);
            this.rBtnHoras.Name = "rBtnHoras";
            this.rBtnHoras.Size = new System.Drawing.Size(53, 17);
            this.rBtnHoras.TabIndex = 17;
            this.rBtnHoras.TabStop = true;
            this.rBtnHoras.Text = "Horas";
            this.rBtnHoras.UseVisualStyleBackColor = true;
            // 
            // boxDíasHoras
            // 
            this.boxDíasHoras.Controls.Add(this.rBtnDías);
            this.boxDíasHoras.Controls.Add(this.rBtnHoras);
            this.boxDíasHoras.Location = new System.Drawing.Point(472, 226);
            this.boxDíasHoras.Margin = new System.Windows.Forms.Padding(0);
            this.boxDíasHoras.Name = "boxDíasHoras";
            this.boxDíasHoras.Size = new System.Drawing.Size(64, 64);
            this.boxDíasHoras.TabIndex = 18;
            this.boxDíasHoras.TabStop = false;
            // 
            // lblMensaje
            // 
            this.lblMensaje.AutoSize = true;
            this.lblMensaje.Location = new System.Drawing.Point(450, 344);
            this.lblMensaje.Name = "lblMensaje";
            this.lblMensaje.Size = new System.Drawing.Size(38, 13);
            this.lblMensaje.TabIndex = 19;
            this.lblMensaje.Text = "Avisos";
            // 
            // chckBxAlarmaAutor
            // 
            this.chckBxAlarmaAutor.AutoSize = true;
            this.chckBxAlarmaAutor.Location = new System.Drawing.Point(310, 98);
            this.chckBxAlarmaAutor.Name = "chckBxAlarmaAutor";
            this.chckBxAlarmaAutor.Size = new System.Drawing.Size(156, 17);
            this.chckBxAlarmaAutor.TabIndex = 20;
            this.chckBxAlarmaAutor.Text = "Alarma centrada en autores";
            this.chckBxAlarmaAutor.UseVisualStyleBackColor = true;
            this.chckBxAlarmaAutor.CheckedChanged += new System.EventHandler(this.chckBxAlarmaAutor_CheckedChanged);
            // 
            // ConfigurarAlarma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chckBxAlarmaAutor);
            this.Controls.Add(this.lblMensaje);
            this.Controls.Add(this.boxDíasHoras);
            this.Controls.Add(this.btnRegistrar);
            this.Controls.Add(this.txtBoxDias);
            this.Controls.Add(this.lblPlazoTiempo);
            this.Controls.Add(this.txtBoxHoras);
            this.Controls.Add(this.txtBoxCantidadPosts);
            this.Controls.Add(this.lblCantidadPosts);
            this.Controls.Add(this.boxTipoAlarma);
            this.Controls.Add(this.lblTipo);
            this.Controls.Add(this.lblEntidad);
            this.Controls.Add(this.lstBoxEntidades);
            this.Controls.Add(this.lblTitulo);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "ConfigurarAlarma";
            this.Size = new System.Drawing.Size(750, 450);
            this.boxDíasHoras.ResumeLayout(false);
            this.boxDíasHoras.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.ListBox lstBoxEntidades;
        private System.Windows.Forms.Label lblEntidad;
        private System.Windows.Forms.Label lblTipo;
        private System.Windows.Forms.ComboBox boxTipoAlarma;
        private System.Windows.Forms.Label lblCantidadPosts;
        private System.Windows.Forms.TextBox txtBoxCantidadPosts;
        private System.Windows.Forms.TextBox txtBoxHoras;
        private System.Windows.Forms.Label lblPlazoTiempo;
        private System.Windows.Forms.TextBox txtBoxDias;
        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.RadioButton rBtnDías;
        private System.Windows.Forms.RadioButton rBtnHoras;
        private System.Windows.Forms.GroupBox boxDíasHoras;
        private System.Windows.Forms.Label lblMensaje;
        private System.Windows.Forms.CheckBox chckBxAlarmaAutor;
    }
}
