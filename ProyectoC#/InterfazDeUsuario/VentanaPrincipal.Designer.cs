﻿namespace InterfazDeUsuario
{
    partial class VentanaPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRegistrarSentimiento = new System.Windows.Forms.Button();
            this.btnRegistrarEntidad = new System.Windows.Forms.Button();
            this.btnIngresarFrase = new System.Windows.Forms.Button();
            this.btnCrearAlarma = new System.Windows.Forms.Button();
            this.btnReporteAnalisis = new System.Windows.Forms.Button();
            this.btnReporteAlarmas = new System.Windows.Forms.Button();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.pnlPanel = new System.Windows.Forms.Panel();
            this.btnIngresarAutor = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnRegistrarSentimiento
            // 
            this.btnRegistrarSentimiento.Location = new System.Drawing.Point(69, 168);
            this.btnRegistrarSentimiento.Name = "btnRegistrarSentimiento";
            this.btnRegistrarSentimiento.Size = new System.Drawing.Size(150, 25);
            this.btnRegistrarSentimiento.TabIndex = 0;
            this.btnRegistrarSentimiento.Text = "Registrar/Borrar Sentimiento";
            this.btnRegistrarSentimiento.UseVisualStyleBackColor = true;
            this.btnRegistrarSentimiento.Click += new System.EventHandler(this.btnRegistrarSentimiento_Click);
            // 
            // btnRegistrarEntidad
            // 
            this.btnRegistrarEntidad.Location = new System.Drawing.Point(69, 221);
            this.btnRegistrarEntidad.Name = "btnRegistrarEntidad";
            this.btnRegistrarEntidad.Size = new System.Drawing.Size(150, 25);
            this.btnRegistrarEntidad.TabIndex = 1;
            this.btnRegistrarEntidad.Text = "Registrar Entidad";
            this.btnRegistrarEntidad.UseVisualStyleBackColor = true;
            this.btnRegistrarEntidad.Click += new System.EventHandler(this.btnRegistrarEntidad_Click);
            // 
            // btnIngresarFrase
            // 
            this.btnIngresarFrase.Location = new System.Drawing.Point(69, 275);
            this.btnIngresarFrase.Name = "btnIngresarFrase";
            this.btnIngresarFrase.Size = new System.Drawing.Size(150, 25);
            this.btnIngresarFrase.TabIndex = 2;
            this.btnIngresarFrase.Text = "Ingresar Frase";
            this.btnIngresarFrase.UseVisualStyleBackColor = true;
            this.btnIngresarFrase.Click += new System.EventHandler(this.btnIngresarFrase_Click);
            // 
            // btnCrearAlarma
            // 
            this.btnCrearAlarma.Location = new System.Drawing.Point(69, 329);
            this.btnCrearAlarma.Name = "btnCrearAlarma";
            this.btnCrearAlarma.Size = new System.Drawing.Size(150, 25);
            this.btnCrearAlarma.TabIndex = 3;
            this.btnCrearAlarma.Text = "Configurar Alarma";
            this.btnCrearAlarma.UseVisualStyleBackColor = true;
            this.btnCrearAlarma.Click += new System.EventHandler(this.btnCrearAlarma_Click);
            // 
            // btnReporteAnalisis
            // 
            this.btnReporteAnalisis.Location = new System.Drawing.Point(69, 429);
            this.btnReporteAnalisis.Name = "btnReporteAnalisis";
            this.btnReporteAnalisis.Size = new System.Drawing.Size(150, 25);
            this.btnReporteAnalisis.TabIndex = 4;
            this.btnReporteAnalisis.Text = "Generar reporte de análisis";
            this.btnReporteAnalisis.UseVisualStyleBackColor = true;
            this.btnReporteAnalisis.Click += new System.EventHandler(this.btnReporteAnalisis_Click);
            // 
            // btnReporteAlarmas
            // 
            this.btnReporteAlarmas.Location = new System.Drawing.Point(69, 482);
            this.btnReporteAlarmas.Name = "btnReporteAlarmas";
            this.btnReporteAlarmas.Size = new System.Drawing.Size(150, 25);
            this.btnReporteAlarmas.TabIndex = 5;
            this.btnReporteAlarmas.Text = "Generar reporte de alarmas";
            this.btnReporteAlarmas.UseVisualStyleBackColor = true;
            this.btnReporteAlarmas.Click += new System.EventHandler(this.btnReporteAlarmas_Click);
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Modern No. 20", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.Location = new System.Drawing.Point(410, 9);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(318, 65);
            this.lblTitulo.TabIndex = 6;
            this.lblTitulo.Text = "Analizador";
            // 
            // pnlPanel
            // 
            this.pnlPanel.Location = new System.Drawing.Point(287, 121);
            this.pnlPanel.Name = "pnlPanel";
            this.pnlPanel.Size = new System.Drawing.Size(750, 450);
            this.pnlPanel.TabIndex = 7;
            // 
            // btnIngresarAutor
            // 
            this.btnIngresarAutor.Location = new System.Drawing.Point(69, 383);
            this.btnIngresarAutor.Name = "btnIngresarAutor";
            this.btnIngresarAutor.Size = new System.Drawing.Size(150, 25);
            this.btnIngresarAutor.TabIndex = 8;
            this.btnIngresarAutor.Text = "Ingresar Autor";
            this.btnIngresarAutor.UseVisualStyleBackColor = true;
            this.btnIngresarAutor.Click += new System.EventHandler(this.btnIngresarAutor_Click);
            // 
            // VentanaPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1084, 611);
            this.Controls.Add(this.btnIngresarAutor);
            this.Controls.Add(this.pnlPanel);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.btnReporteAlarmas);
            this.Controls.Add(this.btnReporteAnalisis);
            this.Controls.Add(this.btnCrearAlarma);
            this.Controls.Add(this.btnIngresarFrase);
            this.Controls.Add(this.btnRegistrarEntidad);
            this.Controls.Add(this.btnRegistrarSentimiento);
            this.Name = "VentanaPrincipal";
            this.Text = "Analizador";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRegistrarSentimiento;
        private System.Windows.Forms.Button btnRegistrarEntidad;
        private System.Windows.Forms.Button btnIngresarFrase;
        private System.Windows.Forms.Button btnCrearAlarma;
        private System.Windows.Forms.Button btnReporteAnalisis;
        private System.Windows.Forms.Button btnReporteAlarmas;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Panel pnlPanel;
        private System.Windows.Forms.Button btnIngresarAutor;
    }
}

