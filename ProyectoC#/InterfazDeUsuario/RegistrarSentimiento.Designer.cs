﻿namespace InterfazDeUsuario
{
    partial class RegistrarSentimiento
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitulo = new System.Windows.Forms.Label();
            this.btnRegistrar = new System.Windows.Forms.Button();
            this.lblSentimiento = new System.Windows.Forms.Label();
            this.txtSentimiento = new System.Windows.Forms.TextBox();
            this.lblTipo = new System.Windows.Forms.Label();
            this.comboTipo = new System.Windows.Forms.ComboBox();
            this.lblMensaje = new System.Windows.Forms.Label();
            this.btnBorrar = new System.Windows.Forms.Button();
            this.lstBoxListaSentimientos = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.Location = new System.Drawing.Point(240, 36);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(313, 24);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "Registrar o eliminar un sentimiento";
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.Location = new System.Drawing.Point(171, 273);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(150, 25);
            this.btnRegistrar.TabIndex = 1;
            this.btnRegistrar.Text = "Registrar";
            this.btnRegistrar.UseVisualStyleBackColor = true;
            this.btnRegistrar.Click += new System.EventHandler(this.btnRegistrar_Click);
            // 
            // lblSentimiento
            // 
            this.lblSentimiento.AutoSize = true;
            this.lblSentimiento.Location = new System.Drawing.Point(104, 130);
            this.lblSentimiento.Name = "lblSentimiento";
            this.lblSentimiento.Size = new System.Drawing.Size(65, 13);
            this.lblSentimiento.TabIndex = 2;
            this.lblSentimiento.Text = "Sentimiento:";
            // 
            // txtSentimiento
            // 
            this.txtSentimiento.Location = new System.Drawing.Point(175, 127);
            this.txtSentimiento.Name = "txtSentimiento";
            this.txtSentimiento.Size = new System.Drawing.Size(146, 20);
            this.txtSentimiento.TabIndex = 3;
            // 
            // lblTipo
            // 
            this.lblTipo.AutoSize = true;
            this.lblTipo.Location = new System.Drawing.Point(78, 200);
            this.lblTipo.Name = "lblTipo";
            this.lblTipo.Size = new System.Drawing.Size(91, 13);
            this.lblTipo.TabIndex = 4;
            this.lblTipo.Text = "Seleccione el tipo";
            // 
            // comboTipo
            // 
            this.comboTipo.AutoCompleteCustomSource.AddRange(new string[] {
            "positivo",
            "negativo"});
            this.comboTipo.FormattingEnabled = true;
            this.comboTipo.Items.AddRange(new object[] {
            "Positivo",
            "Negativo"});
            this.comboTipo.Location = new System.Drawing.Point(175, 197);
            this.comboTipo.Name = "comboTipo";
            this.comboTipo.Size = new System.Drawing.Size(146, 21);
            this.comboTipo.TabIndex = 5;
            // 
            // lblMensaje
            // 
            this.lblMensaje.AutoSize = true;
            this.lblMensaje.Location = new System.Drawing.Point(446, 301);
            this.lblMensaje.Name = "lblMensaje";
            this.lblMensaje.Size = new System.Drawing.Size(0, 13);
            this.lblMensaje.TabIndex = 6;
            // 
            // btnBorrar
            // 
            this.btnBorrar.Location = new System.Drawing.Point(452, 273);
            this.btnBorrar.Name = "btnBorrar";
            this.btnBorrar.Size = new System.Drawing.Size(150, 25);
            this.btnBorrar.TabIndex = 7;
            this.btnBorrar.Text = "Eliminar";
            this.btnBorrar.UseVisualStyleBackColor = true;
            this.btnBorrar.Click += new System.EventHandler(this.btnBorrar_Click);
            // 
            // lstBoxListaSentimientos
            // 
            this.lstBoxListaSentimientos.FormattingEnabled = true;
            this.lstBoxListaSentimientos.Location = new System.Drawing.Point(452, 127);
            this.lstBoxListaSentimientos.Name = "lstBoxListaSentimientos";
            this.lstBoxListaSentimientos.Size = new System.Drawing.Size(150, 134);
            this.lstBoxListaSentimientos.TabIndex = 8;
            // 
            // RegistrarSentimiento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lstBoxListaSentimientos);
            this.Controls.Add(this.btnBorrar);
            this.Controls.Add(this.lblMensaje);
            this.Controls.Add(this.comboTipo);
            this.Controls.Add(this.lblTipo);
            this.Controls.Add(this.txtSentimiento);
            this.Controls.Add(this.lblSentimiento);
            this.Controls.Add(this.btnRegistrar);
            this.Controls.Add(this.lblTitulo);
            this.Name = "RegistrarSentimiento";
            this.Size = new System.Drawing.Size(750, 450);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Button btnRegistrar;
        private System.Windows.Forms.Label lblSentimiento;
        private System.Windows.Forms.TextBox txtSentimiento;
        private System.Windows.Forms.Label lblTipo;
        private System.Windows.Forms.ComboBox comboTipo;
        private System.Windows.Forms.Label lblMensaje;
        private System.Windows.Forms.Button btnBorrar;
        private System.Windows.Forms.ListBox lstBoxListaSentimientos;
    }
}
