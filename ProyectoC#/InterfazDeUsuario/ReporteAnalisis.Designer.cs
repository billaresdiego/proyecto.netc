﻿namespace InterfazDeUsuario
{
    partial class ReporteAnalisis
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitulo = new System.Windows.Forms.Label();
            this.frasesCaracteristicasGrid = new System.Windows.Forms.DataGridView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabAutores = new System.Windows.Forms.TabPage();
            this.gridListado = new System.Windows.Forms.DataGridView();
            this.Nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Apellido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Usuario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FechaNac = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabFrases = new System.Windows.Forms.TabPage();
            this.tabReporteAutores = new System.Windows.Forms.TabPage();
            this.comboBxCriterio = new System.Windows.Forms.ComboBox();
            this.gridReporteAutores = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Valor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Frase = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Entidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Categoria = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Autor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.frasesCaracteristicasGrid)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabAutores.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridListado)).BeginInit();
            this.tabFrases.SuspendLayout();
            this.tabReporteAutores.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridReporteAutores)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.Location = new System.Drawing.Point(240, 36);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(170, 24);
            this.lblTitulo.TabIndex = 2;
            this.lblTitulo.Text = "Reporte de análisis";
            // 
            // frasesCaracteristicasGrid
            // 
            this.frasesCaracteristicasGrid.AllowUserToAddRows = false;
            this.frasesCaracteristicasGrid.AllowUserToDeleteRows = false;
            this.frasesCaracteristicasGrid.AllowUserToOrderColumns = true;
            this.frasesCaracteristicasGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Frase,
            this.Fecha,
            this.Entidad,
            this.Categoria,
            this.Autor});
            this.frasesCaracteristicasGrid.Location = new System.Drawing.Point(0, 0);
            this.frasesCaracteristicasGrid.Name = "frasesCaracteristicasGrid";
            this.frasesCaracteristicasGrid.ReadOnly = true;
            this.frasesCaracteristicasGrid.Size = new System.Drawing.Size(694, 334);
            this.frasesCaracteristicasGrid.TabIndex = 3;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabAutores);
            this.tabControl1.Controls.Add(this.tabFrases);
            this.tabControl1.Controls.Add(this.tabReporteAutores);
            this.tabControl1.Location = new System.Drawing.Point(25, 63);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(711, 372);
            this.tabControl1.TabIndex = 4;
            // 
            // tabAutores
            // 
            this.tabAutores.Controls.Add(this.gridListado);
            this.tabAutores.Location = new System.Drawing.Point(4, 22);
            this.tabAutores.Name = "tabAutores";
            this.tabAutores.Padding = new System.Windows.Forms.Padding(3);
            this.tabAutores.Size = new System.Drawing.Size(703, 346);
            this.tabAutores.TabIndex = 0;
            this.tabAutores.Text = "Autores";
            this.tabAutores.UseVisualStyleBackColor = true;
            // 
            // gridListado
            // 
            this.gridListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridListado.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nombre,
            this.Apellido,
            this.Usuario,
            this.FechaNac});
            this.gridListado.Location = new System.Drawing.Point(141, 6);
            this.gridListado.Name = "gridListado";
            this.gridListado.Size = new System.Drawing.Size(443, 337);
            this.gridListado.TabIndex = 12;
            // 
            // Nombre
            // 
            this.Nombre.HeaderText = "Nombre";
            this.Nombre.Name = "Nombre";
            this.Nombre.ReadOnly = true;
            // 
            // Apellido
            // 
            this.Apellido.HeaderText = "Apellido";
            this.Apellido.Name = "Apellido";
            this.Apellido.ReadOnly = true;
            // 
            // Usuario
            // 
            this.Usuario.HeaderText = "Usuario";
            this.Usuario.Name = "Usuario";
            this.Usuario.ReadOnly = true;
            // 
            // FechaNac
            // 
            this.FechaNac.HeaderText = "Fecha de nacimiento";
            this.FechaNac.Name = "FechaNac";
            this.FechaNac.ReadOnly = true;
            // 
            // tabFrases
            // 
            this.tabFrases.Controls.Add(this.frasesCaracteristicasGrid);
            this.tabFrases.Location = new System.Drawing.Point(4, 22);
            this.tabFrases.Name = "tabFrases";
            this.tabFrases.Padding = new System.Windows.Forms.Padding(3);
            this.tabFrases.Size = new System.Drawing.Size(703, 346);
            this.tabFrases.TabIndex = 1;
            this.tabFrases.Text = "Frases";
            this.tabFrases.UseVisualStyleBackColor = true;
            // 
            // tabReporteAutores
            // 
            this.tabReporteAutores.Controls.Add(this.label1);
            this.tabReporteAutores.Controls.Add(this.comboBxCriterio);
            this.tabReporteAutores.Controls.Add(this.gridReporteAutores);
            this.tabReporteAutores.Location = new System.Drawing.Point(4, 22);
            this.tabReporteAutores.Name = "tabReporteAutores";
            this.tabReporteAutores.Size = new System.Drawing.Size(703, 346);
            this.tabReporteAutores.TabIndex = 2;
            this.tabReporteAutores.Text = "Reporte de autores";
            this.tabReporteAutores.UseVisualStyleBackColor = true;
            // 
            // comboBxCriterio
            // 
            this.comboBxCriterio.FormattingEnabled = true;
            this.comboBxCriterio.Items.AddRange(new object[] {
            "Porcentaje frases positivas",
            "Cantidad de entidades",
            "Promedio diario"});
            this.comboBxCriterio.Location = new System.Drawing.Point(271, 13);
            this.comboBxCriterio.Name = "comboBxCriterio";
            this.comboBxCriterio.Size = new System.Drawing.Size(201, 21);
            this.comboBxCriterio.TabIndex = 14;
            this.comboBxCriterio.SelectedIndexChanged += new System.EventHandler(this.comboBxCriterio_SelectedIndexChanged);
            // 
            // gridReporteAutores
            // 
            this.gridReporteAutores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridReporteAutores.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.Valor});
            this.gridReporteAutores.Location = new System.Drawing.Point(130, 43);
            this.gridReporteAutores.Name = "gridReporteAutores";
            this.gridReporteAutores.Size = new System.Drawing.Size(443, 299);
            this.gridReporteAutores.TabIndex = 13;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Nombre";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Apellido";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Usuario";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // Valor
            // 
            this.Valor.HeaderText = "Valor";
            this.Valor.Name = "Valor";
            this.Valor.ReadOnly = true;
            // 
            // Frase
            // 
            this.Frase.HeaderText = "Frase";
            this.Frase.Name = "Frase";
            this.Frase.ReadOnly = true;
            this.Frase.Width = 300;
            // 
            // Fecha
            // 
            this.Fecha.HeaderText = "Fecha";
            this.Fecha.Name = "Fecha";
            this.Fecha.ReadOnly = true;
            this.Fecha.Width = 90;
            // 
            // Entidad
            // 
            this.Entidad.HeaderText = "Entidad";
            this.Entidad.Name = "Entidad";
            this.Entidad.ReadOnly = true;
            this.Entidad.Width = 90;
            // 
            // Categoria
            // 
            this.Categoria.HeaderText = "Categoria";
            this.Categoria.Name = "Categoria";
            this.Categoria.ReadOnly = true;
            this.Categoria.Width = 60;
            // 
            // Autor
            // 
            this.Autor.HeaderText = "Autor";
            this.Autor.Name = "Autor";
            this.Autor.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(223, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Criterio:";
            // 
            // ReporteAnalisis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.lblTitulo);
            this.Name = "ReporteAnalisis";
            this.Size = new System.Drawing.Size(750, 450);
            ((System.ComponentModel.ISupportInitialize)(this.frasesCaracteristicasGrid)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabAutores.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridListado)).EndInit();
            this.tabFrases.ResumeLayout(false);
            this.tabReporteAutores.ResumeLayout(false);
            this.tabReporteAutores.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridReporteAutores)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.DataGridView frasesCaracteristicasGrid;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabAutores;
        private System.Windows.Forms.TabPage tabFrases;
        private System.Windows.Forms.DataGridView gridListado;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn Apellido;
        private System.Windows.Forms.DataGridViewTextBoxColumn Usuario;
        private System.Windows.Forms.DataGridViewTextBoxColumn FechaNac;
        private System.Windows.Forms.TabPage tabReporteAutores;
        private System.Windows.Forms.ComboBox comboBxCriterio;
        private System.Windows.Forms.DataGridView gridReporteAutores;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Valor;
        private System.Windows.Forms.DataGridViewTextBoxColumn Frase;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fecha;
        private System.Windows.Forms.DataGridViewTextBoxColumn Entidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Categoria;
        private System.Windows.Forms.DataGridViewTextBoxColumn Autor;
        private System.Windows.Forms.Label label1;
    }
}
