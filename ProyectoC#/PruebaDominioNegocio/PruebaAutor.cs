﻿using System;
using DominioNegocio;
using DominioNegocio.Excepciones;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PruebaDominioNegocio
{
    [TestClass]
    public class PruebaAutor
    {
        [TestMethod]
        public void PruebaModificarUsuarioAutorNoVacio()
        {
            Autor autor = new Autor();
            autor.Usuario = "autor1";
            Assert.AreEqual("autor1", autor.Usuario);
        }

        [ExpectedException(typeof(ExcepcionStringVacio))]
        [TestMethod]
        public void PruebaModificarUsuarioAutorVacio()
        {
            Autor autor = new Autor();
            autor.Usuario = "";
        }

        [ExpectedException(typeof(ExcepcionStringNoAlfanumerico))]
        [TestMethod]
        public void PruebaModificarUsuarioAutorNoAlfanumerico()
        {
            Autor autor = new Autor();
            autor.Usuario = "autor$";
        }

        [ExpectedException(typeof(ExcepcionStringLargoInvalido))]
        [TestMethod]
        public void PruebaModificarUsuarioAutorLargoMAyorA10()
        {
            Autor autor = new Autor();
            autor.Usuario = "autor1234567";
        }

        [TestMethod]
        public void PruebaModificarNombreAutorNoVacio()
        {
            Autor autor = new Autor();
            autor.Nombre = "autor";
            Assert.AreEqual("autor", autor.Nombre);
        }

        [TestMethod]
        public void PruebaModificarApellidoAutorNoVacio()
        {
            Autor autor = new Autor();
            autor.Apellido = "apellidoPrueba";
            Assert.AreEqual("apellidoPrueba", autor.Apellido);
        }

        [ExpectedException(typeof(ExcepcionStringVacio))]
        [TestMethod]
        public void PruebaModificarNombreAutorVacio()
        {
            Autor autor = new Autor();
            autor.Nombre = "";
        }

        [ExpectedException(typeof(ExcepcionStringVacio))]
        [TestMethod]
        public void PruebaModificarApellidoAutorVacio()
        {
            Autor autor = new Autor();
            autor.Apellido = "";
        }

        [ExpectedException(typeof(ExcepcionStringNoNumerico))]
        [TestMethod]
        public void PruebaModificarNombreAutorNoNumerico()
        {
            Autor autor = new Autor();
            autor.Nombre = "autor12";
        }

        [ExpectedException(typeof(ExcepcionStringNoNumerico))]
        [TestMethod]
        public void PruebaModificarApellidoAutorNoNumerico()
        {
            Autor autor = new Autor();
            autor.Apellido = "apellido a";
        }

        [ExpectedException(typeof(ExcepcionStringLargoInvalido))]
        [TestMethod]
        public void PruebaModificarNombreAutorLargoMAyorA15()
        {
            Autor autor = new Autor();
            autor.Nombre = "autorAAAAAAAAAAA";
        }

        [ExpectedException(typeof(ExcepcionStringLargoInvalido))]
        [TestMethod]
        public void PruebaModificarApellidoAutorLargoMAyorA15()
        {
            Autor autor = new Autor();
            autor.Apellido = "apellidoAAAAAAAA";
        }

        [TestMethod]
        public void PruebaModificarFechaNacimientoAutor30anios()
        {
            Autor autor = new Autor();
            DateTime fecha = DateTime.UtcNow.AddYears(-30);
            autor.FechaNacimiento = fecha;
            Assert.AreEqual(fecha, autor.FechaNacimiento);
        }

        [ExpectedException(typeof(ExcepcionFechaNacimientoMinima))]
        [TestMethod]
        public void PruebaModificarFechaNacimientoAutor12anios()
        {
            Autor autor = new Autor();
            DateTime fecha = DateTime.UtcNow.AddYears(-12);
            autor.FechaNacimiento = fecha;
        }

        [ExpectedException(typeof(ExcepcionFechaNacimientoMaxima))]
        [TestMethod]
        public void PruebaModificarFechaNacimientoAutor101anios()
        {
            Autor autor = new Autor();
            DateTime fecha = DateTime.UtcNow.AddYears(-101);
            autor.FechaNacimiento = fecha;
        }

        [TestMethod]
        public void PruebaIgualdadMismoUsuario()
        {
            DateTime fecha = DateTime.Now.AddYears(-45);
            Autor autor1 = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            Autor autor2 = new Autor
            {
                Nombre = "Marcos",
                Apellido = "Rojo",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            Assert.AreEqual(autor1, autor2);
        }

        [TestMethod]
        public void PruebaDesIgualdad()
        {
            DateTime fecha = DateTime.Now.AddYears(-45);
            Autor autor1 = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            Autor autor2 = new Autor
            {
                Nombre = "Marcos",
                Apellido = "Rojo",
                Usuario = "Arturo90",
                FechaNacimiento = fecha
            };
            Assert.AreEqual(false, autor1.Equals(autor2));
        }

        [TestMethod]
        public void PruebaDesIgualdadTiposDistintos()
        {
            DateTime fecha = DateTime.Now.AddYears(-45);
            Autor autor1 = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            Entidad entidadPrueba = new Entidad("Prueba");
            Assert.AreEqual(false, autor1.Equals(entidadPrueba));
        }

        [TestMethod]
        public void PruebaToString()
        {
            DateTime fecha = DateTime.Now.AddYears(-45);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = fecha
            };
            Assert.AreEqual("Arturo Vidal, Arturo89, " + fecha.ToShortDateString(), autor.ToString());
        }
    }
}
