﻿using DominioNegocio;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DominioNegocio.Excepciones;

namespace PruebaLogicaNegocio
{
    [TestClass]
    public class PruebaEntidad
    {
        [ExpectedException(typeof(ExcepcionStringVacio))]
        [TestMethod]
        public void PruebaModificarNombreEntidadStringVacio()
        {
            Entidad entidad = new Entidad("Nombre De Prueba");
            entidad.NombreUnaEntidad = "";
        }

        [ExpectedException(typeof(ExcepcionStringVacio))]
        [TestMethod]
        public void PruebaCrearEntidadStringVacio()
        {
            Entidad entidad = new Entidad("");
        }

        [TestMethod]
        public void PruebaModificarNombreEntidadStringNoVacio()
        {
            Entidad entidad = new Entidad("Nombre De Prueba");
            entidad.NombreUnaEntidad = "Nombre De Prueba 2";
            Assert.AreEqual("Nombre De Prueba 2", entidad.NombreUnaEntidad);
        }

        [TestMethod]
        public void PruebaIgualdadMismoNombre()
        {
            Entidad entidad1 = new Entidad("Nombre De Prueba");
            Entidad entidad2 = new Entidad("Nombre De Prueba");
            Assert.AreEqual(true, entidad1.Equals(entidad2));
        }

        [TestMethod]
        public void PruebaIgualdadClaseDiferente()
        {
            Entidad entidad1 = new Entidad("Nombre De Prueba 2");
            int numero = 4;
            Assert.AreEqual(false, entidad1.Equals(numero));
        }

        [TestMethod]
        public void PruebaIgualdadConNulo()
        {
            Entidad entidad1 = new Entidad("Nombre De Prueba 2");
            Entidad entidad2 = null;
            Assert.AreEqual(false, entidad1.Equals(entidad2));
        }

        [TestMethod]
        public void PruebaToString()
        {
            Entidad entidad = new Entidad("cocacola");
            Assert.AreEqual("cocacola", entidad.ToString());
        }
    }
}