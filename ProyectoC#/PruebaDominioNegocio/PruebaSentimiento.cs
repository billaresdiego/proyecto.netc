﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DominioNegocio;
using DominioNegocio.Excepciones;

namespace PruebaLogicaNegocio
{
    [TestClass]
    public class PruebaSentimiento
    {

        [ExpectedException(typeof(ExcepcionSentimientoLargoMenorDeDos))]
        [TestMethod]
        public void PruebaSentimientoVacio()
        {
            Sentimiento sentimiento = new Sentimiento();
            sentimiento.NombreSentimiento = "";
        }

        [ExpectedException(typeof(ExcepcionSentimientoLargoMenorDeDos))]
        [TestMethod]
        public void PruebaSentimientoConUnCaracter()
        {
            Sentimiento sentimiento = new Sentimiento();
            sentimiento.NombreSentimiento = "a";
        }

        [TestMethod]
        public void PruebaSentimientoLargoDos()
        {
            Sentimiento sentimiento = new Sentimiento();
            sentimiento.NombreSentimiento = "ab";
            Assert.AreEqual("ab", sentimiento.NombreSentimiento);
        }

        [TestMethod]
        public void PruebaSentimientoLargoMayorDeDos()
        {
            Sentimiento sentimiento = new Sentimiento();
            sentimiento.NombreSentimiento = "mal";
            Assert.AreEqual("mal", sentimiento.NombreSentimiento);
        }

        [ExpectedException(typeof(ExcepcionSentimientoConCaracteresNoAlfabeticos))]
        [TestMethod]
        public void PruebaSentimientoConCaracteresNumericos()
        {
            Sentimiento sentimiento = new Sentimiento();
            sentimiento.NombreSentimiento = "3.9";
        }

        [ExpectedException(typeof(ExcepcionSentimientoConCaracteresNoAlfabeticos))]
        [TestMethod]
        public void PruebaSentimientoConCaracteresEspeciales()
        {
            Sentimiento sentimiento = new Sentimiento();
            sentimiento.NombreSentimiento = "Prueba Con caracter - especial";
        }

        [TestMethod]
        public void PruebaIgualdadMismoSentimiento()
        {
            Sentimiento sentimiento1 = new Sentimiento();
            sentimiento1.NombreSentimiento = "sentimiendo de prueba";
            Sentimiento sentimiento2 = new Sentimiento();
            sentimiento2.NombreSentimiento = "sentimiendo de prueba";
            Assert.AreEqual(true, sentimiento1.Equals(sentimiento2));
        }

        [TestMethod]
        public void PruebaIgualdadSentimientoDiferente()
        {
            Sentimiento sentimiento1 = new Sentimiento();
            sentimiento1.NombreSentimiento = "sentimiendo de prueba";
            Sentimiento sentimiento2 = new Sentimiento();
            sentimiento2.NombreSentimiento = "otro sentimiento de prueba";
            Assert.AreEqual(false, sentimiento1.Equals(sentimiento2));
        }

        [TestMethod]
        public void PruebaIgualdadClaseDiferente()
        {
            Sentimiento sentimiento = new Sentimiento();
            sentimiento.NombreSentimiento = "sentimiendo de prueba";
            int numero = 4;
            Assert.AreEqual(false, sentimiento.Equals(numero));
        }

        [TestMethod]
        public void PruebaIgualdadConNulo()
        {
            Sentimiento sentimiento1 = new Sentimiento();
            sentimiento1.NombreSentimiento = "sentimiendo de prueba";
            Sentimiento sentimiento2 = null;
            Assert.AreEqual(false, sentimiento1.Equals(sentimiento2));
        }

        [TestMethod]
        public void PruebaSentimientoNegativo()
        {
            Sentimiento sentimiento = new Sentimiento();
            sentimiento.TipoSentimiento = Sentimiento.TiposSentimiento.negativo;
            Assert.AreEqual(Sentimiento.TiposSentimiento.negativo, sentimiento.TipoSentimiento);
        }

        [TestMethod]
        public void PruebaConstructorSentimiento()
        {
            Sentimiento sentimiento1 = new Sentimiento(Sentimiento.TiposSentimiento.positivo, "genial");
            Sentimiento sentimiento2 = new Sentimiento(Sentimiento.TiposSentimiento.positivo, "genial");
            Assert.AreEqual(sentimiento1, sentimiento2);
        }

        [TestMethod]
        public void PruebaToString()
        {
            Sentimiento sentimiento1 = new Sentimiento(Sentimiento.TiposSentimiento.positivo, "genial");
            Assert.AreEqual("genial", sentimiento1.ToString());
        }
    }
}
