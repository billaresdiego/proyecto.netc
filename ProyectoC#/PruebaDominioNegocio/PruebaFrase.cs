﻿using System;
using DominioNegocio;
using DominioNegocio.Excepciones;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PruebaLogicaNegocio
{
    [TestClass]
    public class PruebaFrase
    {
        [TestMethod]
        public void PruebaFraseIdAsignoNumero()
        {
            Frase frase = new Frase();
            frase.Id = 1;
            Assert.AreEqual(1, frase.Id);
        }

        [ExpectedException(typeof(ExcepcionStringVacio))]
        [TestMethod]
        public void PruebaFraseVacia()
        {
            Frase frase = new Frase();
            frase.ContenidoUnaFrase = "";
        }

        [ExpectedException(typeof(ExcepcionFraseLargoMenorDeCuatro))]
        [TestMethod]
        public void PruebaFraseLargoMenorDeCuatro()
        {
            Frase frase = new Frase();
            frase.ContenidoUnaFrase = "ada";
        }

        [ExpectedException(typeof(ExcepcionFraseSinEspacios))]
        [TestMethod]
        public void PruebaFraseSinEspacios()
        {
            Frase frase = new Frase();
            frase.ContenidoUnaFrase = "adaadawdaw";
        }

        [TestMethod]
        public void PruebaFraseLargoMayorDeCuatro()
        {
            Frase frase = new Frase();
            frase.ContenidoUnaFrase = "mal u";
            Assert.AreEqual("mal u", frase.ContenidoUnaFrase);
        }

        [ExpectedException(typeof(ExcepcionCategoriaInvalida))]
        [TestMethod]
        public void PreubaCategoriaInvalida()
        {
            Frase frase = new Frase();
            frase.UnaCategoria = "awdawfxv";
        }

        [TestMethod]
        public void PruebaCategoriaPositivo()
        {
            Frase frase = new Frase();
            frase.UnaCategoria = "PosiTivO";
            Assert.AreEqual("PosiTivO", frase.UnaCategoria);
        }

        [TestMethod]
        public void PruebaCategoriaNegativo()
        {
            Frase frase = new Frase();
            frase.UnaCategoria = "NegatIvO";
            Assert.AreEqual("NegatIvO", frase.UnaCategoria);
        }

        [TestMethod]
        public void PruebaCategoriaNeutro()
        {
            Frase frase = new Frase();
            frase.UnaCategoria = "NeUtrO";
            Assert.AreEqual("NeUtrO", frase.UnaCategoria);
        }

        [ExpectedException(typeof(ExcepcionStringVacio))]
        [TestMethod]
        public void PruebaEntidadVacia()
        {
            Frase frase = new Frase();
            frase.UnaEntidad = new Entidad("");
        }

        [TestMethod]
        public void PruebaEntidadValida()
        {
            Frase frase = new Frase();
            frase.UnaEntidad = new Entidad("Negocio112f");
            Assert.AreEqual(new Entidad("Negocio112f"), frase.UnaEntidad);
        }

        [TestMethod]
        public void PruebaFechaHoy()
        {
            Frase frase = new Frase();
            frase.FechaDeIngreso = DateTime.UtcNow.Date;
            DateTime fechaParaValidar = DateTime.UtcNow.Date;
            String fecha = frase.FechaDeIngreso.ToString("dd/MM/yyyy");
            Assert.AreEqual(fechaParaValidar.ToString("dd/MM/yyyy"), fecha);
        }

        [TestMethod]
        public void PruebaFrasesIgualesDistintaInstancia()
        {
            Frase frase1 = new Frase();
            frase1.ContenidoUnaFrase = "Odio Macdonalds";
            Frase frase2 = new Frase();
            frase2.ContenidoUnaFrase = "Odio Macdonalds";
            Assert.AreEqual(true, frase1.Equals(frase2));
        }

        [TestMethod]
        public void PruebaFrasesIgualdadConNulo()
        {
            Frase frase1 = new Frase();
            frase1.ContenidoUnaFrase = "Odio Macdonalds";
            Frase frase2 = null;
            Assert.AreEqual(false, frase1.Equals(frase2));
        }

        [TestMethod]
        public void PruebaConstructorConParametros()
        {
            Entidad entidad1 = new Entidad("La pasiva");
            Entidad entidad2 = new Entidad("La pasiva");
            DateTime fechaAyer = DateTime.Now.AddDays(-1);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = DateTime.Now.AddYears(-50)
            };
            Frase frase1 = new Frase("Me encanta La Pasiva", "Positivo", entidad1, fechaAyer,autor);
            Frase frase2 = new Frase("Me encanta La Pasiva", "Positivo", entidad2, fechaAyer, autor);
            Assert.AreEqual(true, frase1.Equals(frase2));
        }

        [TestMethod]
        public void PruebaToString()
        {
            Entidad entidad1 = new Entidad("La pasiva");
            DateTime fechaAyer = DateTime.UtcNow.AddDays(-1);
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = DateTime.Now.AddYears(-50)
            };
            Frase frase1 = new Frase("Me encanta La Pasiva", "Positivo", entidad1, fechaAyer, autor);
            Assert.AreEqual("Me encanta La Pasiva", frase1.ToString());
        }

        [TestMethod]
        public void PruebaConfigurarAutor()
        {
            Frase frase = new Frase();
            Autor autor = new Autor
            {
                Nombre = "Arturo",
                Apellido = "Vidal",
                Usuario = "Arturo89",
                FechaNacimiento = DateTime.Now.AddYears(-50)
            };
            frase.Autor = autor;
            Assert.AreEqual(autor, frase.Autor);
        }

        [ExpectedException(typeof(ExcepcionAutorInvalido))]
        [TestMethod]
        public void PruebaConfigurarAutorNulo()
        {
            Frase frase = new Frase();
            Autor autor = null;
            frase.Autor = autor;
        }
    }
}
