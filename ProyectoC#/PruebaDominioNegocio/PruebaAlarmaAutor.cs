﻿using System;
using DominioNegocio;
using DominioNegocio.Excepciones;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PruebaDominioNegocio
{
    [TestClass]
    public class PruebaAlarmaAutor
    {
        [TestMethod]
        public void PruebaModificarPlazoDias()
        {
            AlarmaAutor alarma = new AlarmaAutor();
            alarma.PlazoDias = 1;
            Assert.AreEqual(1, alarma.PlazoDias);
        }

        [TestMethod]
        public void PruebaModificarPlazoHoras()
        {
            AlarmaAutor alarma = new AlarmaAutor();
            alarma.PlazoHoras = 1;
            Assert.AreEqual(1, alarma.PlazoHoras);
        }

        [ExpectedException(typeof(ExcepcionHoraNoValida))]
        [TestMethod]
        public void PruebaModificarPlazoHoras24()
        {
            AlarmaAutor alarma = new AlarmaAutor();
            alarma.PlazoHoras = 24;
        }

        [TestMethod]
        public void PruebaConfigurarTipoPositivo()
        {
            AlarmaAutor alarma = new AlarmaAutor();
            alarma.UnTipo = AlarmaAutor.TipoAlarmaAutor.positivo;
            Assert.AreEqual(AlarmaAutor.TipoAlarmaAutor.positivo, alarma.UnTipo);
        }

        [TestMethod]
        public void PruebaConfigurarTipoNegativo()
        {
            AlarmaAutor alarma = new AlarmaAutor();
            alarma.UnTipo = AlarmaAutor.TipoAlarmaAutor.negativo;
            Assert.AreEqual(AlarmaAutor.TipoAlarmaAutor.negativo, alarma.UnTipo);
        }

        [TestMethod]
        public void PruebaConfigurarEstadoActivada()
        {
            AlarmaAutor alarma = new AlarmaAutor();
            alarma.Activada = true;
            Assert.AreEqual(true, alarma.Activada);
        }

        [TestMethod]
        public void PruebaConfigurarCantidadPosts1()
        {
            AlarmaAutor alarma = new AlarmaAutor();
            alarma.CantidadDePosts = 1;
            Assert.AreEqual(1, alarma.CantidadDePosts);
        }

        [ExpectedException(typeof(ExcepcionCeroNoValido))]
        [TestMethod]
        public void PruebaConfigurarCantidadPosts0()
        {
            AlarmaAutor alarma = new AlarmaAutor();
            alarma.CantidadDePosts = 0;
        }

        [ExpectedException(typeof(ExcepcionCantidadInvalida))]
        [TestMethod]
        public void PruebaConfigurarCantidadPosts1000()
        {
            AlarmaAutor alarma = new AlarmaAutor();
            alarma.CantidadDePosts = 1000;
        }

        [TestMethod]
        public void PruebaCalcularFechaComienzo10Horas()
        {
            AlarmaAutor alarma = new AlarmaAutor();
            alarma.PlazoHoras = 10;
            DateTime fechaComienzoCalculada = alarma.CalcularFechaComienzo();
            DateTime fechaComienzo = DateTime.Now.AddHours(-10);
            Assert.AreEqual(fechaComienzo.ToString("MM/dd/yyyy HH:mm"), fechaComienzoCalculada.ToString("MM/dd/yyyy HH:mm"));
        }

        [TestMethod]
        public void PruebaCalcularFechaComienzo10Dias()
        {
            AlarmaAutor alarma = new AlarmaAutor();
            alarma.PlazoDias = 10;
            DateTime fechaComienzoCalculada = alarma.CalcularFechaComienzo();
            DateTime fechaComienzo = DateTime.Now.AddDays(-10);
            Assert.AreEqual(fechaComienzo.ToString("MM/dd/yyyy HH:mm"), fechaComienzoCalculada.ToString("MM/dd/yyyy HH:mm"));
        }

        [TestMethod]
        public void PruebaIgualdad()
        {
            AlarmaAutor alarma1 = new AlarmaAutor
            {
                UnTipo = AlarmaAutor.TipoAlarmaAutor.positivo,
                CantidadDePosts = 10,
                PlazoDias = 3,
                Activada = false
            };
            AlarmaAutor alarma2 = new AlarmaAutor
            {
                UnTipo = AlarmaAutor.TipoAlarmaAutor.positivo,
                CantidadDePosts = 10,
                PlazoDias = 3,
                Activada = true
            };
            Assert.AreEqual(true, alarma1.Equals(alarma2));
        }

        [TestMethod]
        public void PruebaDesigualdad()
        {
            AlarmaAutor alarma1 = new AlarmaAutor
            {
                UnTipo = AlarmaAutor.TipoAlarmaAutor.positivo,
                CantidadDePosts = 5,
                PlazoDias = 10,
                Activada = false
            };
            AlarmaAutor alarma2 = new AlarmaAutor
            {
                UnTipo = AlarmaAutor.TipoAlarmaAutor.positivo,
                CantidadDePosts = 10,
                PlazoDias = 3,
                Activada = true
            };
            Assert.AreEqual(false, alarma1.Equals(alarma2));
        }

        [TestMethod]
        public void PruebaDesigualdadTiposDistintos()
        {
            AlarmaAutor alarma1 = new AlarmaAutor
            {
                UnTipo = AlarmaAutor.TipoAlarmaAutor.positivo,
                CantidadDePosts = 5,
                PlazoDias = 10,
                Activada = false
            };
            Entidad entidadPrueba = new Entidad("Prueba");
            Assert.AreEqual(false, alarma1.Equals(entidadPrueba));
        }

        [TestMethod]
        public void PruebaDevolverAlarma()
        {
            AlarmaAutor alarma = new AlarmaAutor
            {
                Activada = false,
                CantidadDePosts = 4,
                PlazoDias = 5,
            };
            AlarmaAutor alarmaDevuelta = (AlarmaAutor)alarma.DevolverAlarma();
            Assert.AreEqual(alarma, alarmaDevuelta);
        }
    }
}
