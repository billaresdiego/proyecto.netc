﻿using System;
using DominioNegocio;
using DominioNegocio.Excepciones;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PruebaLogicaNegocio
{
    [TestClass]
    public class PruebaAlarma
    {
        [TestMethod]
        public void PruebaIdAlarma()
        {
            Alarma alarma = new Alarma();
            alarma.Id = 1;
            Assert.AreEqual(1, alarma.Id);
        }

        [TestMethod]
        public void PruebaCrearAlarmaConEntidad()
        {
            Entidad entidadPrueba = new Entidad("Nombre de prueba");
            Alarma alarma = new Alarma(entidadPrueba);
            Assert.AreEqual(entidadPrueba, alarma.UnaEntidad);
        }

        [TestMethod]
        public void PruebaConfigurarEntidadAlarma()
        {
            Entidad entidadPrueba = new Entidad("Nombre de prueba");
            Alarma alarma = new Alarma();
            alarma.UnaEntidad = entidadPrueba;
            Assert.AreEqual(entidadPrueba, alarma.UnaEntidad);
        }

        [TestMethod]
        public void PruebaConfigurarCantidadDePosts1Alarma()
        {
            Alarma alarma = new Alarma();
            alarma.CantidadDePosts = 1;
            Assert.AreEqual(1, alarma.CantidadDePosts);
        }

        [ExpectedException(typeof(ExcepcionCeroNoValido))]
        [TestMethod]
        public void PruebaConfigurarCantidadDePosts0Alarma()
        {
            Alarma alarma = new Alarma();
            alarma.CantidadDePosts = 0;
        }

        [TestMethod]
        public void PruebaConfigurarTipoPositivoAlarma()
        {
            Alarma alarma = new Alarma();
            alarma.UnTipo = Alarma.Tipo.positivo;
            Assert.AreEqual(Alarma.Tipo.positivo, alarma.UnTipo);
        }

        [TestMethod]
        public void PruebaConfigurarEstadoActivadoAlarma()
        {
            Alarma alarma = new Alarma();
            alarma.Activada = true;
            Assert.AreEqual(true, alarma.Activada);
        }

        [TestMethod]
        public void PruebaConfigurarPlazo1Dia()
        {
            Alarma alarma = new Alarma();
            alarma.PlazoDias = 1;
            Assert.AreEqual(1, alarma.PlazoDias);
        }

        [TestMethod]
        public void PruebaConfigurarPlazo1Hora()
        {
            Alarma alarma = new Alarma();
            alarma.PlazoHoras = 1;
            Assert.AreEqual(1, alarma.PlazoHoras);
        }

        [ExpectedException(typeof(ExcepcionHoraNoValida))]
        [TestMethod]
        public void PruebaConfigurarPlazo24Horas()
        {
            Alarma alarma = new Alarma();
            alarma.PlazoHoras = 24;
        }

        [TestMethod]
        public void PruebaCalcularFechaComienzo0Dias10Horas()
        {
            Alarma alarma = new Alarma();
            alarma.PlazoDias = 0;
            alarma.PlazoHoras = 10;
            DateTime fechaComienzoCalculada = alarma.CalcularFechaComienzo();
            DateTime fechaComienzo = DateTime.Now.AddHours(-10);
            Assert.AreEqual(fechaComienzo.Hour, fechaComienzoCalculada.Hour);
        }

        [TestMethod]
        public void PruebaConstructorTodosLosAtributosPrimerAtributo()
        {
            Entidad entidad = new Entidad("Remaing");
            Alarma alarma = new Alarma(entidad, Alarma.Tipo.positivo, 3, false, 4, 5);
            Assert.AreEqual(new Entidad("Remaing"), alarma.UnaEntidad);
        }

        [TestMethod]
        public void PruebaToString()
        {
            Entidad entidad = new Entidad("CocaCola");
            Alarma alarma = new Alarma(entidad, Alarma.Tipo.positivo, 3, false, 4, 5);
            Assert.AreEqual("Entidad: CocaCola, Tipo: positivo, Posts: 3", alarma.ToString());
        }

        [ExpectedException(typeof(ExcepcionAlarmaSinEntidad))]
        [TestMethod]
        public void PruebaAlarmaSinEntidad()
        {
            Alarma alarma = new Alarma
            {
                UnaEntidad = null
            };
        }

        [TestMethod]
        public void PruebaDesigualdad()
        {
            Entidad entidad = new Entidad("CocaCola");
            Entidad entidad2 = new Entidad("Texaco");
            Alarma alarma1 = new Alarma
            {
                UnTipo = Alarma.Tipo.positivo,
                CantidadDePosts = 5,
                PlazoDias = 10,
                Activada = false,
                UnaEntidad = entidad
            };
            Alarma alarma2 = new Alarma
            {
                UnTipo = Alarma.Tipo.positivo,
                CantidadDePosts = 10,
                PlazoDias = 3,
                Activada = true,
                UnaEntidad = entidad2
            };
            Assert.AreEqual(false, alarma1.Equals(alarma2));
        }

        [TestMethod]
        public void PruebaDesigualdadTipoDiferente()
        {
            Entidad entidad = new Entidad("CocaCola");
            Alarma alarma1 = new Alarma
            {
                UnTipo = Alarma.Tipo.positivo,
                CantidadDePosts = 5,
                PlazoDias = 10,
                Activada = false,
                UnaEntidad = entidad
            };
            Assert.AreEqual(false, alarma1.Equals(entidad));
        }
    }
}
